package com.saldmy.command;

import com.saldmy.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The {@code Command} represents a common interface, that {@code Controller} class can receive.
 * This is the main interface, that make possible the realization of front-controller architecture.
 * <p>
 * The controller receives a command from the URI parameters, called {@code "command"}.
 *
 * @author Dmytro Salo
 */
public interface Command {

    String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException;

}

