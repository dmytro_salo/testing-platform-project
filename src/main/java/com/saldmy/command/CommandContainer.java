package com.saldmy.command;

import com.saldmy.command.manage.difficulty.CreateDifficultyCommand;
import com.saldmy.command.manage.difficulty.DeleteDifficultyCommand;
import com.saldmy.command.manage.difficulty.EditDifficultyCommand;
import com.saldmy.command.manage.subject.CreateSubjectCommand;
import com.saldmy.command.manage.subject.DeleteSubjectCommand;
import com.saldmy.command.manage.subject.EditSubjectCommand;
import com.saldmy.command.manage.user.*;
import com.saldmy.command.process.ReceiveAnswerCommand;
import com.saldmy.command.process.ServiceTestCommand;
import com.saldmy.command.process.StartTestCommand;
import com.saldmy.command.process.SubmitTestCommand;
import com.saldmy.command.show.*;
import com.saldmy.command.manage.test.*;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple class-container, that maps realizations of {@code Command} interface to their URI
 * representations.
 *
 * @author Dmytro Salo
 * @see com.saldmy.command.Command
 */
public abstract class CommandContainer {

    private static final Map<String, Command> commands;

    static {
        commands = new HashMap<>();
        commands.put("signIn", new SignInCommand());
        commands.put("signUp", new SignUpCommand());
        commands.put("logOut", new LogOutCommand());
        commands.put("showManageUsersPage", new ShowManageUsersPageCommand());
        commands.put("editUser", new EditUserCommand());
        commands.put("deleteUser", new DeleteUserCommand());
        commands.put("blockUser", new BlockUserCommand());
        commands.put("unblockUser", new UnblockUserCommand());
        commands.put("showManageTestsPage", new ShowManageTestsPageCommand());
        commands.put("showCreateTestPage", new ShowCreateTestPageCommand());
        commands.put("showConfigureTestPage", new ShowConfigureTestPageCommand());
        commands.put("showFoundTestsPage", new ShowFoundTestsPageCommand());
        commands.put("createTest", new CreateTestCommand());
        commands.put("editTest", new EditTestCommand());
        commands.put("deleteTest", new DeleteTestCommand());
        commands.put("download", new DownloadCommand());
        commands.put("setItemQuantity", new SetItemQuantityCommand());
        commands.put("filterTests", new FilterTestsCommand());
        commands.put("startTest", new StartTestCommand());
        commands.put("serviceTest", new ServiceTestCommand());
        commands.put("submitTest", new SubmitTestCommand());
        commands.put("receiveAnswer", new ReceiveAnswerCommand());
        commands.put("editDifficulty", new EditDifficultyCommand());
        commands.put("createDifficulty", new CreateDifficultyCommand());
        commands.put("deleteDifficulty", new DeleteDifficultyCommand());
        commands.put("editSubject", new EditSubjectCommand());
        commands.put("createSubject", new CreateSubjectCommand());
        commands.put("deleteSubject", new DeleteSubjectCommand());
        commands.put("showUserTestResultsPage", new ShowUserTestResultsPageCommand());
    }

    public static Command getCommand(String commandName) {
        return commands.get(commandName);
    }

}
