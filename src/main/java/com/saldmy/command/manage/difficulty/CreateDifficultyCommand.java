package com.saldmy.command.manage.difficulty;

import com.saldmy.command.Command;
import com.saldmy.dao.DifficultyDAO;
import com.saldmy.entity.Difficulty;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The class {@code CreateDifficultyCommand} is a realization of {@code Command} interface.
 * It is responsible for the setting up and initialization of the new difficulty in the testing
 * platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class CreateDifficultyCommand implements Command {
    private static final Logger log = LogManager.getLogger(CreateDifficultyCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String name = req.getParameter("name");
        String nameUk = req.getParameter("nameUk");
        String levelParam = req.getParameter("level");

        log.debug("name = " + name);
        log.debug("nameUk = " + nameUk);
        log.debug("level = " + levelParam);

        int level = Integer.parseInt(levelParam);
        if (level > 100 || level < 1) {
            return req.getContextPath() + "/badRequest.jsp";
        }

        if (name == null || nameUk == null || name.equals("") || nameUk.equals("")) {
            return req.getContextPath() + "/secure/subjAndDiff.jsp?emptyCreateDiffName=true";
        }

        Difficulty writtenDifficulty = new Difficulty();
        writtenDifficulty.setName(name);
        writtenDifficulty.setNameUk(nameUk);
        writtenDifficulty.setLevel(level);

        List<Difficulty> existingDifficulties =
                (List<Difficulty>) req.getServletContext().getAttribute("difficulties");

        if (existingDifficulties.stream()
                .anyMatch(d -> d.getLevel() == writtenDifficulty.getLevel())) {

            req.getSession().setAttribute("writtenDifficulty", writtenDifficulty);

            return req.getContextPath() + "/secure/subjAndDiff.jsp?levelExists=true";
        }

        if (existingDifficulties.stream()
                .anyMatch(d -> d.getName().equals(writtenDifficulty.getName()))) {

            req.getSession().setAttribute("writtenDifficulty", writtenDifficulty);

            return req.getContextPath() + "/secure/subjAndDiff.jsp?englishNameExists=true";
        }

        if (existingDifficulties.stream()
                .anyMatch(d -> d.getNameUk().equals(writtenDifficulty.getNameUk()))) {

            req.getSession().setAttribute("writtenDifficulty", writtenDifficulty);

            return req.getContextPath() + "/secure/subjAndDiff.jsp?ukrainianNameExists=true";
        }

        try {
            if (DifficultyDAO.createDifficulty(writtenDifficulty)) {

                List<Difficulty> difficulties = DifficultyDAO.getAllDifficulties();
                req.getServletContext().setAttribute("difficulties", difficulties);

                req.getSession().removeAttribute("writtenDifficulty");

                return req.getContextPath() + "/secure/subjAndDiff.jsp";
            } else {
                return req.getContextPath() + "/error.jsp";
            }
        } catch (DAOException e) {
            throw new CommandException(e);
        }
    }

}
