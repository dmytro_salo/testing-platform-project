package com.saldmy.command.manage.difficulty;

import com.saldmy.command.Command;
import com.saldmy.dao.DifficultyDAO;
import com.saldmy.dao.TestDAO;
import com.saldmy.entity.Difficulty;
import com.saldmy.entity.Test;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The class {@code DeleteDifficultyCommand} is a realization of {@code Command} interface.
 * It is responsible for the deleting the difficulty in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class DeleteDifficultyCommand implements Command {
    private static final Logger log = LogManager.getLogger(DeleteDifficultyCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String difficultyId = req.getParameter("id");

        if (!Validators.validateIntParameter(difficultyId)) {
            return req.getContextPath() + "/badRequest.jsp";
        }

        List<Test> tests;
        try {
            tests = TestDAO.getAllTests();
        } catch (DAOException e) {
            throw new CommandException(e);
        }

        if (tests.stream().anyMatch(t -> t.getDifficulty().getId() == Integer.parseInt(difficultyId))) {
            return req.getContextPath() + "/secure/subjAndDiff.jsp?deleteError=testExists";
        }

        try {
            if (DifficultyDAO.deleteDifficulty(difficultyId)) {

                List<Difficulty> difficulties = DifficultyDAO.getAllDifficulties();
                req.getServletContext().setAttribute("difficulties", difficulties);

                return req.getContextPath() + "/secure/subjAndDiff.jsp";
            } else {
                return req.getContextPath() + "/error.jsp";
            }
        } catch (DAOException e) {
            throw new CommandException(e);
        }
    }
}
