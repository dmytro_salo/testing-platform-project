package com.saldmy.command.manage.difficulty;

import com.saldmy.command.Command;
import com.saldmy.dao.DifficultyDAO;
import com.saldmy.entity.Difficulty;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The class {@code EditDifficultyCommand} is a realization of {@code Command} interface.
 * It is responsible for editing the difficulty in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class EditDifficultyCommand implements Command {
    private static final Logger log = LogManager.getLogger(EditDifficultyCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String difficultyId = req.getParameter("id");
        String name = req.getParameter("name");
        String nameUk = req.getParameter("nameUk");
        String levelParam = req.getParameter("level");

        log.debug("difficultyId = " + difficultyId);
        log.debug("name = " + name);
        log.debug("nameUk = " + nameUk);
        log.debug("level = " + levelParam);

        if (!Validators.validateIntParameter(difficultyId) || !Validators.validateIntParameter(levelParam)) {
            return req.getContextPath() + "/badRequest.jsp";
        }

        int level = Integer.parseInt(levelParam);
        if (level > 100 || level < 1) {
            return req.getContextPath() + "/badRequest.jsp";
        }

        if (name == null || nameUk == null || name.equals("") || nameUk.equals("")) {
            return req.getContextPath() + "/secure/subjAndDiff.jsp?emptyDiffName=true";
        }

        Difficulty writtenDifficulty = new Difficulty();
        writtenDifficulty.setId(Integer.parseInt(difficultyId));
        writtenDifficulty.setName(name);
        writtenDifficulty.setNameUk(nameUk);
        writtenDifficulty.setLevel(level);

        List<Difficulty> existingDifficulties =
                (List<Difficulty>) req.getServletContext().getAttribute("difficulties");

        if (existingDifficulties.stream()
                .anyMatch(d -> (d.getLevel() == writtenDifficulty.getLevel() &&
                        d.getId() != writtenDifficulty.getId()))) {

            return req.getContextPath() + "/secure/subjAndDiff.jsp?levelExists=true";
        }

        if (existingDifficulties.stream()
                .anyMatch(d -> d.getName().equals(writtenDifficulty.getName()) &&
                        d.getId() != writtenDifficulty.getId())) {

            return req.getContextPath() + "/secure/subjAndDiff.jsp?englishNameExists=true";
        }

        if (existingDifficulties.stream()
                .anyMatch(d -> d.getNameUk().equals(writtenDifficulty.getNameUk()) &&
                        d.getId() != writtenDifficulty.getId())) {

            return req.getContextPath() + "/secure/subjAndDiff.jsp?ukrainianNameExists=true";
        }

        try {
            if (DifficultyDAO.updateDifficulty(writtenDifficulty)) {

                List<Difficulty> difficulties = DifficultyDAO.getAllDifficulties();
                req.getServletContext().setAttribute("difficulties", difficulties);

                return req.getContextPath() + "/secure/subjAndDiff.jsp";
            } else {
                return req.getContextPath() + "/error.jsp";
            }
        } catch (DAOException e) {
            throw new CommandException(e);
        }
    }

}
