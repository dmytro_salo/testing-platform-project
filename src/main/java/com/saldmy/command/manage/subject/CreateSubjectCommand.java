package com.saldmy.command.manage.subject;

import com.saldmy.command.Command;
import com.saldmy.dao.SubjectDAO;
import com.saldmy.entity.Subject;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The class {@code CreateSubjectCommand} is a realization of {@code Command} interface.
 * It is responsible for the setting up and initialization of the new subject in the testing
 * platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class CreateSubjectCommand implements Command {
    private static final Logger log = LogManager.getLogger(CreateSubjectCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String name = req.getParameter("name");
        String nameUk = req.getParameter("nameUk");

        log.debug("name = " + name);
        log.debug("nameUk = " + nameUk);

        if (name == null || nameUk == null || name.equals("") || nameUk.equals("")) {
            return req.getContextPath() + "/secure/subjAndDiff.jsp?emptyCreateSubjName=true";
        }

        Subject writtenSubject = new Subject();
        writtenSubject.setName(name);
        writtenSubject.setNameUk(nameUk);

        List<Subject> existingSubjects =
                (List<Subject>) req.getServletContext().getAttribute("subjects");

        if (existingSubjects.stream()
                .anyMatch(s -> s.getName().equals(writtenSubject.getName()))) {

            req.getSession().setAttribute("writtenSubject", writtenSubject);

            return req.getContextPath() + "/secure/subjAndDiff.jsp?subjEnglishNameExists=true";
        }

        if (existingSubjects.stream()
                .anyMatch(s -> s.getNameUk().equals(writtenSubject.getNameUk()))) {

            req.getSession().setAttribute("writtenSubject", writtenSubject);

            return req.getContextPath() + "/secure/subjAndDiff.jsp?subjUkrainianNameExists=true";
        }

        try {
            if (SubjectDAO.createSubject(writtenSubject)) {

                List<Subject> subjects = SubjectDAO.getAllSubjects();
                req.getServletContext().setAttribute("subjects", subjects);

                req.getSession().removeAttribute("writtenSubject");

                return req.getContextPath() + "/secure/subjAndDiff.jsp";
            } else {
                return req.getContextPath() + "/error.jsp";
            }
        } catch (DAOException e) {
            throw new CommandException(e);
        }
    }
}
