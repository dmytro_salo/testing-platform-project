package com.saldmy.command.manage.subject;

import com.saldmy.command.Command;
import com.saldmy.dao.SubjectDAO;
import com.saldmy.dao.TestDAO;
import com.saldmy.entity.Subject;
import com.saldmy.entity.Test;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The class {@code DeleteSubjectCommand} is a realization of {@code Command} interface.
 * It is responsible for the deleting the subject in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class DeleteSubjectCommand implements Command {
    private static final Logger log = LogManager.getLogger(DeleteSubjectCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String subjectId = req.getParameter("id");

        if (!Validators.validateIntParameter(subjectId)) {
            return req.getContextPath() + "/badRequest.jsp";
        }

        List<Test> tests;
        try {
            tests = TestDAO.getAllTests();
        } catch (DAOException e) {
            throw new CommandException(e);
        }

        if (tests.stream().anyMatch(t -> t.getSubject().getId() == Integer.parseInt(subjectId))) {
            return req.getContextPath() + "/secure/subjAndDiff.jsp?deleteSubjError=testExists";
        }

        try {
            if (SubjectDAO.deleteSubject(subjectId)) {

                List<Subject> subjects = SubjectDAO.getAllSubjects();
                req.getServletContext().setAttribute("subjects", subjects);

                return req.getContextPath() + "/secure/subjAndDiff.jsp";
            } else {
                return req.getContextPath() + "/error.jsp";
            }
        } catch (DAOException e) {
            throw new CommandException(e);
        }
    }

}
