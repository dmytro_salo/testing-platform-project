package com.saldmy.command.manage.subject;

import com.saldmy.command.Command;
import com.saldmy.dao.SubjectDAO;
import com.saldmy.entity.Subject;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The class {@code EditSubjectCommand} is a realization of {@code Command} interface.
 * It is responsible for editing the subject in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class EditSubjectCommand implements Command {
    private static final Logger log = LogManager.getLogger(EditSubjectCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String subjectId = req.getParameter("id");
        String name = req.getParameter("name");
        String nameUk = req.getParameter("nameUk");

        log.debug("subjectId = " + subjectId);
        log.debug("name = " + name);
        log.debug("nameUk = " + nameUk);

        if (name == null || nameUk == null || name.equals("") || nameUk.equals("")) {
            return req.getContextPath() + "/secure/subjAndDiff.jsp?emptySubjName=true";
        }

        Subject writtenSubject = new Subject();
        writtenSubject.setId(Integer.parseInt(subjectId));
        writtenSubject.setName(name);
        writtenSubject.setNameUk(nameUk);

        List<Subject> existingSubjects =
                (List<Subject>) req.getServletContext().getAttribute("subjects");

        if (existingSubjects.stream()
                .anyMatch(d -> d.getName().equals(writtenSubject.getName()) &&
                        d.getId() != writtenSubject.getId())) {

            return req.getContextPath() + "/secure/subjAndDiff.jsp?subjEnglishNameExists=true";
        }

        if (existingSubjects.stream()
                .anyMatch(d -> d.getNameUk().equals(writtenSubject.getNameUk()) &&
                        d.getId() != writtenSubject.getId())) {

            return req.getContextPath() + "/secure/subjAndDiff.jsp?subjUkrainianNameExists=true";
        }

        try {
            if (SubjectDAO.updateSubject(writtenSubject)) {

                List<Subject> subjects = SubjectDAO.getAllSubjects();
                req.getServletContext().setAttribute("subjects", subjects);

                return req.getContextPath() + "/secure/subjAndDiff.jsp";
            } else {
                return req.getContextPath() + "/error.jsp";
            }
        } catch (DAOException e) {
            throw new CommandException(e);
        }
    }
}
