package com.saldmy.command.manage.test;

import com.saldmy.command.Command;
import com.saldmy.dao.TestDAO;
import com.saldmy.entity.Difficulty;
import com.saldmy.entity.Subject;
import com.saldmy.entity.Test;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.exception.NoRowsAffectedSQLException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;

/**
 * The class {@code CreateTestCommand} is a realization of {@code Command} interface.
 * It is responsible for the setting up and initialization of the new test in the testing
 * platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class CreateTestCommand implements Command {
    private static final Logger log = LogManager.getLogger(CreateTestCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String filePath = (String) req.getSession().getAttribute("filePath");
        File tempTestFile = new File(filePath);

        Test newTest = (Test) req.getSession().getAttribute("test");
        List<Subject> subjects = (List<Subject>) req.getServletContext().getAttribute("subjects");
        List<Difficulty> difficulties = (List<Difficulty>) req.getServletContext().getAttribute("difficulties");

        String title = req.getParameter("title").trim();

        String subjectId = req.getParameter("subjectId");
        if (subjectId == null) {
            return req.getContextPath() + "/secure/createTest.jsp?invalidSubject=true";
        }
        String difficultyId = req.getParameter("difficultyId");
        if (difficultyId == null) {
            return req.getContextPath() + "/secure/createTest.jsp?invalidDifficulty=true";
        }

        Subject subject = subjects.stream()
                .filter(s -> s.getId() == Integer.parseInt(subjectId))
                .findAny()
                .orElseThrow(CommandException::getStandardCommandException);
        Difficulty difficulty = difficulties.stream()
                .filter(diff -> diff.getId() == Integer.parseInt(
                        difficultyId
                ))
                .findAny()
                .orElseThrow(CommandException::getStandardCommandException);
        String language = req.getParameter("language");
        if (language == null) {
            return req.getContextPath() + "/secure/createTest.jsp?invalidLanguage=true";
        }
        int minPercentage = Integer.parseInt(req.getParameter("minPercentage"));
        int timeLimit = Integer.parseInt(req.getParameter("timeLimit"));

        log.debug("title = " + title);
        log.debug("subject = " + subject);
        log.debug("difficulty = " + difficulty);
        log.debug("language = " + language);
        log.debug("minPercentage = " + minPercentage);
        log.debug("timeLimit = " + timeLimit);

        if (!Validators.validateTestTitle(title)) {
            return req.getContextPath() + "/secure/createTest.jsp?invalidTitle=true";
        }

        if (difficulty == null || subject == null) {
            throw CommandException.getStandardCommandException();
        }

        newTest.setTitle(title);
        newTest.setSubject(subject);
        newTest.setDifficulty(difficulty);
        newTest.setLanguage(language);

        if (minPercentage >= 10 && minPercentage <= 100) {
            newTest.setMinPercentage(minPercentage);
        } else {
            throw new CommandException("Illegal minPercentage value '" + minPercentage + "'");
        }

        if (timeLimit >= 1 && timeLimit <= 500) {
            newTest.setTimeLimit(timeLimit);
        } else {
            throw new CommandException("Illegal timeLimit value '" + timeLimit + "'");
        }

        try {
            Test existingTest = TestDAO.findTestByTitle(title);
            if (existingTest != null) {
                return req.getContextPath() + "/secure/createTest.jsp?duplicateTitle=true";
            }
            if (TestDAO.createTest(newTest)) {
                if (tempTestFile.renameTo(new File(
                        System.getProperty("testsPath") + "test_" + newTest.getId() + ".xml"))) {
                    log.info("tempTestFile moved to tests directory = " + tempTestFile);
                    log.info("Test successfully added to db");
                    req.getSession().removeAttribute("test");
                    req.getSession().removeAttribute("filePath");

                    return req.getContextPath() + "/controller?command=showManageTestsPage&page=1&sort=title&new=true";
                } else {
                    if(!tempTestFile.delete()){
                        throw new CommandException("Error during file deletion: " + filePath);
                    }
                    throw new CommandException("Error during file manipulation: " + filePath);
                }
            } else {
                throw CommandException.getStandardCommandException(new NoRowsAffectedSQLException());
            }
        } catch (DAOException e) {
            throw CommandException.getStandardCommandException(e);
        }
    }

}
