package com.saldmy.command.manage.test;

import com.saldmy.command.Command;
import com.saldmy.dao.TestDAO;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * The class {@code DeleteTestCommand} is a realization of {@code Command} interface.
 * It is responsible for the deleting the test in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class DeleteTestCommand implements Command {
    private static final Logger log = LogManager.getLogger(DeleteTestCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String id = req.getParameter("id");
        try {
            TestDAO.deleteTest(id);
            Files.delete(Paths.get(System.getProperty("testsPath") + "test_" + id + ".xml"));
        } catch (DAOException | IOException e) {
            throw CommandException.getStandardCommandException(e);
        }

        return req.getContextPath() + "/controller?command=showManageTestsPage&page=1&sort=title&new=true";
    }

}
