package com.saldmy.command.manage.test;

import com.saldmy.command.Command;
import com.saldmy.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Arrays;

/**
 * The class {@code DownloadCommand} is a realization of {@code Command} interface.
 * It is responsible for downloading the files from the web application.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class DownloadCommand implements Command {
    private static final Logger log = LogManager.getLogger(DownloadCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String file = req.getParameter("file");
        String path;
        switch (file) {
            case "testSchema": {
                path = System.getProperty("examplesPath") + "test-schema.xsd";
                break;
            }
            case "testExample": {
                path = System.getProperty("examplesPath") + "test-example.xml";
                break;
            }
            case "test": {
                String id = req.getParameter("id");
                path = System.getProperty("testsPath") + "test_" + id + ".xml";
                break;
            }
            default: {
                req.setAttribute("message", "File not found :/");
                throw CommandException.getStandardCommandException();
            }
        }

        log.debug("path = " + path);
        // reads input file from an absolute path
        File downloadFile = new File(path);
        FileInputStream inStream = null;
        try {
            inStream = new FileInputStream(downloadFile);
        } catch (FileNotFoundException e) {
            throw CommandException.getStandardCommandException();
        }

        // obtains ServletContext
        ServletContext context = req.getServletContext();

        // gets MIME type of the file
        String mimeType = context.getMimeType(path);
        if (mimeType == null) {
            // set to binary type if MIME mapping not found
            mimeType = "application/octet-stream";
        }
        log.debug("MIME type: " + mimeType);

        // modifies response
        resp.setContentType(mimeType);
        resp.setContentLength((int) downloadFile.length());

        // forces download
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
        resp.setHeader(headerKey, headerValue);

        // obtains response's output stream
        OutputStream outStream = null;
        try {
            outStream = resp.getOutputStream();
            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            throw CommandException.getStandardCommandException();
        } finally {
            try {
                inStream.close();
                if (outStream != null) {
                    outStream.close();
                }
            } catch (IOException e) {
                log.error(Arrays.toString(e.getStackTrace()));
            }
        }

        return req.getContextPath() + req.getServletPath();
    }

}
