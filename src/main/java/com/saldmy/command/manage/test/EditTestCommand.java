package com.saldmy.command.manage.test;

import com.saldmy.command.Command;
import com.saldmy.dao.TestDAO;
import com.saldmy.entity.Difficulty;
import com.saldmy.entity.Subject;
import com.saldmy.entity.Test;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.util.CommandUtils;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The class {@code EditTestCommand} is a realization of {@code Command} interface.
 * It is responsible for editing the test in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class EditTestCommand implements Command {
    private static final Logger log = LogManager.getLogger(EditTestCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        Test editTest = (Test) req.getSession().getAttribute("test");
        List<Subject> subjects = (List<Subject>) req.getServletContext().getAttribute("subjects");
        List<Difficulty> difficulties = (List<Difficulty>) req.getServletContext().getAttribute("difficulties");

        String title = req.getParameter("title").trim();
        String subjectId = req.getParameter("subjectId");
        String difficultyId = req.getParameter("difficultyId");

        Subject subject = CommandUtils.extractSubject(subjectId, subjects);
        Difficulty difficulty = CommandUtils.extractDifficulty(difficultyId, difficulties);

        String language = req.getParameter("language");
        int minPercentage = Integer.parseInt(req.getParameter("minPercentage"));
        int timeLimit = Integer.parseInt(req.getParameter("timeLimit"));

        log.debug("title = " + title);
        log.debug("subject = " + subject);
        log.debug("difficulty = " + difficulty);
        log.debug("language = " + language);
        log.debug("minPercentage = " + minPercentage);
        log.debug("timeLimit = " + timeLimit);

        if (!Validators.validateTestTitle(title)) {
            return req.getContextPath() + "/secure/editTest.jsp?invalidTitle=true";
        }

        editTest.setTitle(title);
        editTest.setSubject(subject);
        editTest.setDifficulty(difficulty);
        editTest.setLanguage(language);

        if (minPercentage >= 10 && minPercentage <= 100) {
            editTest.setMinPercentage(minPercentage);
        } else {
            throw new CommandException("Illegal minPercentage value '" + minPercentage + "'");
        }

        if (timeLimit >= 1 && timeLimit <= 500) {
            editTest.setTimeLimit(timeLimit);
        } else {
            throw new CommandException("Illegal timeLimit value '" + timeLimit + "'");
        }

        try {
            TestDAO.updateTestProperties(editTest);
            req.getSession().removeAttribute("test");

            return req.getContextPath() + "/controller?command=showManageTestsPage&page=1&sort=title&new=true";
        } catch (DAOException e) {
            throw CommandException.getStandardCommandException(e);
        }
    }

}
