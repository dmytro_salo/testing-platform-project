package com.saldmy.command.manage.test;

import com.saldmy.command.Command;
import com.saldmy.dao.TestDAO;
import com.saldmy.entity.Test;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * The class {@code FilterTestsCommand} is a realization of {@code Command} interface.
 * It is responsible for filtering the results shown in the view layer of the application.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class FilterTestsCommand implements Command {
    private static final Logger log = LogManager.getLogger(FilterTestsCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String returnCommand = req.getParameter("returnCommand");
        String[] subjectParams = req.getParameterValues("subjects");
        String[] difficultyParams = req.getParameterValues("difficulties");
        String[] languageParams = req.getParameterValues("languages");

        log.debug("filterLanguages = " + Arrays.toString(languageParams));
        log.debug("filterSubjectIdParams = " + Arrays.toString(subjectParams));
        log.debug("filterDifficultyIdParams = " + Arrays.toString(difficultyParams));


        List<Test> tests;
        try {
            tests = TestDAO.getFilteredTests(subjectParams, difficultyParams, languageParams);
        } catch (DAOException e) {
            throw new CommandException(e);
        }

        req.getSession().setAttribute("filteredTests", tests);
        req.getSession().setAttribute("filteredSubjects", subjectParams);
        req.getSession().setAttribute("filteredDifficulties", difficultyParams);
        req.getSession().setAttribute("filteredLanguages", languageParams);
        req.getSession().setAttribute("filtered", true);

        return req.getContextPath() + "/controller?command=" + returnCommand + "&page=1&sort=title";
    }
}
