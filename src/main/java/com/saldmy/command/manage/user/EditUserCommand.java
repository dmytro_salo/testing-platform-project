package com.saldmy.command.manage.user;

import com.saldmy.command.Command;
import com.saldmy.dao.UserDAO;
import com.saldmy.entity.User;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.exception.NoRowsAffectedSQLException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.saldmy.util.Validators.*;

/**
 * The class {@code EditUserCommand} is a realization of {@code Command} interface.
 * It is responsible for editing user accounts in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class EditUserCommand implements Command {
    private static final Logger log = LogManager.getLogger(EditUserCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String userParam = req.getParameter("user");

        boolean doneByUser;
        if (Validators.validateBooleanParameter(userParam)) {
            doneByUser = Boolean.parseBoolean(userParam);
        } else {
            return req.getContextPath() + "/badRequest.jsp";
        }

        String id = req.getParameter("id");
        log.debug("id = " + id);

        if (id.equals("1")) {
            return req.getContextPath() + "/accessDenied.jsp?admin=true";
        }

        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String preferredLanguage = req.getParameter("preferredLanguage");

        log.debug("firstName = " + firstName);
        log.debug("lastName = " + lastName);
        log.debug("preferredLanguage = " + preferredLanguage);

        if (firstName == null || lastName == null) {
            log.info("name or password = null");
            return req.getContextPath() + "/badRequest.jsp";
        }

        User writtenUser = new User();
        writtenUser.setFirstName(firstName);
        writtenUser.setLastName(lastName);
        writtenUser.setPreferredLanguage(preferredLanguage);

        if (!validateName(firstName) || !validateName(lastName)) {
            req.getSession().setAttribute("writtenUser", writtenUser);

            if (doneByUser) {
                return req.getContextPath() + "/protected/myProfile.jsp/?edit=true&invalidName=true";
            } else {
                return req.getContextPath() + "/controller?command=showManageUsersPage&invalidName=true";
            }
        }

        req.getSession().removeAttribute("writtenUser");

        try {
            User user = UserDAO.findUserById(req.getParameter("id"));

            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setPreferredLanguage(preferredLanguage);

            log.debug("user to update = " + user);

            if (UserDAO.updateUser(user)) {
                if (doneByUser) {
                    User updatedUser = UserDAO.findUserById(String.valueOf(user.getId()));
                    req.getSession().setAttribute("loggedUser", updatedUser);

                    return req.getContextPath() + "/protected/myProfile.jsp";
                } else {
                    return req.getContextPath() + "/controller?command=showManageUsersPage";
                }
            } else {
                throw CommandException.getStandardCommandException(new NoRowsAffectedSQLException());
            }
        } catch (DAOException e) {
            throw CommandException.getStandardCommandException(e);
        }
    }

}
