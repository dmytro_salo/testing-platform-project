package com.saldmy.command.manage.user;

import com.saldmy.command.Command;
import com.saldmy.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The class {@code LogOutCommand} is a realization of {@code Command} interface.
 * It is responsible for logout functionality in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class LogOutCommand implements Command {
    private static final Logger log = LogManager.getLogger(LogOutCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        req.getSession().setAttribute("loggedUser", null);

        return null;
    }

}
