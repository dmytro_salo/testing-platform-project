package com.saldmy.command.manage.user;

import com.saldmy.command.Command;
import com.saldmy.dao.UserDAO;
import com.saldmy.entity.User;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.util.CommandUtils;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The class {@code SignInCommand} is a realization of {@code Command} interface.
 * It is responsible for signing in to the user account in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class SignInCommand implements Command {
    private static final Logger log = LogManager.getLogger(SignInCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String email = req.getParameter("email").trim();
        String password = req.getParameter("password").trim();

        log.debug("email ==> " + email);
        log.debug("password = " + password);

        if (!Validators.validateEmail(email) && !email.equals("admin")) {
            req.setAttribute("writtenEmail", email);
            return "main.jsp?invalidEmail=true";
        }

        User user;
        try {
            user = UserDAO.findUserByEmail(email);
        } catch (DAOException e) {
            throw CommandException.getStandardCommandException(e);
        }

        log.debug("user in db = " + user);

        if (user != null) {
            String typedPassHash = CommandUtils.getEncryptedHexString(password);
            if (typedPassHash.equals(user.getPassword())) {
                log.debug("User has been logged ==> " + user);
                req.getSession().removeAttribute("writtenEmail");
                req.getSession().setAttribute("loggedUser", user);
                return req.getContextPath() + "/";
            } else {
                req.getSession().setAttribute("writtenEmail", email);
                if (Validators.validateCapsInPassword(password)) {
                    return "main.jsp?capsInPassword=true";
                } else {
                    return "main.jsp?wrongPassword=true";
                }
            }
        } else {
            req.setAttribute("writtenEmail", email);
            return "main.jsp?noUser=" + email;
        }
    }

}
