package com.saldmy.command.manage.user;

import com.saldmy.command.Command;
import com.saldmy.dao.UserDAO;
import com.saldmy.entity.User;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.exception.NoRowsAffectedSQLException;
import com.saldmy.util.CommandUtils;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The class {@code SignUpCommand} is a realization of {@code Command} interface.
 * It is responsible for entering the user account space in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class SignUpCommand implements Command {
    private static final Logger log = LogManager.getLogger(SignUpCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String email = req.getParameter("email").trim();
        String firstName = req.getParameter("firstName").trim();
        String lastName = req.getParameter("lastName").trim();
        String password = req.getParameter("password").trim();
        String repeatPassword = req.getParameter("repeatPassword").trim();
        String localeString = req.getParameter("localeString");

        log.debug("localeString = " + localeString);

        String preferredLanguage;
        switch (localeString) {
            case "uk":
                preferredLanguage = "Ukrainian";
                break;
            case "en":
            default:
                preferredLanguage = "English";
        }

        log.debug("email = " + email);
        log.debug("firstName = " + firstName);
        log.debug("lastName = " + lastName);
        log.debug("password = " + password);
        log.debug("repeatPassword = " + repeatPassword);
        log.debug("preferredLanguage = " + preferredLanguage);

        User writtenUser = new User();
        writtenUser.setEmail(email);
        writtenUser.setFirstName(firstName);
        writtenUser.setLastName(lastName);
        writtenUser.setPreferredLanguage(preferredLanguage);

        try {
            User existingUser = UserDAO.findUserByEmail(email);
            if (existingUser != null) {
                req.getSession().setAttribute("writtenUser", writtenUser);
                return "signUp.jsp?alreadySignedUp=true&loc=" + localeString;
            }
        } catch (DAOException e) {
            throw CommandException.getStandardCommandException(e);
        }

        if (!Validators.validateEmail(email)) {
            log.info("email '" + email + "' is not valid");
            req.getSession().setAttribute("writtenUser", writtenUser);
            return "signUp.jsp?invalidEmail=true&loc=" + localeString;
        }

        if (!Validators.validateName(firstName) || !Validators.validateName(lastName)) {
            log.info("first or last name is not valid");
            req.getSession().setAttribute("writtenUser", writtenUser);
            return "signUp.jsp?invalidName=true&loc=" + localeString;
        }

        if (!Validators.validatePassword(password)){
            log.info("password '" + password + "' is not valid");
            req.getSession().setAttribute("writtenUser", writtenUser);
            return "signUp.jsp?invalidPassword=true&loc=" + localeString;
        }

        if (!password.equals(repeatPassword)) {
            log.info("password '" + password + "' and repeated password '" + "' do not coincide");
            req.getSession().setAttribute("writtenUser", writtenUser);
            return "signUp.jsp?assertPassError=true&loc=" + localeString;
        }

        req.getSession().removeAttribute("writtenUser");

        String encryptedPassword = CommandUtils.getEncryptedHexString(password);

        User newUser = new User();
        newUser.setEmail(email);
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setPreferredLanguage(preferredLanguage);

        log.debug("newUser instance = " + newUser);

        try {
            if (UserDAO.createUser(newUser)) {
                log.info("newUser added to db: " + newUser);
            } else {
                throw CommandException.getStandardCommandException(new NoRowsAffectedSQLException());
            }
        } catch (DAOException e) {
            throw CommandException.getStandardCommandException(e);
        }

        User loggedUser = (User) req.getSession().getAttribute("loggedUser");
        if (loggedUser != null) {
            if (loggedUser.getId() == 1) {
                return req.getContextPath() + "/controller?command=showManageUsersPage";
            } else {
                throw CommandException.getStandardCommandException();
            }
        } else {
            req.getSession().setAttribute("loggedUser", newUser);
            return req.getContextPath() + "/";
        }
    }

}
