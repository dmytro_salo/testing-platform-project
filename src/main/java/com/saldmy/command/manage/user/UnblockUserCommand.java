package com.saldmy.command.manage.user;

import com.saldmy.command.Command;
import com.saldmy.dao.UserDAO;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.exception.NoRowsAffectedSQLException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The class {@code UnblockUserCommand} is a realization of {@code Command} interface.
 * It is responsible for unblocking functionality in the testing platform environment.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class UnblockUserCommand implements Command {
    private static final Logger log = LogManager.getLogger(UnblockUserCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String id = req.getParameter("id");
        if (id.equals("1")) {
            return req.getContextPath() + "/accessDenied.jsp?admin=true";
        }

        if (Validators.validateIntParameter(id)) {
            try {
                if (UserDAO.unblockUser(id)) {
                    return req.getContextPath() + "/controller?command=showManageUsersPage";
                } else {
                    throw CommandException.getStandardCommandException(new NoRowsAffectedSQLException());
                }
            } catch (DAOException e) {
                throw CommandException.getStandardCommandException(e);
            }
        } else {
            throw new CommandException("Invalid id: '" + id + "'");
        }
    }

}
