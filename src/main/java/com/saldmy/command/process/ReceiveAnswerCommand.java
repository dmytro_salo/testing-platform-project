package com.saldmy.command.process;

import com.saldmy.command.Command;
import com.saldmy.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Map;

/**
 * The class {@code ReceiveAnswerCommand} is a realization of {@code Command} interface.
 * It is responsible for receiving AJAX calls of answered questions and write them to
 * an attribute.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class ReceiveAnswerCommand implements Command {
    private static final Logger log = LogManager.getLogger(ReceiveAnswerCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        Integer questionId = Integer.parseInt(req.getParameter("questionId"));
        String[] selectedAnswers = req.getParameterValues("selectedAnswers");

        log.debug("questionId = " + questionId);
        log.debug("selectedAnswers = " + Arrays.toString(selectedAnswers));

        Map<Integer, String[]> answerMap =
                (Map<Integer, String[]>) req.getSession().getAttribute("answerMap");

        answerMap.put(questionId, selectedAnswers);

        boolean allAnswered = true;
        byte unanswered = 0;
        for (Integer k : answerMap.keySet()) {
            if (answerMap.get(k) == null) {
                unanswered++;
            }

            if (unanswered == 2) {
                allAnswered = false;
                break;
            }
        }

        req.getSession().setAttribute("allAnswered", allAnswered);

        return null;
    }
}
