package com.saldmy.command.process;

import com.saldmy.command.Command;
import com.saldmy.entity.Test;
import com.saldmy.exception.CommandException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The class {@code ServiceTestCommand} is a realization of {@code Command} interface.
 * It is responsible for the functioning of the testing procedure.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class ServiceTestCommand implements Command {
    private static final Logger log = LogManager.getLogger(ServiceTestCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        Test startedTest = (Test) req.getSession().getAttribute("startedTest");
        String questionNumberParam = req.getParameter("question");

        int questionNumber;
        if (Validators.validateIntParameter(questionNumberParam)) {
            int tempNumber = Integer.parseInt(questionNumberParam);
            if (tempNumber >= 1 || tempNumber <= startedTest.getNumOfQuestions()) {
                questionNumber = tempNumber;
            } else {
                questionNumber = 1;
            }
        } else {
            questionNumber = 1;
        }

        Test.Question question = startedTest.getQuestions().get(questionNumber - 1);

        req.setAttribute("currentQuestion", question);
        req.setAttribute("currentQuestionNumber", questionNumber);

        return "/secure/question.jsp";
    }
}
