package com.saldmy.command.process;

import com.saldmy.command.Command;
import com.saldmy.dao.TestDAO;
import com.saldmy.entity.Test;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.util.TestXMLParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * The class {@code StartTestCommand} is a realization of {@code Command} interface.
 * It is responsible for setting up the environment to properly initiate testing procedure.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class StartTestCommand implements Command {
    private static final Logger log = LogManager.getLogger(StartTestCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String id = req.getParameter("id");

        Test test;
        try {
            test = TestDAO.findTestById(id);
        } catch (DAOException e) {
            throw new CommandException(e);
        }

        try {
            test.setQuestions(TestXMLParser.parseTestQuestions(id));
        } catch (IOException | SAXException e) {
            throw new CommandException("Error while parsing questions for test with id '" + id + "'");
        }

        test.getQuestions().forEach(q -> Collections.shuffle(q.getAnswers(), new Random(q.hashCode())));
        Collections.shuffle(test.getQuestions(), new Random(System.currentTimeMillis()));

        Map<Integer, String[]> answerMap = new HashMap<>();
        test.getQuestions().forEach(question -> answerMap.put(question.getId(), null));

        int timeLimitMillis = 1000 * 60 * test.getTimeLimit();
        long deadline = System.currentTimeMillis() + timeLimitMillis;

        req.getSession().setAttribute("startedTest", test);
        req.getSession().setAttribute("answerMap", answerMap);
        req.getSession().setAttribute("allAnswered", false);
        req.getSession().setAttribute("deadline", deadline);

        return req.getContextPath() + "/controller?command=serviceTest&question=1";
    }

}
