package com.saldmy.command.process;

import com.saldmy.command.Command;
import com.saldmy.dao.TestDAO;
import com.saldmy.dao.TestResultDAO;
import com.saldmy.entity.Test;
import com.saldmy.entity.TestResult;
import com.saldmy.entity.User;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The class {@code SubmitTestCommand} is a realization of {@code Command} interface.
 * It is responsible for properly ending the testing procedure.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class SubmitTestCommand implements Command {
    private static final Logger log = LogManager.getLogger(SubmitTestCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        User user = (User) req.getSession().getAttribute("loggedUser");
        Test submittedTest = (Test) req.getSession().getAttribute("startedTest");
        Map<Integer, String[]> answerMap =
                (Map<Integer, String[]>) req.getSession().getAttribute("answerMap");

        int rightAnswers = 0;

        for (Integer key : answerMap.keySet()) {
            String[] selectedAnswerIdArray = answerMap.get(key);

            if (selectedAnswerIdArray == null) {
                continue;
            }

            List<Test.Question.Answer> answerList = submittedTest.getQuestions().stream()
                    .filter(q -> q.getId() == key)
                    .findAny()
                    .get().getAnswers();

            List<Integer> correctAnswerIdList = answerList.stream()
                    .filter(Test.Question.Answer::isCorrect)
                    .map(Test.Question.Answer::getId)
                    .sorted()
                    .collect(Collectors.toList());

            List<Integer> selectedAnswerIdList = Arrays.stream(selectedAnswerIdArray)
                    .map(Integer::parseInt)
                    .sorted()
                    .collect(Collectors.toList());

            if (correctAnswerIdList.equals(selectedAnswerIdList)) {
                rightAnswers++;
            }
        }

        float result = (float) rightAnswers / submittedTest.getNumOfQuestions();
        int resultPercent = Math.round(result * 100);
        boolean success = resultPercent >= submittedTest.getMinPercentage();

        log.debug("result = " + result);
        log.debug("resultPercent = " + resultPercent);
        log.debug("success = " + success);


        TestResult testResult = new TestResult();
        testResult.setUserId(user.getId());
        testResult.setTest(submittedTest);
        testResult.setPercent(resultPercent);
        testResult.setSuccess(success);

        try {
            TestResultDAO.createTestResult(testResult);
            if (success) {
                TestDAO.incrementNumOfSubmissionsAndPasses(submittedTest);
            } else {
                TestDAO.incrementNumOfSubmissions(submittedTest);
            }
        } catch (DAOException e) {
            throw new CommandException("Cannot add test result '" + testResult + "' to database");
        }

        req.getSession().setAttribute("submittedTestResult", testResult);

        return req.getContextPath() + "/protected/showResult.jsp";
    }

}
