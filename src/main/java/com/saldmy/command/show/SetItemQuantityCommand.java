package com.saldmy.command.show;

import com.saldmy.command.Command;
import com.saldmy.exception.CommandException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This realization of {@code Command} interface is responsible for changing the quantity of rows
 * per page, where html tables are used.<p>
 *
 * This code is only used by AJAX methods, so the page will not be changed.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class SetItemQuantityCommand implements Command {
    private static final Logger log = LogManager.getLogger(SetItemQuantityCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String quantityParameter = req.getParameter("quantity");
        if (quantityParameter == null || !Validators.validateIntParameter(quantityParameter)) {
            throw new CommandException();
        }

        int quantity = Integer.parseInt(quantityParameter);

        req.getSession().setAttribute("itemsPerPage", quantity);

        return null;
    }

}
