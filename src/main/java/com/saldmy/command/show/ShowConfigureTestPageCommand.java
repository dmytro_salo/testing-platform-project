package com.saldmy.command.show;

import com.saldmy.command.Command;
import com.saldmy.dao.TestDAO;
import com.saldmy.entity.Test;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This realization of {@code Command} interface is responsible for communication with DB and
 * sending the data to the JSP, that configures the {@code Test} object.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class ShowConfigureTestPageCommand implements Command {
    private static final Logger log = LogManager.getLogger(ShowConfigureTestPageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String id = req.getParameter("id");
        if (Validators.validateIntParameter(id)) {
            try {
                Test test = TestDAO.findTestById(id);
                req.getSession().setAttribute("test", test);

                return "/secure/editTest.jsp";
            } catch (DAOException e) {
                throw CommandException.getStandardCommandException(e);
            }
        } else {
            throw CommandException.getStandardCommandException();
        }
    }

}
