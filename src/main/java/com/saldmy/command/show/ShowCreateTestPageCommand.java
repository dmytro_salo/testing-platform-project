package com.saldmy.command.show;

import com.saldmy.command.Command;
import com.saldmy.entity.Test;
import com.saldmy.exception.CommandException;
import com.saldmy.util.TestXMLParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * This realization of {@code Command} interface is responsible for setting up the
 * attributes, which are necessary to JSP to properly create the new {@code Test}
 * object.
 *
 * @author Dmytro Salo
 * @see Command
 */
public class ShowCreateTestPageCommand implements Command {
    private static final Logger log = LogManager.getLogger(ShowCreateTestPageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String filePath = (String) req.getSession().getAttribute("filePath");

        log.debug("filePath = " + filePath);

        try {
            Test test = TestXMLParser.parseTest(filePath);
            for (Test.Question q : test.getQuestions()) {
                long correctAnswers = q.getAnswers().stream().filter(Test.Question.Answer::isCorrect).count();
                if (correctAnswers == 0) {
                    log.info("Validation error. No correct answer in question " + q);
                    Files.delete(Paths.get(filePath));
                    return "/?validationFailed=true";
                }
            }

            req.getSession().setAttribute("test", test);

            return "/secure/createTest.jsp";
        } catch (SAXException | IOException e) {
            throw new CommandException("Cannot parse test '" + filePath + "'", e);
        }
    }

}
