package com.saldmy.command.show;

import com.saldmy.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is a realization of abstract class {@code ShowTestsPageCommand}, which is
 * responsible for showing results in the <li>"Testing"</li> page.
 *
 * @author Dmytro Salo
 * @see ShowTestsPageCommand
 * @see com.saldmy.command.Command
 */
public class ShowFoundTestsPageCommand extends ShowTestsPageCommand {
    private static final Logger log = LogManager.getLogger(ShowFoundTestsPageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        executeTests(req);

        return "/protected/foundTests.jsp";
    }

}
