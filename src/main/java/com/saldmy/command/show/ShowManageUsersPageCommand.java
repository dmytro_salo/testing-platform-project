package com.saldmy.command.show;

import com.saldmy.dao.TestResultDAO;
import com.saldmy.dao.UserDAO;
import com.saldmy.entity.TestResult;
import com.saldmy.entity.User;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class is a realization of abstract class {@code ShowUsersPageCommand}, which is
 * responsible for showing results in the <li>"Manage users"</li> page.
 *
 * @author Dmytro Salo
 * @see ShowUsersPageCommand
 * @see com.saldmy.command.Command
 */
public class ShowManageUsersPageCommand extends ShowUsersPageCommand {
    private static final Logger log = LogManager.getLogger(ShowManageUsersPageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        List<User> users;
        List<TestResult> testResults;
        try {
            users = UserDAO.getAllUsers();
        } catch (DAOException e) {
            throw CommandException.getStandardCommandException(e);
        }

        users = getSortedUsers(users, req);
        users = managePages(users, req);

        req.setAttribute("users", users);

        return "/secure/manageUsers.jsp";
    }

}
