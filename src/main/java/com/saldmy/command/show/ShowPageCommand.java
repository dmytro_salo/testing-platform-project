package com.saldmy.command.show;

import com.saldmy.command.Command;
import com.saldmy.exception.CommandException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * The abstract class {@code ShowPageCommand} is a superclass for all realizations of
 * {@code Command} interface, where page management is needed.
 *
 * @author Dmytro Salo
 * @param <E> the object, which will be returned as list
 * @see Command
 */
public abstract class ShowPageCommand<E> implements Command {
    private static final Logger log = LogManager.getLogger(ShowPageCommand.class);

    public abstract String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException;

    List<E> managePages(List<E> items, HttpServletRequest req) {
        int currentPage = getCurrentPage(req);
        log.debug("Current page = " + currentPage);
        int itemsPerPage = getItemsPerPage(req);

        int pageQuantity = (items.size() % itemsPerPage == 0) ?
                items.size() / itemsPerPage :
                items.size() / itemsPerPage + 1;

        if (currentPage > pageQuantity) {
            currentPage = pageQuantity;
        }

        if (currentPage <= 0) {
            currentPage = 1;
        }

        if (pageQuantity > 1) {
            int startIndex = (currentPage - 1) * itemsPerPage;
            int testsOnPreviousPages = itemsPerPage * (currentPage - 1);
            int endIndex = (currentPage == pageQuantity) ?
                    (testsOnPreviousPages + (items.size()) - testsOnPreviousPages) :
                    (startIndex + itemsPerPage);
            items = items.subList(startIndex, endIndex);
        }

        log.debug("pageQuantity = " + pageQuantity);
        log.debug("currentPage = " + currentPage);

        req.setAttribute("pageQuantity", pageQuantity);
        req.setAttribute("currentPage", currentPage);
        req.setAttribute("itemsPerPage", itemsPerPage);

        return items;
    }

    int getCurrentPage(HttpServletRequest req) {
        String pageParameter = req.getParameter("page");
        log.debug("pageParameter = " + pageParameter);

        if (Validators.validateIntParameter(pageParameter)) {
            return Integer.parseInt(pageParameter);
        } else {
            return 1;
        }
    }

    int getItemsPerPage(HttpServletRequest req) {
        Integer quantityParameter = (Integer) req.getSession().getAttribute("itemsPerPage");
        if (quantityParameter != null) {
            return quantityParameter;
        } else {
            return 10;
        }
    }

    boolean getReversedOrder(HttpServletRequest req) {
        String reversedParameter = req.getParameter("reversed");
        if (Validators.validateBooleanParameter(reversedParameter)) {
            return Boolean.parseBoolean(reversedParameter);
        } else {
            return false;
        }
    }

}
