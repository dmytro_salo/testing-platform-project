package com.saldmy.command.show;

import com.saldmy.entity.TestResult;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;

/**
 * The abstract class {@code ShowTestResultsPageCommand} is a superclass for all
 * realizations of abstract class {@code ShowPageCommand}, that represent {@code TestResult}
 * as the main object to manipulate with.
 *
 * @author Dmytro Salo
 * @see ShowPageCommand
 * @see com.saldmy.command.Command
 */
public abstract class ShowTestResultsPageCommand extends ShowPageCommand<TestResult> {

    void getSortedResults(List<TestResult> testResults, HttpServletRequest req) {
        String sort = req.getParameter("sort");
        boolean reversedOrder = getReversedOrder(req);
        if (sort == null) {
            sort = (String) req.getSession().getAttribute("sort");
            if (sort == null) {
                sort = "email";
                req.getSession().setAttribute("sort", sort);
            }
        } else if (!sort.equals(req.getSession().getAttribute("sort"))) {
            req.getSession().setAttribute("sort", sort);
        }

        switch (sort) {
            case "title":
                if (reversedOrder) {
                    testResults.sort(Comparator.comparing((TestResult t) -> t.getTest().getTitle())
                            .reversed()
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparing((TestResult t) -> t.getTest().getTitle())
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                }
            case "subject":
                if (reversedOrder) {
                    testResults.sort(Comparator.comparingInt((TestResult t) -> t.getTest().getSubject().getId())
                            .reversed()
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparingInt((TestResult t) -> t.getTest().getSubject().getId())
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                }
                break;
            case "difficulty":
                if (reversedOrder) {
                    testResults.sort(Comparator.comparingInt((TestResult t) -> t.getTest().getDifficulty().getLevel())
                            .reversed()
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparingInt((TestResult t) -> t.getTest().getDifficulty().getLevel())
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                }
                break;
            case "language":
                if (reversedOrder) {
                    testResults.sort(Comparator.comparing((TestResult t) -> t.getTest().getLanguage())
                            .reversed()
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparing((TestResult t) -> t.getTest().getLanguage())
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                }
                break;
            case "numOfQuestions":
                if (reversedOrder) {
                    testResults.sort(Comparator.comparingInt((TestResult t) -> t.getTest().getNumOfQuestions())
                            .reversed()
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparingInt((TestResult t) -> t.getTest().getNumOfQuestions())
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                }
                break;
            case "timeLimit":
                if (reversedOrder) {
                    testResults.sort(Comparator.comparingInt((TestResult t) -> t.getTest().getTimeLimit())
                            .reversed()
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparingInt((TestResult t) -> t.getTest().getTimeLimit())
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                }
                break;
            case "minPercentage":
                if (reversedOrder) {
                    testResults.sort(Comparator.comparingInt((TestResult t) -> t.getTest().getMinPercentage())
                            .reversed()
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparingInt((TestResult t) -> t.getTest().getMinPercentage())
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                }
                break;
            case "resultPercentage":
                if (reversedOrder) {
                    testResults.sort(Comparator.comparingInt(TestResult::getPercent)
                            .reversed()
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparingInt(TestResult::getPercent)
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                }
                break;
            case "verdict":
                if (reversedOrder) {
                    testResults.sort(Comparator.comparing(TestResult::isSuccess)
                            .reversed()
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparing(TestResult::isSuccess)
                            .thenComparing(TestResult::getSubmissionDate).reversed());
                }
                break;
            case "submissionDate":
                if (reversedOrder) {
                    testResults.sort(Comparator.comparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparing(TestResult::getSubmissionDate));
                }
                break;
            default:
                sort = "submissionDate";
                req.getSession().setAttribute("sort", sort);
                if (reversedOrder) {
                    testResults.sort(Comparator.comparing(TestResult::getSubmissionDate).reversed());
                } else {
                    testResults.sort(Comparator.comparing(TestResult::getSubmissionDate));
                }
        }

        req.setAttribute("sort", sort);
    }
}
