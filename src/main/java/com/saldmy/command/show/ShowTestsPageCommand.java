package com.saldmy.command.show;

import com.saldmy.dao.TestDAO;
import com.saldmy.entity.Difficulty;
import com.saldmy.entity.Subject;
import com.saldmy.entity.Test;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * The abstract class {@code ShowTestsPageCommand} is a superclass for all
 * realizations of abstract class {@code ShowPageCommand}, that represent {@code Test}
 * as the main object to manipulate with.
 *
 * @author Dmytro Salo
 * @see ShowPageCommand
 * @see com.saldmy.command.Command
 */
public abstract class ShowTestsPageCommand extends ShowPageCommand<Test> {

    public abstract String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException;

    void executeTests(HttpServletRequest req) throws CommandException {
        Object filteredTestsObj = req.getSession().getAttribute("filteredTests");

        List<Test> tests;
        if (filteredTestsObj != null && !Objects.equals(req.getParameter("new"), "true")) {
            tests = (List<Test>) filteredTestsObj;
        } else {
            try {
                tests = TestDAO.getAllTests();
                List<Subject> subjects =
                        (List<Subject>) req.getServletContext().getAttribute("subjects");
                List<Difficulty> difficulties =
                        (List<Difficulty>) req.getServletContext().getAttribute("difficulties");

                String[] subjectIds = subjects.stream()
                        .map(s -> String.valueOf(s.getId()))
                        .toArray(String[]::new);
                String[] difficultyIds = difficulties.stream()
                        .map(d -> String.valueOf(d.getId()))
                        .toArray(String[]::new);

                req.getSession().setAttribute("filteredTests", tests);
                req.getSession().setAttribute("filteredSubjects", subjectIds);
                req.getSession().setAttribute("filteredDifficulties", difficultyIds);
                req.getSession().setAttribute("filteredLanguages", new String[]{"English", "Ukrainian"});
                req.getSession().setAttribute("filtered", false);
            } catch (DAOException e) {
                throw new CommandException(e);
            }
        }

        sortTests(tests, req);
        tests = managePages(tests, req);

        req.setAttribute("tests", tests);
    }

    private void sortTests(List<Test> tests, HttpServletRequest req) {
        boolean reversedOrder = getReversedOrder(req);
        String sort = req.getParameter("sort");
        if (sort == null) {
            sort = (String) req.getSession().getAttribute("sort");
            if (sort == null) {
                sort = "title";
                req.getSession().setAttribute("sort", sort);
            }
        } else if (!sort.equals(req.getSession().getAttribute("sort"))) {
            req.getSession().setAttribute("sort", sort);
        }

        switch (sort) {
            case "title":
                if (reversedOrder) {
                    tests.sort(Comparator.comparing(Test::getTitle).reversed());
                } else {
                    tests.sort(Comparator.comparing(Test::getTitle));
                }
                break;
            case "subject":
                if (reversedOrder) {
                    tests.sort(Comparator.comparing(this::getSubjectName).reversed().thenComparing(Test::getTitle));
                } else {
                    tests.sort(Comparator.comparing(this::getSubjectName).thenComparing(Test::getTitle));
                }
                break;
            case "difficulty":
                if (reversedOrder) {
                    tests.sort(Comparator.comparingInt(this::getDifficultyLevel).reversed()
                            .thenComparing(Test::getTitle));
                } else {
                    tests.sort(Comparator.comparingInt(this::getDifficultyLevel)
                            .thenComparing(Test::getTitle));
                }
                break;
            case "language":
                if (reversedOrder) {
                    tests.sort(Comparator.comparing(Test::getLanguage).reversed().thenComparing(Test::getTitle));
                } else {
                    tests.sort(Comparator.comparing(Test::getLanguage).thenComparing(Test::getTitle));
                }
                break;
            case "numOfQuestions":
                if (reversedOrder) {
                    tests.sort(Comparator.comparingInt(Test::getNumOfQuestions).reversed()
                            .thenComparing(Test::getTitle));
                } else {
                    tests.sort(Comparator.comparingInt(Test::getNumOfQuestions)
                            .thenComparing(Test::getTitle));
                }
                break;
            case "timeLimit":
                if (reversedOrder) {
                    tests.sort(Comparator.comparingInt(Test::getTimeLimit).reversed()
                            .thenComparing(Test::getTitle));
                } else {
                    tests.sort(Comparator.comparingInt(Test::getTimeLimit)
                            .thenComparing(Test::getTitle));
                }
                break;
            case "minPercentage":
                if (reversedOrder) {
                    tests.sort(Comparator.comparingInt(Test::getMinPercentage).reversed()
                            .thenComparing(Test::getTitle));
                } else {
                    tests.sort(Comparator.comparingInt(Test::getMinPercentage)
                            .thenComparing(Test::getTitle));
                }
                break;
            case "numOfSubmissions":
                if (reversedOrder) {
                    tests.sort(Comparator.comparingInt(Test::getNumOfSubmissions).reversed()
                            .thenComparing(Test::getTitle));
                } else {
                    tests.sort(Comparator.comparingInt(Test::getNumOfSubmissions)
                            .thenComparing(Test::getTitle));
                }
                break;
            case "numOfPasses":
                if (reversedOrder) {
                    tests.sort(Comparator.comparingInt(Test::getNumOfPasses).reversed()
                            .thenComparing(Test::getTitle));
                } else {
                    tests.sort(Comparator.comparingInt(Test::getNumOfPasses)
                            .thenComparing(Test::getTitle));
                }
                break;
            case "creationDate":
                if (reversedOrder) {
                    tests.sort(Comparator.comparing(Test::getCreationDateObject).reversed()
                            .thenComparing(Test::getTitle));
                } else {
                    tests.sort(Comparator.comparing(Test::getCreationDateObject)
                            .thenComparing(Test::getTitle));
                }
                break;
            case "passRate":
                List<Test> withoutPassRate = tests.stream()
                        .filter(test -> test.getPassRate().equals("-"))
                        .collect(Collectors.toList());

                withoutPassRate.forEach(tests::remove);

                if (reversedOrder) {
                    tests.sort(Comparator.comparingDouble(this::getPassRate).reversed());
                    tests.addAll(withoutPassRate);
                } else {
                    tests.sort(Comparator.comparingDouble(this::getPassRate));
                    tests.addAll(0, withoutPassRate);
                }
                break;
            default:
                sort = "title";
                req.getSession().setAttribute("sort", sort);
                if (reversedOrder) {
                    tests.sort(Comparator.comparing(Test::getTitle).reversed());
                } else {
                    tests.sort(Comparator.comparing(Test::getTitle));
                }
        }

        req.setAttribute("sort", sort);
    }



    private double getPassRate(Test test) {
        return Double.parseDouble(test.getPassRate());
    }

    private int getDifficultyLevel(Test t) {
        return t.getDifficulty().getLevel();
    }

    private String getSubjectName(Test t) {
        return t.getSubject().getName();
    }

}
