package com.saldmy.command.show;

import com.saldmy.dao.TestResultDAO;
import com.saldmy.dao.UserDAO;
import com.saldmy.entity.TestResult;
import com.saldmy.entity.User;
import com.saldmy.exception.CommandException;
import com.saldmy.exception.DAOException;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class is a realization of abstract class {@code ShowTestResultsPageCommand}, which is
 * responsible for showing results in the <li>"My statistics"</li> page.
 *
 * @author Dmytro Salo
 * @see ShowTestResultsPageCommand
 * @see com.saldmy.command.Command
 */
public class ShowUserTestResultsPageCommand extends ShowTestResultsPageCommand {
    private static final Logger log = LogManager.getLogger(ShowUserTestResultsPageCommand.class);

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        log.trace("~~~");

        String userId = req.getParameter("userId");
        User loggedUser = (User) req.getSession().getAttribute("loggedUser");
        User user;

        if (Validators.validateIntParameter(userId)) {
            if (!String.valueOf(loggedUser.getId()).equals(userId) && loggedUser.getId() != 1) {
                return "accessDenied.jsp?preferences=true";
            }
            try {
                user = UserDAO.findUserById(userId);
            } catch (DAOException e) {
                throw new CommandException(e);
            }
        } else {
            user = loggedUser;
        }

        List<TestResult> testResults;
        try {
            testResults = TestResultDAO.getTestResultsByUserId(user.getId());
        } catch (DAOException e) {
            throw new CommandException(e);
        }

        getSortedResults(testResults, req);
        testResults = managePages(testResults, req);

        req.setAttribute("user", user);
        req.setAttribute("testResults", testResults);

        return "/protected/testResults.jsp";
    }

}
