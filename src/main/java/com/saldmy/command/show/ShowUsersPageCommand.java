package com.saldmy.command.show;

import com.saldmy.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The abstract class {@code ShowUsersPageCommand} is a superclass for all
 * realizations of abstract class {@code ShowPageCommand}, that represent {@code User}
 * as the main object to manipulate with.
 *
 * @author Dmytro Salo
 * @see ShowPageCommand
 * @see com.saldmy.command.Command
 */
public abstract class ShowUsersPageCommand extends ShowPageCommand<User> {

    List<User> getSortedUsers(List<User> users, HttpServletRequest req) {
        String sort = req.getParameter("sort");
        boolean reversedOrder = getReversedOrder(req);
        if (sort == null) {
            sort = (String) req.getSession().getAttribute("sort");
            if (sort == null) {
                sort = "email";
                req.getSession().setAttribute("sort", sort);
            }
        } else if (!sort.equals(req.getSession().getAttribute("sort"))) {
            req.getSession().setAttribute("sort", sort);
        }

        switch (sort) {
            case "email":
                if (reversedOrder) {
                    users.sort(Comparator.comparing(User::getEmail).reversed());
                } else {
                    users.sort(Comparator.comparing(User::getEmail));
                }
                break;
            case "firstName":
                users = reversedOrder ?
                        users.stream()
                                .sorted(Comparator.comparing(User::getFirstName).reversed().thenComparing(User::getEmail))
                                .collect(Collectors.toList()) :
                        users.stream()
                                .sorted(Comparator.comparing(User::getFirstName).thenComparing(User::getEmail))
                                .collect(Collectors.toList());
                break;
            case "lastName":
                users = reversedOrder ?
                        users.stream()
                                .sorted(Comparator.comparing(User::getLastName).reversed()
                                        .thenComparing(User::getEmail))
                                .collect(Collectors.toList()) :
                        users.stream()
                                .sorted(Comparator.comparing(User::getLastName)
                                        .thenComparing(User::getEmail))
                                .collect(Collectors.toList());
                break;
            case "role":
                users = reversedOrder ?
                        users.stream()
                                .sorted(Comparator.comparing(User::getRole).reversed().thenComparing(User::getEmail))
                                .collect(Collectors.toList()) :
                        users.stream()
                                .sorted(Comparator.comparing(User::getRole).thenComparing(User::getEmail))
                                .collect(Collectors.toList());
                break;
            case "status":
                users = reversedOrder ?
                        users.stream()
                                .sorted(Comparator.comparing(User::getStatus).reversed().thenComparing(User::getEmail))
                                .collect(Collectors.toList()) :
                        users.stream()
                                .sorted(Comparator.comparing(User::getStatus).thenComparing(User::getEmail))
                                .collect(Collectors.toList());
                break;
            case "language":
                users = reversedOrder ?
                        users.stream()
                                .sorted(Comparator.comparing(User::getPreferredLanguage).reversed()
                                        .thenComparing(User::getEmail))
                                .collect(Collectors.toList()) :
                        users.stream()
                                .sorted(Comparator.comparing(User::getPreferredLanguage)
                                        .thenComparing(User::getEmail))
                                .collect(Collectors.toList());
                break;
            case "averagePercentage":
                List<User> withoutPassRate = users.stream()
                        .filter(test -> test.getAveragePercentage().equals("-"))
                        .collect(Collectors.toList());

                withoutPassRate.forEach(users::remove);

                if (reversedOrder) {
                    users.sort(Comparator.comparingDouble(this::getAveragePercentage).reversed());
                    users.addAll(withoutPassRate);
                } else {
                    users.sort(Comparator.comparingDouble(this::getAveragePercentage));
                    users.addAll(0, withoutPassRate);
                }
                break;
            case "registrationDate":
                users = reversedOrder ?
                        users.stream()
                                .sorted(Comparator.comparing(User::getRegistrationDate).reversed()
                                        .thenComparing(User::getEmail))
                                .collect(Collectors.toList()) :
                        users.stream()
                                .sorted(Comparator.comparing(User::getRegistrationDate)
                                        .thenComparing(User::getEmail))
                                .collect(Collectors.toList());
                break;
            default:
                sort = "email";
                req.getSession().setAttribute("sort", sort);
                if (reversedOrder) {
                    users.sort(Comparator.comparing(User::getEmail).reversed());
                } else {
                    users.sort(Comparator.comparing(User::getEmail));
                }
        }

        req.setAttribute("sort", sort);

        return users;
    }

    private double getAveragePercentage(User user) {
        return Double.parseDouble(user.getAveragePercentage());
    }
}
