package com.saldmy.controller;

import com.saldmy.command.Command;
import com.saldmy.command.CommandContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Arrays;

/**
 * The {@code Controller} class is the main servlet, that is responsible for <li>front controller</li>
 * architecture. <p>
 *
 * Its functionality is determined by the instance of {@code Command} class, which is injected
 * by the URI parameter. <p>
 *
 * The proper error handling is also the responsibility of this class.
 *
 * @author Dmytro Salo
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {
    private static final Logger log = LogManager.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        log.trace("===");
        log.debug("Controller.doGet");

        String address = executeCommand(req, resp);

        log.debug("forward address = " + address);
        log.trace("===");

        if (address != null) {
            req.getRequestDispatcher(address).forward(req, resp);
        } else {
            resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        log.trace("===");
        log.debug("Controller.doPost");

        String address = executeCommand(req, resp);

        log.debug("redirect address = " + address);
        log.trace("===");

        if (address != null) {
            resp.sendRedirect(address);
        } else {
            resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }
    }

    private String executeCommand(HttpServletRequest req, HttpServletResponse resp) {
        String commandName = req.getParameter("command");
        log.debug("commandName = " + commandName);

        Command command = CommandContainer.getCommand(commandName);
        if (command == null) {
            return "/badRequest.jsp";
        }

        String address;
        try {
            address = command.execute(req, resp);
        } catch (Exception ex) {
            log.error(Arrays.toString(ex.getStackTrace()));
            ex.printStackTrace();
            address = "error.jsp";
        }
        return address;
    }

}
