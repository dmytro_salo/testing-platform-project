package com.saldmy.controller;

import com.saldmy.dao.TestDAO;
import com.saldmy.entity.Test;
import com.saldmy.exception.DAOException;
import com.saldmy.util.TestXMLParser;
import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Objects;

import static com.saldmy.util.CommandUtils.getFileName;

/**
 * The {@code TestUpdateServlet} class is a secondary servlet, which is responsible for test
 * updating functionality, which needs {@code @MultipartConfig} to be present.
 *
 * @author Dmytro Salo
 */
@WebServlet("/upload/update")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1,  // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 15, // 15 MB
        location = "D:\\epam\\testing-platform-downloads\\temp"
)
public class TestUpdateServlet extends HttpServlet {
    private static final Logger log = LogManager.getLogger(TestUpdateServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.trace("===");
        log.debug("TestUpdateServlet.doPost");

        String id = req.getParameter("id");
        if (id == null) {
            resp.sendRedirect(req.getContextPath() + "error.jsp");
        }

        final String folderPath = System.getProperty("tempPath");
        final Part filePart = req.getPart("file");
        final String actualFileName = getFileName(filePart);
        final String requiredFileName = "test_" + id + ".xml";
        final String filePath = folderPath + actualFileName;

        log.debug("actualFileName = " + actualFileName);
        log.debug("requiredFileName = " + requiredFileName);

        if (Objects.equals(actualFileName, requiredFileName)) {
            try (OutputStream out = new FileOutputStream(filePath);
                 InputStream fileContent = filePart.getInputStream()) {
                int read;
                final byte[] bytes = new byte[1024];
                while ((read = fileContent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
            }

            Test test = null;
            try {
                Validators.validateTest(filePath);
                test = TestXMLParser.parseTest(filePath);
                for (Test.Question q : test.getQuestions()) {
                    long correctAnswers = q.getAnswers().stream().filter(Test.Question.Answer::isCorrect).count();
                    if (correctAnswers == 0) {
                        throw new SAXException("Validation error. No correct answer in question " + q);
                    }
                }
                test.setId(Integer.parseInt(id));
            } catch (SAXException e) {
                log.info("validation error: " + filePath);
                resp.sendRedirect(req.getContextPath() +
                        "/controller?command=showManageTestsPage&invalidTestError=true");
                Files.delete(Paths.get(filePath));
                return;
            }

            try {
                TestDAO.updateQuestions(test);
                Files.move(
                        Paths.get(filePath),
                        Paths.get(System.getProperty("testsPath") + requiredFileName),
                        StandardCopyOption.REPLACE_EXISTING
                );

                resp.sendRedirect(req.getContextPath() +
                        "/controller?command=showManageTestsPage&successfulUpdate=true");
            } catch (DAOException e) {
                log.error(Arrays.toString(e.getStackTrace()));
                resp.sendRedirect(req.getContextPath() + "/error.jsp");
            }
        } else {
            resp.sendRedirect(req.getContextPath() +
                    "/controller?command=showManageTestsPage&invalidNameError=true");
        }
    }

}
