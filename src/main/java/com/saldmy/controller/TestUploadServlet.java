package com.saldmy.controller;

import com.saldmy.util.Validators;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.saldmy.util.CommandUtils.getFileName;

/**
 * The {@code TestUploadServlet} class is a secondary servlet, which is responsible for test
 * uploading functionality, which needs {@code @MultipartConfig} to be present.
 *
 * @author Dmytro Salo
 */
@WebServlet("/upload/test")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1,  // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 15, // 15 MB
        location = "D:\\epam\\testing-platform-downloads\\temp"
)
public class TestUploadServlet extends HttpServlet {
    private static final Logger log = LogManager.getLogger(TestUploadServlet.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.trace("===");
        log.debug("TestUploadServlet.doPost");

        // Create path components to save the file
        final String folderPath = System.getProperty("tempPath");
        final Part filePart = request.getPart("file");
        final String fileName = getFileName(filePart);
        final String filePath = folderPath + fileName;

        log.info("fileName = " + fileName);

        try (OutputStream out = new FileOutputStream(filePath);
             InputStream fileContent = filePart.getInputStream()) {

            int read;
            final byte[] bytes = new byte[1024];
            while ((read = fileContent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            Validators.validateTest(filePath);
            log.info("File {} being uploaded to {}", new Object[] {fileName, folderPath});

            request.getSession().setAttribute("filePath", filePath);
            response.sendRedirect(request.getContextPath() + "/controller?command=showCreateTestPage");

        } catch (FileNotFoundException fne) {
            log.error("Problems during file upload. Error: {}", new Object[] {fne.getMessage()});
            request.getSession().setAttribute("message",
                    "An error occurred during file upload =(. Please, try to upload file again");
            response.sendRedirect(request.getContextPath() + "/error.jsp");
        } catch (SAXException e) {
            log.warn("Validation of test file '" + fileName + "' failed");
            response.sendRedirect(request.getContextPath() +
                    "/?validationFailed=true");
            Files.delete(Paths.get(filePath));
        }
    }

}

