package com.saldmy.dao;

import com.saldmy.entity.Difficulty;
import com.saldmy.exception.DAOException;
import com.saldmy.util.DBManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.saldmy.util.DAOUtils.close;
import static com.saldmy.util.SQLQueries.DifficultyQueries.*;

/**
 * The {@code DifficultyDAO} class provides CRUD functionality to {@code Difficulty} JavaBean class.
 *
 * @author Dmytro Salo
 * @see Difficulty
 */
public abstract class DifficultyDAO {
    private static final Logger log = LogManager.getLogger(DifficultyDAO.class);

    public static List<Difficulty> getAllDifficulties() throws DAOException {
        log.debug("DifficultyDAO.getAllDifficulties");
        List<Difficulty> difficulties = new ArrayList<>();

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_GET_ALL_DIFFICULTIES);
            while (rs.next()) {
                difficulties.add(extractDifficulty(rs));
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot get difficulties from db", e);
        } finally {
            close(con, stmt, rs);
        }

        difficulties.sort(Comparator.comparingInt(Difficulty::getLevel));

        return difficulties;
    }

    public static Difficulty findDifficultyById(int id, Connection con) throws DAOException {
        log.debug("DifficultyDAO.findDifficultyById");
        log.debug("id = " + id);

        Difficulty difficulty = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            pstmt = con.prepareStatement(SQL_FIND_DIFFICULTY_BY_ID);

            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                difficulty = extractDifficulty(rs);
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot find difficulty with id '" + id + "' in database", e);
        } finally {
            close(pstmt, rs);
        }

        return difficulty;
    }

    public static boolean createDifficulty(Difficulty difficulty) throws DAOException {
        log.debug("DifficultyDAO.createTest");
        log.debug("difficulty = " + difficulty);

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_CREATE_DIFFICULTY, Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, difficulty.getName());
            pstmt.setString(2, difficulty.getNameUk());
            pstmt.setInt(3, difficulty.getLevel());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    difficulty.setId(rs.getInt(1));
                }
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new DAOException("cannot insert difficulty: '" + difficulty + "' into db", e);
        } finally {
            close(con, pstmt, rs);
        }
    }

    public static boolean updateDifficulty(Difficulty difficulty) throws DAOException {
        log.debug("DifficultyDAO.updateDifficulty");

        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_UPDATE_DIFFICULTY);

            pstmt.setString(1, difficulty.getName());
            pstmt.setString(2, difficulty.getNameUk());
            pstmt.setInt(3, difficulty.getLevel());
            pstmt.setInt(4, difficulty.getId());

            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Cannot update difficulty in db: " + difficulty, e);
        } finally {
            close(con, pstmt);
        }
    }

    public static boolean deleteDifficulty(String id) throws DAOException {
        log.debug("DifficultyDAO.deleteDifficulty");
        log.debug("difficulty to delete = id: " + id);

        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = DBManager.getInstance().getConnection();
            pstmt = connection.prepareStatement(SQL_DELETE_DIFFICULTY);

            pstmt.setString(1, id);

            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete difficulty with id: " + id + " from db", e);
        } finally {
            close(connection, pstmt);
        }
    }

    private static Difficulty extractDifficulty(ResultSet rs) throws DAOException {
        log.debug("DifficultyDAO.extractDifficulty");
        Difficulty difficulty = new Difficulty();
        try {
            difficulty.setId(rs.getInt("id"));
            difficulty.setName(rs.getString("name"));
            difficulty.setNameUk(rs.getString("name_uk"));
            difficulty.setLevel(rs.getInt("level"));
        } catch (SQLException e) {
            throw new DAOException("Cannot extract difficulty from ResultSet" + rs, e);
        }
        log.debug("extracted difficulty = " + difficulty);

        return difficulty;
    }

}
