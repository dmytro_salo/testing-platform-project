package com.saldmy.dao;

import com.saldmy.entity.Subject;
import com.saldmy.exception.DAOException;
import com.saldmy.util.DBManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.saldmy.util.DAOUtils.close;
import static com.saldmy.util.SQLQueries.SubjectQueries.*;

/**
 * The {@code SubjectDAO} class provides CRUD functionality to {@code Subject} JavaBean class.
 *
 * @author Dmytro Salo
 * @see Subject
 */
public abstract class SubjectDAO {
    private static final Logger log = LogManager.getLogger(SubjectDAO.class);

    public static List<Subject> getAllSubjects() throws DAOException {
        log.debug("SubjectDAO.getAllSubjects");
        List<Subject> tests = new ArrayList<>();

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_GET_ALL_SUBJECTS);
            while (rs.next()) {
                tests.add(extractSubject(rs));
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot get subjects from db", e);
        } finally {
            close(con, stmt, rs);
        }

        return tests;
    }

    public static Subject findSubjectById(int id, Connection con) throws DAOException {
        log.debug("SubjectDAO.findSubjectById");
        log.debug("id = " + id);

        Subject subject = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            pstmt = con.prepareStatement(SQL_FIND_SUBJECT_BY_ID);

            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                subject = extractSubject(rs);
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot find subject with id '" + id + "' in database", e);
        } finally {
            close(pstmt, rs);
        }

        return subject;
    }

    public static boolean createSubject(Subject subject) throws DAOException {
        log.debug("SubjectDAO.createSubject");
        log.debug("subject = " + subject);

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_CREATE_SUBJECT, Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, subject.getName());
            pstmt.setString(2, subject.getNameUk());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    subject.setId(rs.getInt(1));
                }
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new DAOException("cannot insert subject: '" + subject + "' into db", e);
        } finally {
            close(con, pstmt, rs);
        }
    }

    public static boolean updateSubject(Subject subject) throws DAOException {
        log.debug("SubjectDAO.updateSubject");

        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_UPDATE_SUBJECT);

            pstmt.setString(1, subject.getName());
            pstmt.setString(2, subject.getNameUk());
            pstmt.setInt(3, subject.getId());

            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Cannot update subject in db: " + subject, e);
        } finally {
            close(con, pstmt);
        }
    }

    public static boolean deleteSubject(String id) throws DAOException {
        log.debug("SubjectDAO.deleteSubject");
        log.debug("subject id to delete: " + id);

        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = DBManager.getInstance().getConnection();
            pstmt = connection.prepareStatement(SQL_DELETE_SUBJECT);

            pstmt.setString(1, id);

            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete subject with id: " + id + " from db", e);
        } finally {
            close(connection, pstmt);
        }
    }

    private static Subject extractSubject(ResultSet rs) throws DAOException {
        log.debug("SubjectDAO.extractDifficulty");
        Subject subject = new Subject();
        try {
            subject.setId(rs.getInt("id"));
            subject.setName(rs.getString("name"));
            subject.setNameUk(rs.getString("name_uk"));
        } catch (SQLException e) {
            throw new DAOException("Cannot extract subject from ResultSet" + rs, e);
        }
        log.debug("extracted subject = " + subject);

        return subject;
    }

}
