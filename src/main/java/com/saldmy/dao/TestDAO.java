package com.saldmy.dao;

import com.saldmy.entity.Difficulty;
import com.saldmy.entity.Subject;
import com.saldmy.entity.Test;
import com.saldmy.exception.DAOException;
import com.saldmy.util.DBManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.saldmy.util.DAOUtils.close;
import static com.saldmy.util.SQLQueries.TestQueries.*;

/**
 * The {@code TestDAO} class provides CRUD functionality to {@code Test} JavaBean class.
 *
 * @author Dmytro Salo
 * @see Test
 */
public abstract class TestDAO {
    private static final Logger log = LogManager.getLogger(TestDAO.class);

    public static List<Test> getAllTests() throws DAOException {
        log.debug("TestDAO.getAllTests");

        List<Test> tests = new ArrayList<>();

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_GET_ALL_TESTS);
            Test test;
            while (rs.next()) {
                test = extractTest(rs, con);
                tests.add(test);
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot get tests from db", e);
        } finally {
            close(con, stmt, rs);
        }

        return tests;
    }

    public static Test findTestByTitle(String title) throws DAOException {
        log.debug("TestDAO.findTestByTitle");
        log.debug("title = " + title);

        Test test = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_FIND_TEST_BY_TITLE);

            pstmt.setString(1, title);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                test = extractTest(rs, con);
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot find test by title '" + title + "'", e);
        } finally {
            close(con, pstmt, rs);
        }

        return test;
    }

    public static Test findTestById(String id) throws DAOException {
        log.debug("TestDAO.findTestById");
        log.debug("id = " + id);

        Test test = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_FIND_TEST_BY_ID);

            pstmt.setString(1, id);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                test = extractTest(rs, con);
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot find test with id '" + id + "' in database", e);
        } finally {
            close(con, pstmt, rs);
        }

        return test;
    }

    public static boolean createTest(Test test) throws DAOException {
        log.debug("TestDAO.createTest");
        log.debug("test = " + test);

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            pstmt = con.prepareStatement(SQL_CREATE_TEST, Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, test.getTitle());
            pstmt.setInt(2, test.getSubject().getId());
            pstmt.setInt(3, test.getDifficulty().getId());
            pstmt.setString(4, test.getLanguage());
            pstmt.setInt(5, test.getNumOfQuestions());
            pstmt.setInt(6, test.getTimeLimit());
            pstmt.setInt(7, test.getMinPercentage());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    test.setId(rs.getInt(1));
                }
                con.commit();
                return true;
            }
            con.rollback();
            return false;
        } catch (SQLException e) {
            throw new DAOException("cannot insert test: '" + test + "' into db", e);
        } finally {
            close(con, pstmt, rs);
        }
    }

    public static void updateTestProperties(Test test) throws DAOException {
        log.debug("TestDAO.updateTestProperties");

        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_UPDATE_TEST_PROPERTIES);

            pstmt.setString(1, test.getTitle());
            pstmt.setInt(2, test.getSubject().getId());
            pstmt.setInt(3, test.getDifficulty().getId());
            pstmt.setString(4, test.getLanguage());
            pstmt.setInt(5, test.getTimeLimit());
            pstmt.setInt(6, test.getMinPercentage());
            pstmt.setInt(7, test.getId());

            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Cannot update test in db: " + test, e);
        } finally {
            close(con, pstmt);
        }
    }

    public static boolean deleteTest(String id) throws DAOException {
        log.debug("TestDAO.deleteTest");
        log.debug("test to delete = id: " + id);

        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = DBManager.getInstance().getConnection();
            pstmt = connection.prepareStatement(SQL_DELETE_TEST);

            pstmt.setString(1, id);

            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete test with id: " + id + " from db", e);
        } finally {
            close(connection, pstmt);
        }
    }

    public static boolean incrementNumOfSubmissions(Test test) throws DAOException {
        log.debug("TestDAO.updateNumOfSubmissions");
        log.debug("id = " + test.getId());

        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = DBManager.getInstance().getConnection();
            pstmt = connection.prepareStatement(SQL_UPDATE_TEST_NUM_OF_SUBMISSIONS);

            pstmt.setInt(1, test.getNumOfSubmissions() + 1);
            pstmt.setInt(2, test.getId());

            if (pstmt.executeUpdate() > 0) {
                test.setNumOfSubmissions(test.getNumOfSubmissions() + 1);
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new DAOException("Cannot set update num_of_submissions for user test id: "
                    + test.getId() + " in db", e);
        } finally {
            close(connection, pstmt);
        }
    }

    public static boolean incrementNumOfSubmissionsAndPasses(Test test) throws DAOException {
        log.debug("TestDAO.updateNumOfSubmissionsAndPasses");
        log.debug("id = " + test.getId());

        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = DBManager.getInstance().getConnection();
            pstmt = connection.prepareStatement(SQL_UPDATE_TEST_NUM_OF_SUBMISSIONS_AND_PASSES);

            pstmt.setInt(1, test.getNumOfSubmissions() + 1);
            pstmt.setInt(2, test.getNumOfPasses() + 1);
            pstmt.setInt(3, test.getId());

            if (pstmt.executeUpdate() > 0) {
                test.setNumOfSubmissions(test.getNumOfSubmissions() + 1);
                test.setNumOfPasses(test.getNumOfPasses() + 1);
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new DAOException("Cannot set update num_of_submissions and num_of_passes for test with id: "
                    + test.getId() + " in db", e);
        } finally {
            close(connection, pstmt);
        }
    }

    public static void updateQuestions(Test test) throws DAOException {
        log.debug("TestDAO.updateQuestions");

        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = DBManager.getInstance().getConnection();
            pstmt = connection.prepareStatement(SQL_UPDATE_TEST_QUESTIONS);

            pstmt.setInt(1, test.getNumOfQuestions());
            pstmt.setInt(2, test.getId());

            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("Cannot set update questions for test with id '"
                    + test.getId() + "' in db", e);
        } finally {
            close(connection, pstmt);
        }
    }

    public static List<Test> getFilteredTests(String[] subjects, String[] difficulties, String[] languages) throws DAOException {
        log.debug("TestDAO.getFilteredTests");

        if (subjects == null && difficulties == null && languages == null) {
            return new ArrayList<>();
        }

        String query = generateQuery(subjects, difficulties, languages);
        List<Test> tests = new ArrayList<>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);
            Test test;
            while (rs.next()) {
                test = extractTest(rs, con);
                tests.add(test);
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot get tests from db", e);
        } finally {
            close(con, stmt, rs);
        }

        return tests;
    }

    private static Test extractTest(ResultSet rs, Connection con) throws DAOException {
        log.debug("TestDAO.extractTest");

        Test test = new Test();
        try {
            test.setId(rs.getInt("id"));
            test.setTitle(rs.getString("title"));
            setDifficultyAndSubject(test, rs, con);
            test.setLanguage(rs.getString("language"));
            test.setNumOfQuestions(rs.getInt("num_of_questions"));
            test.setTimeLimit(rs.getInt("time_limit"));
            test.setMinPercentage(rs.getInt("min_percentage"));
            test.setNumOfSubmissions(rs.getInt("num_of_submissions"));
            test.setNumOfPasses(rs.getInt("num_of_passes"));
            test.setCreationDate(rs.getTimestamp("creation_date"));
        } catch (SQLException e) {
            throw new DAOException("Cannot extract test from ResultSet" + rs, e);
        }

        log.debug("extracted test = " + test);
        return test;
    }

    private static void setDifficultyAndSubject(Test test, ResultSet rs, Connection con) throws DAOException {
        int difficultyId = 0;
        int subjectId = 0;

        try {
            difficultyId = rs.getInt("difficulty_id");
            subjectId = rs.getInt("subject_id");
            Difficulty difficulty = DifficultyDAO.findDifficultyById(difficultyId, con);
            Subject subject = SubjectDAO.findSubjectById(subjectId, con);
            test.setDifficulty(difficulty);
            test.setSubject(subject);
        } catch (SQLException ex) {
            throw new DAOException("cannot extract difficulty '" + difficultyId
                    + "' and subject '" + subjectId + "' from db.");
        }
    }

    private static String generateQuery(String[] subjects, String[] difficulties, String[] languages) {
        StringBuilder query = new StringBuilder();

        query.append("SELECT * FROM tests WHERE \n");

        if (difficulties != null) {
            query.append("difficulty_id IN (");
            query.append(String.join(", ", difficulties));
            query.append(")\n");
        }

        if (subjects != null) {
            if (difficulties != null) {
                query.append("AND ");
            }
            query.append("subject_id IN (");
            query.append(String.join(", ", subjects));
            query.append(")\n");
        }

        if (languages != null) {
            String[] singleQuotedLanguages = new String[languages.length];

            for (int i = 0; i < languages.length; i++) {
                singleQuotedLanguages[i] = "'" + languages[i] + "'";
            }

            if (difficulties != null || subjects != null) {
                query.append("AND ");
            }
            query.append("language IN (");


            query.append(String.join(", ", singleQuotedLanguages));
            query.append(")");
        }

        return new String(query);
    }

}
