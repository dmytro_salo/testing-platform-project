package com.saldmy.dao;

import com.saldmy.entity.Test;
import com.saldmy.entity.TestResult;
import com.saldmy.exception.DAOException;
import com.saldmy.exception.NoRowsAffectedSQLException;
import com.saldmy.util.DBManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.saldmy.util.DAOUtils.close;
import static com.saldmy.util.SQLQueries.TestResultQueries.*;

/**
 * The {@code TestResultDAO} class provides CRUD functionality to {@code TestResult} JavaBean class.
 *
 * @author Dmytro Salo
 * @see TestResult
 */
public abstract class TestResultDAO {
    private static final Logger log = LogManager.getLogger(TestResultDAO.class);

    public static List<TestResult> getTestResultsByUserId(int id) throws DAOException {
        log.debug("TestResultDAO.getTestResultsByUserId");
        List<TestResult> testsResults = new ArrayList<>();

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_GET_RESULTS_BY_USER_ID);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                TestResult testResult = new TestResult();
                Test test = TestDAO.findTestById(rs.getString("test_id"));
                try {
                    testResult.setId(rs.getInt("id"));
                    testResult.setUserId(rs.getInt("user_id"));
                    testResult.setTest(test);
                    testResult.setPercent(rs.getInt("percent"));
                    testResult.setSuccess(rs.getBoolean("success"));
                    testResult.setSubmissionDate(rs.getTimestamp("submission_date"));

                } catch (SQLException e) {
                    throw new DAOException("Cannot extract difficulty from ResultSet" + rs, e);
                }
                testsResults.add(testResult);
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot get test results from db", e);
        } finally {
            close(con, pstmt, rs);
        }

        return testsResults;
    }

    public static List<TestResult> getTestResultsByUserId(int id, Connection con) throws DAOException {
        log.debug("TestResultDAO.getTestResultsByUserId");
        List<TestResult> testsResults = new ArrayList<>();

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            pstmt = con.prepareStatement(SQL_GET_RESULTS_BY_USER_ID);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                TestResult testResult = new TestResult();
                Test test = TestDAO.findTestById(rs.getString("test_id"));
                try {
                    testResult.setId(rs.getInt("id"));
                    testResult.setUserId(rs.getInt("user_id"));
                    testResult.setTest(test);
                    testResult.setPercent(rs.getInt("percent"));
                    testResult.setSuccess(rs.getBoolean("success"));
                    testResult.setSubmissionDate(rs.getTimestamp("submission_date"));

                } catch (SQLException e) {
                    throw new DAOException("Cannot extract difficulty from ResultSet" + rs, e);
                }
                testsResults.add(testResult);
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot get test results from db", e);
        } finally {
            close(pstmt, rs);
        }

        return testsResults;
    }

    public static void createTestResult(TestResult testResult) throws DAOException {
        log.debug("TestResultDAO.createTestResult");
        log.debug("testResult = " + testResult);

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_CREATE_NEW_RESULT, Statement.RETURN_GENERATED_KEYS);

            pstmt.setInt(1, testResult.getUserId());
            pstmt.setInt(2,testResult.getTest().getId());
            pstmt.setInt(3, testResult.getPercent());
            pstmt.setBoolean(4, testResult.isSuccess());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    testResult.setId(rs.getInt(1));
                }
            } else {
                throw new SQLException(new NoRowsAffectedSQLException());
            }
        } catch (SQLException e) {
            throw new DAOException("cannot insert test result: '" + testResult + "' into db", e);
        } finally {
            close(con, pstmt, rs);
        }
    }

}
