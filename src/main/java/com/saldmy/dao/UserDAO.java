package com.saldmy.dao;

import com.saldmy.entity.User;
import com.saldmy.exception.DAOException;
import com.saldmy.util.DBManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.saldmy.util.DAOUtils.close;
import static com.saldmy.util.SQLQueries.UserQueries.*;

/**
 * The {@code UserDAO} class provides CRUD functionality to {@code User} JavaBean class.
 *
 * @author Dmytro Salo
 * @see User
 */
public abstract class UserDAO {
    private static final Logger log = LogManager.getLogger(UserDAO.class);

    public static List<User> getAllUsers() throws DAOException {
        log.debug("UserDAO.getAllUsers");

        List<User> users = new ArrayList<>();

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_GET_ALL_USERS);
            while (rs.next()) {
                User user = extractUser(rs);
                user.setTestResults(TestResultDAO.getTestResultsByUserId(user.getId(), con));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot get all users from db", e);
        } finally {
            close(con, stmt, rs);
        }

        return users;
    }

    public static User findUserByEmail(String email) throws DAOException {
        log.debug("UserDAO.findUserByEmail");
        log.debug("email = " + email);

        User user = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_FIND_USER_BY_EMAIL);

            pstmt.setString(1, email);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                user = extractUser(rs);
                user.setTestResults(TestResultDAO.getTestResultsByUserId(user.getId(), con));
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot find user by email '" + email + "'", e);
        } finally {
            close(con, pstmt, rs);
        }

        return user;
    }

    public static User findUserById(String id) throws DAOException {
        log.debug("UserDAO.findUserById");
        log.debug("id = " + id);

        User user = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_FIND_USER_BY_ID);

            pstmt.setString(1, id);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                user = extractUser(rs);
                user.setTestResults(TestResultDAO.getTestResultsByUserId(user.getId(), con));
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot find user with id '" + id + "' in database", e);
        } finally {
            close(con, pstmt, rs);
        }

        return user;
    }

    public static boolean createUser(User user) throws DAOException {
        log.debug("UserDAO.createUser");
        log.debug("user = " + user);

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_CREATE_USER, Statement.RETURN_GENERATED_KEYS);

            pstmt.setString(1, user.getEmail());
            pstmt.setString(2, user.getPassword());
            pstmt.setString(3, user.getFirstName());
            pstmt.setString(4, user.getLastName());
            pstmt.setString(5, user.getPreferredLanguage());

            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                    user.setStatus("active");
                }
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new DAOException("cannot insert user: '" + user + "' into db", e);
        } finally {
            close(con, pstmt, rs);
        }
    }

    public static boolean updateUser(User user) throws DAOException {
        log.debug("UserDAO.updateUserPassword");

        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_UPDATE_USER);

            pstmt.setString(1, user.getFirstName());
            pstmt.setString(2, user.getLastName());
            pstmt.setString(3, user.getPreferredLanguage());
            pstmt.setInt(4, user.getId());


            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Cannot update user's password: " + user, e);
        } finally {
            close(con, pstmt);
        }
    }

    public static boolean deleteUser(String id) throws DAOException {
        log.debug("UserDAO.deleteUser");
        log.debug("user to delete = id: " + id);

        Connection connection = null;
        PreparedStatement pstmt = null;

        try {
            connection = DBManager.getInstance().getConnection();
            pstmt = connection.prepareStatement(SQL_DELETE_USER);

            pstmt.setString(1, id);

            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot delete user with id: " + id + " from db", e);
        } finally {
            close(connection, pstmt);
        }
    }

    public static boolean blockUser(String id) throws DAOException {
        log.debug("UserDAO.blockUser");
        log.debug("user to block = id: " + id);

        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = DBManager.getInstance().getConnection();
            pstmt = connection.prepareStatement(SQL_BLOCK_USER);

            pstmt.setString(1, id);

            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Cannot set block status for user with id: " + id + " in db", e);
        } finally {
            close(connection, pstmt);
        }
    }

    public static boolean unblockUser(String id) throws DAOException {
        log.debug("UserDAO.unblockUser");
        log.debug("user to unblock = id: " + id);

        Connection connection = null;
        PreparedStatement pstmt = null;
        try {
            connection = DBManager.getInstance().getConnection();
            pstmt = connection.prepareStatement(SQL_UNBLOCK_USER);

            pstmt.setString(1, id);

            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Cannot set unblock status for user with id: " + id + " in db", e);
        } finally {
            close(connection, pstmt);
        }
    }

    private static User extractUser(ResultSet rs) throws DAOException {
        log.debug("UserDAO.extractUser");

        User user = new User();
        try {
            user.setId(rs.getInt("id"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setStatus(rs.getString("status"));
            user.setRole(rs.getString("role"));
            user.setPreferredLanguage(rs.getString("language"));
            user.setRegistrationDate(rs.getTimestamp("registration_date"));
        } catch (SQLException e) {
            throw new DAOException("Cannot extract user from ResultSet" + rs, e);
        }
        log.debug("extracted user = " + user);

        return user;
    }

}
