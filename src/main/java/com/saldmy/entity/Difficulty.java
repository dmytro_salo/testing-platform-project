package com.saldmy.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * The {@code Difficulty} class is a JavaBean, the objects of which store information
 * about difficulties in web application.
 *
 * @author Dmytro Salo
 */
public class Difficulty implements Serializable {
    private int id;          // primary key in db
    private String name;
    private String nameUk;
    private int level;       // difficulty level from 0 to 100

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameUk() {
        return nameUk;
    }

    public void setNameUk(String nameUk) {
        this.nameUk = nameUk;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Difficulty that = (Difficulty) o;

        if (id != that.id) return false;
        if (level != that.level) return false;
        if (!Objects.equals(name, that.name)) return false;
        return Objects.equals(nameUk, that.nameUk);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (nameUk != null ? nameUk.hashCode() : 0);
        result = 31 * result + level;
        return result;
    }

    @Override
    public String toString() {
        return "Difficulty{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", nameUk='" + nameUk + '\'' +
                ", level=" + level +
                '}';
    }
}
