package com.saldmy.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 * The {@code Subject} class is a JavaBean, the objects of which store information about test subjects.
 *
 * @author Dmytro Salo
 */
public class Subject implements Serializable {
    private int id;
    private String name;
    private String nameUk;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameUk() {
        return nameUk;
    }

    public void setNameUk(String nameUk) {
        this.nameUk = nameUk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subject subject = (Subject) o;

        if (id != subject.id) return false;
        if (!Objects.equals(name, subject.name)) return false;
        return Objects.equals(nameUk, subject.nameUk);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (nameUk != null ? nameUk.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", nameUk='" + nameUk + '\'' +
                '}';
    }

}
