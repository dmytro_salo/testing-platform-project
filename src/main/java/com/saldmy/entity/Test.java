package com.saldmy.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The {@code Test} class is a JavaBean, the objects of which store information about tests.
 *
 * @author Dmytro Salo
 */
public class Test implements Serializable {
	private int id;                              // db primary key
	private String title;
	private Subject subject;
	private Difficulty difficulty;
	private String language;
	private int numOfQuestions;
	private int timeLimit;                       // time to pass (in minutes)
	private int minPercentage;                   // minimum percentage of right answers to pass the test
	private int numOfSubmissions;                // number of people, submitted to the test
	private int numOfPasses;                     // number of people, passed the test
	private Date creationDate;
	private List<Question> questions;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getNumOfQuestions() {
		return numOfQuestions;
	}

	public void setNumOfQuestions(int numOfQuestions) {
		this.numOfQuestions = numOfQuestions;
	}

	public int getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(int timeLimit) {
		this.timeLimit = timeLimit;
	}

	public int getNumOfSubmissions() {
		return numOfSubmissions;
	}

	public void setNumOfSubmissions(int numOfSubmissions) {
		this.numOfSubmissions = numOfSubmissions;
	}

	public int getNumOfPasses() {
		return numOfPasses;
	}

	public void setNumOfPasses(int numOfPasses) {
		this.numOfPasses = numOfPasses;
	}

	public int getMinPercentage() {
		return minPercentage;
	}

	public void setMinPercentage(int minPercentage) {
		this.minPercentage = minPercentage;
	}

	public Date getCreationDateObject() {
		return creationDate;
	}

	public String getCreationDate() {
		return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(creationDate);
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public void addQuestion(Question question) {
		questions.add(question);
	}

	public String getPassRate() {
		if (numOfSubmissions == 0) {
			return "-";
		} else {
			double passRate = (double) numOfPasses / numOfSubmissions;
			return String.format(Locale.US, "%.2f", passRate);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Test test = (Test) o;
		return id == test.id && timeLimit == test.timeLimit && minPercentage == test.minPercentage && numOfSubmissions == test.numOfSubmissions && numOfPasses == test.numOfPasses && Objects.equals(title, test.title) && Objects.equals(subject, test.subject) && Objects.equals(difficulty, test.difficulty) && Objects.equals(language, test.language) && Objects.equals(creationDate, test.creationDate) && Objects.equals(questions, test.questions);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, title, subject, difficulty, language, timeLimit, minPercentage, numOfSubmissions, numOfPasses, creationDate, questions);
	}

	@Override
	public String toString() {
		return "Test{" +
				"id=" + id +
				", title='" + title + '\'' +
				", subject=" + subject +
				", difficulty=" + difficulty +
				", language='" + language + '\'' +
				", timeLimit=" + timeLimit +
				", minPercentage=" + minPercentage +
				", numOfSubmissions=" + numOfSubmissions +
				", numOfPasses=" + numOfPasses +
				", creationDate=" + creationDate +
				'}';
	}

	public static class Question implements Serializable {
		private int id;
		private String questionText;
		private final List<Answer> answers;

		public Question() {
			this.answers = new ArrayList<>();
		}

		public String getQuestionText() {
			return questionText;
		}

		public String getPreview() {
			if (questionText.length() > 79) {
				return questionText.substring(0, 80).concat("...");
			} else {
				return questionText;
			}
		}

		public void setQuestionText(String questionText) {
			this.questionText = questionText;
		}

		public boolean isMultipleAnswers() {
			return answers.stream().filter(Answer::isCorrect).count() > 1;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public List<Answer> getAnswers() {
			return answers;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			Question question = (Question) o;
			return id == question.id && questionText.equals(question.questionText) && answers.equals(question.answers);
		}

		@Override
		public int hashCode() {
			return Objects.hash(questionText, id, answers);
		}

		@Override
		public String toString() {
			return "Question{" +
					"questionText='" + questionText + '\'' +
					", number=" + id +
					", answers=" + answers +
					'}';
		}

		public static class Answer implements Serializable {
			private int id;
			private boolean correct;
			private String text;

			public boolean isCorrect() {
				return correct;
			}

			public void setCorrect(boolean correct) {
				this.correct = correct;
			}

			public String getText() {
				return text;
			}

			public void setText(String text) {
				this.text = text;
			}

			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			@Override
			public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;
				Answer answer = (Answer) o;
				return id == answer.id && correct == answer.correct && text.equals(answer.text);
			}

			@Override
			public int hashCode() {
				return Objects.hash(id, correct, text);
			}

			@Override
			public String toString() {
				return "Answer{" +
						"correct=" + correct +
						", text='" + text + '\'' +
						", number=" + id +
						'}';
			}
		}
	}
}
