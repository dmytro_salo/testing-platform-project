package com.saldmy.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * The {@code TestResult} class is a JavaBean, the objects of which store information about test results.
 *
 * @author Dmytro Salo
 */
public class TestResult implements Serializable {
    private int id;
    private int userId;
    private Test test;
    private int percent;
    private boolean success;
    private Date submissionDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public String getSubmissionDateFormatted() {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(submissionDate);
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestResult that = (TestResult) o;

        if (id != that.id) return false;
        if (userId != that.userId) return false;
        if (percent != that.percent) return false;
        if (success != that.success) return false;
        if (test != null ? !test.equals(that.test) : that.test != null) return false;
        return submissionDate != null ? submissionDate.equals(that.submissionDate) : that.submissionDate == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + userId;
        result = 31 * result + (test != null ? test.hashCode() : 0);
        result = 31 * result + percent;
        result = 31 * result + (success ? 1 : 0);
        result = 31 * result + (submissionDate != null ? submissionDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TestResult{" +
                "id=" + id +
                ", userId=" + userId +
                ", test=" + test +
                ", percent=" + percent +
                ", success=" + success +
                ", submissionDate=" + submissionDate +
                '}';
    }

}
