package com.saldmy.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The {@code User} class is a JavaBean, the objects of which store information about users.
 *
 * @author Dmytro Salo
 */
public class User implements Serializable {
    private int id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String status;
    private String role;
    private String preferredLanguage;
    private Date registrationDate;
    private List<TestResult> testResults = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLocaleString() {
        if (Objects.equals(preferredLanguage, "English")) {
            return "en";
        } else if (Objects.equals(preferredLanguage, "Ukrainian")) {
            return "uk";
        } else {
            return null;
        }
    }

    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(String preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public String getRegistrationDate() {
        return registrationDate != null
                ? new SimpleDateFormat("dd.MM.yyyy HH:mm").format(registrationDate) : null;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setTestResults(List<TestResult> testResults) {
        this.testResults = testResults;
    }

    public String getAveragePercentage() {
        int submittedTests = testResults.size();
        if (submittedTests == 0) {
            return "-";
        } else {
            long successfulTests = (int) testResults.stream()
                    .filter(TestResult::isSuccess)
                    .count();
            return String.format(Locale.US, "%.2f", (double) successfulTests / submittedTests);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && Objects.equals(email, user.email) && Objects.equals(password, user.password) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(status, user.status) && Objects.equals(registrationDate, user.registrationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, password, firstName, lastName, status, registrationDate);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", status='" + status + '\'' +
                ", role='" + role + '\'' +
                ", preferredLanguage='" + preferredLanguage + '\'' +
                ", registrationDate=" + registrationDate +
                '}';
    }
}

