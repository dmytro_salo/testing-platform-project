package com.saldmy.exception;

/**
 * The {@code CommandException} class represents an {@link Exception}
 * which occurs in classes that implement {@link com.saldmy.command.Command} interface.
 *
 * @author Dmytro Salo
 * @see Exception
 * @see com.saldmy.command.Command
 */
public class CommandException extends Exception {

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException() {
        super();
    }

    public CommandException(Throwable cause) {
        super(cause);
    }

    public static CommandException getStandardCommandException(Throwable cause) {
        String message = "The server encountered an internal error or misconfiguration and was unable to " +
                "complete your request.";
        return new CommandException(message, cause);
    }

    public static CommandException getStandardCommandException() {
        String message = "The server encountered an internal error or misconfiguration and was unable to " +
                "complete your request.";
        return new CommandException(message);
    }

}
