package com.saldmy.exception;

/**
 * The {@code DAOException} class represents an {@link Exception}
 * which occurs in classes that implement DAO functionality.
 *
 * @author Dmytro Salo
 * @see Exception
 */
public class DAOException extends Exception {

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(String message) {
        super(message);
    }

}
