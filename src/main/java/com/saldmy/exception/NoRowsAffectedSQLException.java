package com.saldmy.exception;

/**
 * The {@code NoRowsAffectedSQLException} class represents an {@link Exception}
 * which specifies an overly general {@link java.sql.SQLException}, if no data is changed in the DB
 * after method execution.
 *
 * @author Dmytro Salo
 * @see java.sql.SQLException
 */
public class NoRowsAffectedSQLException extends Exception {

    public NoRowsAffectedSQLException() {
        super("No rows were affected after SQL script execution");
    }
}
