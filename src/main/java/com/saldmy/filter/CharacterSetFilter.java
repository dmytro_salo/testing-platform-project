package com.saldmy.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * The {@code CharacterSetFilter} class is the implementation of {@code Filter} interface,
 * which is responsible for setting the character set to UTF-8.
 *
 * @author Dmytro Salo
 */
@WebFilter("/*")
public class CharacterSetFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding("UTF-8");
        servletResponse.setContentType("text/html; charset=UTF-8");
        servletResponse.setCharacterEncoding("UTF-8");

        filterChain.doFilter(servletRequest, servletResponse);
    }

}
