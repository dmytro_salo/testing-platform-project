package com.saldmy.filter;

import com.saldmy.entity.User;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The {@code ControllerAccessFilter} class is the implementation of {@code Filter} interface,
 * which is responsible for secure access to controller servlet.
 *
 * @author Dmytro Salo
 */
@WebFilter("/controller")
public class ControllerAccessFilter extends HttpFilter {

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String command = req.getParameter("command");
        User loggedUser = (User) req.getSession().getAttribute("loggedUser");

        if (loggedUser == null || loggedUser.getId() != 1) {
            switch (command) {
                case "showManageUsersPage":
                case "showManageTestsPage":
                case "showDeleteUserPage":
                case "showBlockUserPage":
                case "showUnblockUserPage":
                case "showEditUserPage":
                    req.getServletContext()
                            .getRequestDispatcher("/accessDenied.jsp?preferences=true")
                            .forward(req, res);
            }
        }
        chain.doFilter(req, res);
    }

}
