package com.saldmy.filter;

import com.saldmy.entity.User;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The {@code ProtectedJSPAccessFilter} class is the implementation of {@code Filter} interface,
 * which is responsible for secure access to logged-in-only pages.
 *
 * @author Dmytro Salo
 */
@WebFilter("/protected/*")
public class ProtectedJSPAccessFilter extends HttpFilter {

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        User loggedUser = (User) req.getSession().getAttribute("loggedUser");

        if (loggedUser == null) {
            res.sendRedirect(req.getContextPath() + "/accessDenied.jsp?signUp=true");
        }

        chain.doFilter(req, res);
    }

}
