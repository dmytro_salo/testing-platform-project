package com.saldmy.listener;

import com.saldmy.dao.DifficultyDAO;
import com.saldmy.dao.SubjectDAO;
import com.saldmy.entity.Difficulty;
import com.saldmy.entity.Subject;
import com.saldmy.exception.DAOException;
import com.saldmy.util.DBManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * The {@code ContextListener} class is the implementation of {@code ServletContextListener} interface.
 * The main responsibility is setting up the environment before application start.
 *
 * @author Dmytro Salo
 */
@WebListener
public class ContextListener implements ServletContextListener {
    private static final Logger log = LogManager.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        checkConnection();
        cacheData(sce);
        cleanTempDirectory();
    }

    private void checkConnection() {
        try (Connection connection = DBManager.getInstance().getConnection()) {
            log.debug("connection = " + connection);
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot initialize DB subsystem", e);
        }
    }

    private void cacheData(ServletContextEvent sce) {
        try {
            List<Subject> subjects = SubjectDAO.getAllSubjects();
            sce.getServletContext().setAttribute("subjects", subjects);

            List<Difficulty> difficulties = DifficultyDAO.getAllDifficulties();
            sce.getServletContext().setAttribute("difficulties", difficulties);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    private void cleanTempDirectory() {
        String tempPath = System.getProperty("tempPath");
        File file = new File(tempPath);
        String[] files = file.list();
        if (files != null) {
            Arrays.stream(files)
                    .filter(s -> s.contains(".xml"))
                    .forEach(s -> {
                        try {
                            Files.delete(Paths.get(tempPath + s));
                            log.debug("Temporary file '" + s + "' is deleted;");
                        } catch (IOException e) {
                            log.warn("problem deleting files temporary files");
                            e.printStackTrace();
                        }
                    });
        }
    }

}
