package com.saldmy.tag;

import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * The {@code TimerColorTag} class describes a custom tag, that changes timer color, according to
 * time remained to finish the test.
 *
 * @author Dmytro Salo
 */
public class TimerColorTag extends SimpleTagSupport {
    private long deadlineMillis;

    public void setDeadline(long deadlineMillis) {
        this.deadlineMillis = deadlineMillis;
    }

    @Override
    public void doTag() throws IOException {
        String color;

        long leftMillis = deadlineMillis - System.currentTimeMillis();

        if (leftMillis > 1000 * 60 * 3) {
            color = "text-muted";
        } else if (leftMillis > 1000 * 60) {
            color = "text-warning";
        } else {
            color = "text-danger";
        }

        getJspContext().getOut().write(color);
    }

}
