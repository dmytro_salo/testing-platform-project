package com.saldmy.util;

import com.saldmy.entity.Difficulty;
import com.saldmy.entity.Subject;
import com.saldmy.exception.CommandException;

import javax.servlet.http.Part;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * The {@code CommandUtils} utility class contains helpful static methods,
 * which are used in {@link com.saldmy.command.Command} interface implementations.
 *
 * @author Dmytro Salo
 */
public abstract class CommandUtils {

    public static Subject extractSubject(String id, List<Subject> subjects) throws CommandException {
        return subjects.stream()
                .filter(s -> s.getId() == Integer.parseInt(id))
                .findAny()
                .orElseThrow(CommandException::getStandardCommandException);
    }

    public static Difficulty extractDifficulty(String id, List<Difficulty> difficulties) throws CommandException {
        return difficulties.stream()
                .filter(s -> s.getId() == Integer.parseInt(id))
                .findAny()
                .orElseThrow(CommandException::getStandardCommandException);
    }

    public static String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        for (String content : partHeader.split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    public static String getEncryptedHexString(String input) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException();
        }

        return bytesToHex(md.digest(input.getBytes(StandardCharsets.UTF_8)));
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

}
