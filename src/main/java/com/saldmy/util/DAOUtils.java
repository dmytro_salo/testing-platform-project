package com.saldmy.util;

import com.saldmy.exception.DAOException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * The {@code DAOUtils} utility class contains helpful static methods,
 * which are used in DAO classes.
 *
 * @author Dmytro Salo
 */
public abstract class DAOUtils {

    public static void close(Connection con, Statement stmt, ResultSet rs) throws DAOException {
        try {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot close connection: '" + con + "'", e);
        }

    }

    public static void close(Connection con, Statement stmt) throws DAOException {
        try {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot close connection: '" + con + "'", e);
        }
    }

    public static void close(Statement stmt, ResultSet rs) throws DAOException {
        try {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot close statement '" + stmt + "' or result set '" + rs + "'", e);
        }

    }

}
