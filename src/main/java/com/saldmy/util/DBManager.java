package com.saldmy.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

/**
 * The {@code DBManager} singleton class is responsible for providing DB connection
 * from the connection pool.
 *
 * @author Dmytro Salo
 */
public class DBManager {
    private static DBManager instance;
    private DataSource dataSource;
    private static final Logger log = LogManager.getLogger(DBManager.class);

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context)initContext.lookup("java:/comp/env");
            dataSource = (DataSource)envContext.lookup("jdbc/testing_platform_db");
        } catch (NamingException e) {
            log.fatal(Arrays.toString(e.getStackTrace()));
        }
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

}
