package com.saldmy.util;

/**
 * The {@code SQLQueries} class is a storage of MySQL code to access database.
 *
 * @author Dmytro Salo
 */
public abstract class SQLQueries {

    public static abstract class UserQueries {
        public static final String SQL_GET_ALL_USERS =
                "SELECT * FROM users";

        public static final String SQL_FIND_USER_BY_EMAIL =
                "SELECT * FROM users WHERE email = ?";

        public static final String SQL_FIND_USER_BY_ID =
                "SELECT * FROM users WHERE id = ?";

        public static final String SQL_CREATE_USER =
                "INSERT INTO users VALUES (DEFAULT, ?, ?, ?, ?, DEFAULT, DEFAULT, ?, DEFAULT)";

        public static final String SQL_UPDATE_USER =
                "UPDATE users SET first_name = ?, last_name = ?, language = ? WHERE id = ?";

        public static final String SQL_DELETE_USER =
                "DELETE FROM users WHERE id = ?";

        public static final String SQL_BLOCK_USER =
                "UPDATE users SET status = 'blocked' WHERE id = ?";

        public static final String SQL_UNBLOCK_USER =
                "UPDATE users SET status = 'active' WHERE id = ?";
    }

    public static abstract class TestQueries {
        public static final String SQL_GET_ALL_TESTS =
                "SELECT * FROM tests";

        public static final String SQL_FIND_TEST_BY_ID =
                "SELECT * FROM tests WHERE id = ?";

        public static final String SQL_FIND_TEST_BY_TITLE =
                "SELECT * FROM tests WHERE title = ?";

        public static final String SQL_CREATE_TEST =
                "INSERT INTO tests VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, DEFAULT, DEFAULT, DEFAULT)";

        public static final String SQL_UPDATE_TEST_PROPERTIES =
                "UPDATE tests " +
                "SET title = ?, subject_id = ?, difficulty_id = ?, language = ?, time_limit = ?, min_percentage = ? " +
                "WHERE id = ?";

        public static final String SQL_UPDATE_TEST_QUESTIONS =
                "UPDATE tests " +
                        "SET num_of_questions = ? " +
                        "WHERE id = ?";

        public static final String SQL_UPDATE_TEST_NUM_OF_SUBMISSIONS =
                "UPDATE tests SET num_of_submissions = ? WHERE id = ?";

        public static final String SQL_UPDATE_TEST_NUM_OF_SUBMISSIONS_AND_PASSES =
                "UPDATE tests SET num_of_submissions = ?, num_of_passes = ? WHERE id = ?";

        public static final String SQL_DELETE_TEST =
                "DELETE FROM tests WHERE id = ?";
    }

    public static abstract class DifficultyQueries {
        public static final String SQL_GET_ALL_DIFFICULTIES =
                "SELECT * FROM difficulties";

        public static final String SQL_FIND_DIFFICULTY_BY_ID =
                "SELECT * FROM difficulties WHERE id = ?";

        public static final String SQL_CREATE_DIFFICULTY =
                "INSERT INTO difficulties VALUES (DEFAULT, ?, ?, ?)";

        public static final String SQL_UPDATE_DIFFICULTY =
                "UPDATE difficulties SET name = ?, name_uk = ?, level = ? WHERE id = ?";

        public static final String SQL_DELETE_DIFFICULTY =
                "DELETE FROM difficulties WHERE id = ?";
    }

    public static abstract class SubjectQueries {
        public static final String SQL_GET_ALL_SUBJECTS =
                "SELECT * FROM subjects";

        public static final String SQL_FIND_SUBJECT_BY_ID =
                "SELECT * FROM subjects WHERE id = ?";

        public static final String SQL_CREATE_SUBJECT =
                "INSERT INTO subjects VALUES (DEFAULT, ?, ?)";

        public static final String SQL_UPDATE_SUBJECT =
                "UPDATE subjects SET name = ?, name_uk = ? WHERE id = ?";

        public static final String SQL_DELETE_SUBJECT =
                "DELETE FROM subjects WHERE id = ?";
    }

    public static abstract class TestResultQueries {
        public static final String SQL_GET_ALL_RESULTS =
                "SELECT * FROM test_results";

        public static final String SQL_GET_RESULTS_BY_USER_ID =
                "SELECT * FROM test_results WHERE user_id = ?";

        public static final String SQL_GET_RESULTS_BY_TEST_ID =
                "SELECT * FROM test_results WHERE test_id = ?";

        public static final String SQL_FIND_RESULT_BY_ID =
                "SELECT * FROM test_results WHERE id = ?";

        public static final String SQL_CREATE_NEW_RESULT =
                "INSERT INTO test_results VALUES (DEFAULT, ?, ?, ?, ?, DEFAULT)";

        public static final String SQL_UPDATE_RESULT =
                "UPDATE test_results SET success = ?, percent = ? WHERE id = ?";

        public static final String SQL_DELETE_RESULT =
                "DELETE FROM test_results WHERE id = ?";
    }
}
