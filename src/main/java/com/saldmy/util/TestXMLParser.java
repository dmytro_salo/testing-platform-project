package com.saldmy.util;

import com.saldmy.entity.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code TestXMLParser} utility class contains methods that validate XML test files,
 * parse information and create {@link Test} objects.
 *
 * @author Dmytro Salo
 * @see Test
 */
public abstract class TestXMLParser {
    private static DocumentBuilder documentBuilder;

    static {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            String examplePath = System.getProperty("examplesPath");
            Source schemaFile = new StreamSource(
                    new File(examplePath + "test-schema.xsd"));
            Schema schema = factory.newSchema(schemaFile);

            documentBuilderFactory.setNamespaceAware(true);
            documentBuilderFactory.setFeature("http://xml.org/sax/features/validation", true);
            documentBuilderFactory.setFeature("http://apache.org/xml/features/validation/schema", true);
            documentBuilderFactory.setSchema(schema);
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
    }

    public static Test parseTest(String path) throws IOException, SAXException {
        Document document = documentBuilder.parse(path);
        Test test = new Test();
        test.setQuestions(new ArrayList<>());

        Element root = document.getDocumentElement();
        test.setTitle(root.getAttribute("title"));

        NodeList questionNodes = root.getElementsByTagName("question");
        for (int j = 0; j < questionNodes.getLength(); j++) {
            if (questionNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
                Test.Question question = getQuestion((Element) questionNodes.item(j));
                question.setId(j);
                test.addQuestion(question);
            }
        }
        test.setNumOfQuestions(test.getQuestions().size());

        return test;
    }

    public static List<Test.Question> parseTestQuestions(String id) throws IOException, SAXException {
        String path = System.getProperty("testsPath") + "test_" + id + ".xml";
        Test test = parseTest(path);
        return test.getQuestions();
    }

    private static Test.Question getQuestion(Element questionElement) {
        Test.Question question = new Test.Question();
        List<Test.Question.Answer> answerList = question.getAnswers();

        NodeList questionTextNodes = questionElement.getElementsByTagName("questionText");
        if (questionTextNodes.getLength() != 0) {
            question.setQuestionText(questionTextNodes.item(0).getTextContent());
        }

        NodeList answerNodes = questionElement.getElementsByTagName("answer");
        for (int i = 0; i < answerNodes.getLength(); i++) {
            if (answerNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Test.Question.Answer answer = getAnswer((Element) answerNodes.item(i));
                answer.setId(i);
                answerList.add(answer);
            }
        }

        return question;
    }

    private static Test.Question.Answer getAnswer(Element answerNode) {
        Test.Question.Answer answer = new Test.Question.Answer();
        answer.setCorrect(Boolean.parseBoolean(answerNode.getAttribute("correct")));
        answer.setText(answerNode.getTextContent());
        return answer;
    }

}
