package com.saldmy.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * The {@code Validators} utility class provides methods, that validate different variables
 * according to REGEX.
 *
 * @author Dmytro Salo
 */
public abstract class Validators {
    private static final Logger log = LogManager.getLogger(Validators.class);

    public static boolean validateEmail(String email) {
        log.debug("Validators.validateEmail");

        if (email == null) {
            log.debug("email is null");
            return false;
        }

        boolean result = email.matches("[a-zA-Z0-9._]+@[a-zA-Z]+\\.[a-zA-Z]+");
        log.debug("The result of validating email '" + email + "' is " + result);
        return result;
    }

    // this method imposes restrictions on acceptable password
    public static boolean validatePassword(String password) {
        log.debug("Validators.validatePassword");

        if (password == null) {
            log.debug("Password is null");
            return false;
        }

        // 8+ characters, at least 1 letter and 1 number
        boolean result = password.matches("^(?=.*[0-9])(?=.*[a-zA-Z])(?=\\S+$).{8,}$");

        log.debug("The result of validating password '" + password + "' is " + result);
        return result;
    }

    public static boolean validateIntParameter(String id) {
        log.debug("Validators.validateIntParameter");

        if (id == null) {
            log.debug("int is null");
            return false;
        }

        boolean result = id.matches("^\\d+$");
        log.debug("The result  of validating id '" + id + "' is " + result);

        return result;

    }

    // this method imposes restrictions on acceptable first and last name
    public static boolean validateName(String name) {
        log.debug("Validators.validateName");

        if (name == null) {
            log.debug("name is null");
            return false;
        }

        // First capital letter, Ukrainian or English symbols only, '-' is acceptable
        boolean result = name.matches("^[A-ZА-ЩЬЮЯҐЄІЇ][a-zа-щьюяґєії']+(-[A-ZА-ЩЬЮЯҐЄІЇ][a-zа-щьюяґєії']+)?$");

        log.debug("the result of validating name '" + name + "' is " + result);
        return result;
    }

    public static void validateTest(String path) throws SAXException, IOException {
        log.debug("Validators.validateTest");
        log.debug("path = " + path);

        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        String examplePath = System.getProperty("examplesPath");
        Source schemaFile = new StreamSource(
                new File(examplePath + "test-schema.xsd"));
        Schema schema = factory.newSchema(schemaFile);
        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(new File(path)));
        log.debug(path + "successfully validated");
    }

    // this method imposes restrictions on acceptable title
    public static boolean validateTestTitle(String title) {
        log.debug("Validators.validateTestTitle");

        if (title == null) {
            log.debug("title is null");
            return false;
        }

        // 7+ characters, first capital letter
        boolean result = title.matches("^[A-ZА-ЩЬЮЯҐЄІЇ].{6,}");
        log.debug("the result of validating title '" + title + "' is " + result);

        return result;
    }

    public static boolean validateCapsInPassword(String password) {
        log.debug("Validators.validateCapsInPassword");

        if (password == null) {
            log.debug("password is null");
            return false;
        }

        boolean result = password.matches("^[A-ZА-ЩЬЮЯҐЄІЇa-zа-щьюяґєії']?[A-ZА-ЩЬЮЯҐЄІЇ]{5,}\\d*");
        log.debug("the result of validating CAPS in password '" + password + "' is " + result);

        return result;
    }

    public static boolean validateBooleanParameter(String parameter) {
        log.debug("Validators.validateBooleanParameter");
        log.debug("parameter = " + parameter);

        if (parameter == null) {
            log.debug("parameter is null");
            return false;
        }

        return parameter.equals("true") || parameter.equals("false");
    }
}
