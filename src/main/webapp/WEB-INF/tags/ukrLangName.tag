<%@ tag pageEncoding="UTF-8" description="Displays language name in Ukrainian" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="language" required="true" type="java.lang.String" description="Language name in English" %>
<c:if test="${language == 'English'}">
  англійська
</c:if>
<c:if test="${language == 'Ukrainian'}">
  українська
</c:if>