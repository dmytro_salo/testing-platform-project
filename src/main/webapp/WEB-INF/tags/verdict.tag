<%@ tag pageEncoding="UTF-8" description="Displays language name in Ukrainian" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="success" required="true" type="java.lang.Boolean" %>
<%@ attribute name="locale" required="true" type="java.lang.String" %>

<c:if test="${locale == 'en'}">
  ${success == 'true' ? 'Success' : "Not passed"}
</c:if>
<c:if test="${locale == 'uk'}">
  ${success == 'true' ? 'Успіх' : "Не складено"}
</c:if>