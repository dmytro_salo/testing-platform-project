<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="locale"
       value="${not empty param.loc ?
       param.loc : not empty sessionScope.loggedUser ?
       sessionScope.loggedUser.localeString : pageContext.request.locale}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<% System.out.println("jsp: accessDenied.jsp"); %>
<html>
<head>
  <title><f:message key="access_denied"/></title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
</head>
<body>
  <div class="container-md">
    <br/>
    <a href="${pageContext.request.contextPath}/" class="btn btn-primary" role="button">Go to main page</a>
    <hr/>
    <br/><br/><br/>
    <c:if test="${param.preferences == true}">
      <figure class="text-center">
        <blockquote class="blockquote">
          <p><f:message key="not_enough_preferences"/></p>
        </blockquote>
      </figure>
    </c:if>
    <c:if test="${param.admin == true}">
      <figure class="text-center">
        <blockquote class="blockquote">
          <p><f:message key="admin_account_edit"/></p>
        </blockquote>
      </figure>
    </c:if>
    <c:if test="${param.signUp == true}">
      <figure class="text-center">
        <blockquote class="blockquote">
          <p><f:message key="only_registered"/></p>
          <p><f:message key="no_account?"/> <a href="signUp.jsp"><f:message key="sign_up"/></a></p>
        </blockquote>
      </figure>
    </c:if>
  </div>
</body>
</html>