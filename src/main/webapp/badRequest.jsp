<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="locale"
       value="${not empty param.loc ?
       param.loc : not empty sessionScope.loggedUser ?
       sessionScope.loggedUser.localeString : pageContext.request.locale}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<% System.out.println("jsp: badRequest.jsp"); %>
<html>
<head>
  <title><f:message key="404"/></title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
</head>
<body>
  <div class="container-md">
    <br/>
    <a href="${pageContext.request.contextPath}/" class="btn btn-primary" role="button">Go to main page</a>
    <hr/>
    <br/><br/>
    <p style="text-align: center; font-size: xxx-large"><f:message key="404"/></p>
    <figure class="text-center">
      <blockquote class="blockquote">
        <p><f:message key="page_cannot_find"/></p>
        <p><f:message key="url_misspelled"/></p>
      </blockquote>
    </figure>
  </div>
</body>
</html>
