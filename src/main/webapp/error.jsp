<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="locale"
       value="${not empty param.loc ?
       param.loc : not empty sessionScope.loggedUser ?
       sessionScope.loggedUser.localeString : pageContext.request.locale}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<% System.out.println("jsp: error.jsp"); %>
<html>
<head>
  <title><f:message key="oops"/></title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
</head>
<body>
  <div class="container-md">
    <br/>
    <a href="${pageContext.request.contextPath}/" class="btn btn-primary" role="button">Go to main page</a>
    <hr/>
    <br/><br/>
    <p style="text-align: center; font-size: xxx-large"><f:message key="something_went_wrong"/></p>
    
    <figure class="text-center">
      <blockquote class="blockquote">
        <p><f:message key="internal_error"/></p>
      </blockquote>
    </figure>
  </div>

</body>
</html>
