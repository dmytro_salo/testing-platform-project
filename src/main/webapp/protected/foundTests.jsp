<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<c:set var="locale" value="${sessionScope.loggedUser.localeString}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<%
  System.out.println("jsp: foundTests.jsp");
  int k = (int) request.getAttribute("itemsPerPage") * ((int) request.getAttribute("currentPage") - 1);
%>
<html>
<head>
  <title><f:message key="find_test"/></title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous" async></script>
</head>
<body>
  <jsp:include page="../secure/header.jsp"/>
  <div class="container-md">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/"><f:message key="home"/></a></li>
        <li class="breadcrumb-item active" aria-current="page"><f:message key="find_test"/></li>
      </ol>
    </nav>
    <c:if test="${requestScope.tests.size() != 0}">
      <table class="table table-bordered table-striped align-middle text-center">
        <tr class="table-header">
          <th scope="col">#</th>
          <th scope="col">
            <c:if test="${param.sort != 'title' || (param.sort == 'title' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=title<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
            </c:if>
              <c:if test="${(param.sort == 'title' && param.reversed != true) || param.sort == null}">
              <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=title&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
                 class="text-decoration-none link-dark">
              </c:if>
                <c:if test="${(param.sort == 'title' && param.reversed != true) || param.sort == null}"><f:message key="title"/> &#9660;</c:if>
                <c:if test="${(param.sort == 'title' && param.reversed == true)}"><f:message key="title"/> &#9650;</c:if>
                <c:if test="${param.sort != 'title' && param.sort != null}"><f:message key="title"/></c:if>
              </a>
          </th>
          <th scope="col">
            <c:if test="${param.sort != 'subject' || (param.sort == 'subject' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=subject<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
            </c:if>
              <c:if test="${param.sort == 'subject' && param.reversed != true}">
              <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=subject&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
                 class="text-decoration-none link-dark">
              </c:if>
                <c:if test="${param.sort == 'subject' && param.reversed != true}"><f:message key="subject"/> &#9660</c:if>
                <c:if test="${param.sort == 'subject' && param.reversed == true}"><f:message key="subject"/> &#9650</c:if>
                <c:if test="${param.sort != 'subject'}"><f:message key="subject"/></c:if>
              </a>
          </th>
          <th scope="col">
            <c:if test="${param.sort != 'difficulty' || (param.sort == 'difficulty' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=difficulty<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
            </c:if>
              <c:if test="${param.sort == 'difficulty' && param.reversed != true}">
              <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=difficulty&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
                 class="text-decoration-none link-dark">
              </c:if>
                <c:if test="${param.sort == 'difficulty' && param.reversed != true}"><f:message key="difficulty"/> &#9660</c:if>
                <c:if test="${param.sort == 'difficulty' && param.reversed == true}"><f:message key="difficulty"/> &#9650</c:if>
                <c:if test="${param.sort != 'difficulty'}"><f:message key="difficulty"/></c:if>
              </a>
          </th>
          <th scope="col">
            <c:if test="${param.sort != 'language' || (param.sort == 'language' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=language<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
            </c:if>
              <c:if test="${param.sort == 'language' && param.reversed != true}">
              <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=language&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
                 class="text-decoration-none link-dark">
              </c:if>
                <c:if test="${param.sort == 'language' && param.reversed != true}"><f:message key="language"/> &#9660</c:if>
                <c:if test="${param.sort == 'language' && param.reversed == true}"><f:message key="language"/> &#9650</c:if>
                <c:if test="${param.sort != 'language'}"><f:message key="language"/></c:if>
              </a>
          </th>
          <th scope="col">
            <c:if test="${param.sort != 'numOfQuestions' || (param.sort == 'numOfQuestions' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=numOfQuestions<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
            </c:if>
              <c:if test="${param.sort == 'numOfQuestions' && param.reversed != true}">
              <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=numOfQuestions&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
                 class="text-decoration-none link-dark">
              </c:if>
                <c:if test="${param.sort == 'numOfQuestions' && param.reversed != true}"><f:message key="questions"/> &#9660</c:if>
                <c:if test="${param.sort == 'numOfQuestions' && param.reversed == true}"><f:message key="questions"/> &#9650</c:if>
                <c:if test="${param.sort != 'numOfQuestions'}"><f:message key="questions"/></c:if>
              </a>
          </th>
          <th scope="col">
            <c:if test="${param.sort != 'timeLimit' || (param.sort == 'timeLimit' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=timeLimit<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
            </c:if>
              <c:if test="${param.sort == 'timeLimit' && param.reversed != true}">
              <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=timeLimit&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
                 class="text-decoration-none link-dark">
              </c:if>
                <c:if test="${param.sort == 'timeLimit' && param.reversed != true}"><f:message key="time_limit"/> &#9660</c:if>
                <c:if test="${param.sort == 'timeLimit' && param.reversed == true}"><f:message key="time_limit"/> &#9650</c:if>
                <c:if test="${param.sort != 'timeLimit'}"><f:message key="time_limit"/></c:if>
              </a>
          </th>
          <th scope="col">
            <c:if test="${param.sort != 'minPercentage' || (param.sort == 'minPercentage' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=minPercentage<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
            </c:if>
              <c:if test="${param.sort == 'minPercentage' && param.reversed != true}">
              <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=minPercentage&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
                 class="text-decoration-none link-dark">
              </c:if>
                <c:if test="${param.sort == 'minPercentage' && param.reversed != true}"><f:message key="min_percentage"/> &#9660</c:if>
                <c:if test="${param.sort == 'minPercentage' && param.reversed == true}"><f:message key="min_percentage"/> &#9650</c:if>
                <c:if test="${param.sort != 'minPercentage'}"><f:message key="min_percentage"/></c:if>
              </a>
          </th>
          <th scope="col">
            <c:if test="${param.sort != 'numOfSubmissions' || (param.sort == 'numOfSubmissions' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=numOfSubmissions<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
            </c:if>
              <c:if test="${param.sort == 'numOfSubmissions' && param.reversed != true}">
              <a href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&sort=numOfSubmissions&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
                 class="text-decoration-none link-dark">
              </c:if>
                <c:if test="${param.sort == 'numOfSubmissions' && param.reversed != true}"><f:message key="num_of_submissions"/> &#9660</c:if>
                <c:if test="${param.sort == 'numOfSubmissions' && param.reversed == true}"><f:message key="num_of_submissions"/> &#9650</c:if>
                <c:if test="${param.sort != 'numOfSubmissions'}"><f:message key="num_of_submissions"/></c:if>
              </a>
          </th>
          <th scope="col"><f:message key="options"/></th>
        </tr>
        <c:forEach var="test" items="${requestScope.tests}">
          <tr>
            <th><%= ++k %>
            </th>
            <td><c:out value="${test.title}"/></td>
            <td><c:out value="${locale == 'en' ? test.subject.name : test.subject.nameUk}"/></td>
            <td><c:out value="${locale == 'en' ? test.difficulty.name : test.difficulty.nameUk}"/></td>
            <c:if test="${locale == 'en'}">
              <td><c:out value="${test.language}"/></td>
            </c:if>
            <c:if test="${locale == 'uk'}">
              <td><my:ukrLangName language="${test.language}"/></td>
            </c:if>
            <td><c:out value="${test.numOfQuestions}"/></td>
            <td><c:out value="${test.timeLimit}"/></td>
            <td><c:out value="${test.minPercentage}"/></td>
            <td><c:out value="${test.numOfSubmissions}"/></td>
            <td>
              <button type="button" class="btn btn-outline-success btn-sm dropdown-toggle"
                      data-bs-toggle="modal" data-bs-target="#startModal${test.id}">
                <f:message key="go_to_test"/>
              </button>
            </td>
          </tr>
          <!-- Start modal -->
          <div class="modal fade" id="startModal${test.id}" tabindex="-1" aria-labelledby="startModalLabel${test.id}"
               aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="startModalLabel${test.id}"><f:message key="sure"/></h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center">
                  <p><f:message key="start_modal_start"/></p>
                  <p><f:message key="title"/>: <span class="fw-bolder">"${test.title}"</span></p>
                  <p><f:message key="questions"/>: <span class="fw-bolder">${test.numOfQuestions}</span></p>
                  <p><f:message key="time_limit"/>: <span class="fw-bolder">${test.timeLimit}</span></p>
                </div>
                <form method="post" action="${pageContext.request.contextPath}/controller"
                      id="startTestForm${test.id}">
                  <input type="hidden" name="command" value="startTest">
                  <input type="hidden" name="id" value="${test.id}">
                </form>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><f:message key="close"/></button>
                  <button type="submit" class="btn btn-success" form="startTestForm${test.id}"><f:message key="begin_test"/></button>
                </div>
              </div>
            </div>
          </div>
        </c:forEach>
      </table>
    </div>
    <jsp:include page="../secure/paginationFooter.jsp"/>
  </c:if>
  <c:if test="${requestScope.tests.size() == 0}">
    <br/><br/>
    <p class="text-center fs-3 align-middle"><f:message key="no_tests_found"/></p>
  </c:if>
  <jsp:include page="../secure/filterModal.jsp"/>
</body>
</html>