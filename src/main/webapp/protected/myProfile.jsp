<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<c:set var="locale" value="${sessionScope.loggedUser.localeString}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<% System.out.println("jsp: myProfile.jsp"); %>
<c:set var="user" value="${sessionScope.loggedUser}"/>
<html>
<head>
  <title><f:message key="my_profile"/></title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"/>
  <script async src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous"></script>
</head>
<body>
  <jsp:include page="/secure/header.jsp"/>
  <div class="container-md">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/"><f:message key="home"/></a></li>
        <li class="breadcrumb-item active" aria-current="page"><f:message key="my_profile"/></li>
      </ol>
    </nav>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title text-center"><f:message key="my_profile"/></h5>
          </div>
          <div class="card-body">
            <form action="${pageContext.request.contextPath}/controller" method="post"
                  id="editUserForm${user.id}">
              <input type="hidden" name="command" value="editUser">
              <input type="hidden" name="id" value="${user.id}">
              <input type="hidden" name="user" value="true">
              <div class="form-group">
                <div class="form-text">
                  <label for="editEmail"><f:message key="email"/>:</label>
                  <input type="text" name="email" id="editEmail" value="${user.email}" readonly disabled>
                </div>
              </div>
              <br/>
              <div class="form-group">
                <div class="form-text">
                  <label for="editFirstName"><f:message key="first_name"/>:</label>
                  <input type="text" name="firstName" id="editFirstName" value="${user.firstName}"
                  ${param.edit == true ? 'required' : 'readonly'}>
                </div>
                <div class="form-text">
                  <label for="editLastName"><f:message key="last_name"/>:</label>
                  <input type="text" name="lastName" id="editLastName" value="${user.lastName}"
                  ${param.edit == true ? 'required' : 'readonly'}>
                </div>
              </div>
              <br/>
              <p ${param.edit != true ? 'class="text-muted"' : ''}><f:message key="preferred_lang"/>:</p>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="preferredLanguage" value="English"
                       id="english" ${user.preferredLanguage == 'English' ? ' checked' : ''}
                ${param.edit == true ? '' : 'disabled'}>
                <label class="form-check-label" for="english">
                  <c:if test="${locale == 'en'}">English</c:if>
                  <c:if test="${locale == 'uk'}"><my:ukrLangName language="English"/></c:if>
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="preferredLanguage" value="Ukrainian"
                       id="ukrainian" ${user.preferredLanguage == 'Ukrainian' ? ' checked' : ''}
                ${param.edit == true ? '' : 'disabled'}>
                <label class="form-check-label" for="ukrainian">
                  <c:if test="${locale == 'en'}">Ukrainian</c:if>
                  <c:if test="${locale == 'uk'}"><my:ukrLangName language="Ukrainian"/></c:if>
                </label>
              </div>
            </form>
          </div>
          <div class="card-footer">
            <c:if test="${param.edit == true}">
              <a class="btn btn-secondary"
                 href="${pageContext.request.contextPath}/protected/myProfile.jsp">
                <f:message key="cancel"/>
              </a>
              <button type="submit" form="editUserForm${user.id}" class="btn btn-primary">
                <f:message key="submit_changes"/>
              </button>
            </c:if>
            <c:if test="${param.edit != true}">
              <a class="btn btn-outline-primary"
                 href="${pageContext.request.contextPath}/protected/myProfile.jsp?edit=true">
                <f:message key="edit_profile"/>
              </a>
            </c:if>
            <div class="col-md-4"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
