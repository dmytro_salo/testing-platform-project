<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% System.out.println("jsp: testResult.jsp"); %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<c:set var="locale" value="${sessionScope.loggedUser.localeString}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<html>
<head>
  <title><f:message key="test_result"/></title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
  <script async src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous"></script>
</head>
<body>
  <jsp:include page="../secure/header.jsp"/>
  <div class="container-md">
    <div class="row">
      <div class="col-lg-2"></div>
      <div class="col-lg-8">
        <div class="card align-middle">
          <div class="card-header">
            <h5 class="card-title">${sessionScope.startedTest.title}</h5>
          </div>
          <div class="card-body">
            <p class="fs-5 text-center"><f:message key="your_result"/>:</p>
            <p class="fs-1 text-center">${sessionScope.submittedTestResult.percent}%</p>
            <hr/>
            <p class="text-center"><f:message key="min_percentage_alt"/>: ${sessionScope.startedTest.minPercentage}%</p>
            <c:if test="${sessionScope.submittedTestResult.success == true}">
              <p class="text-center fs-5"><f:message key="verdict"/>: <span class="fw-bolder text-success">
                <my:verdict success="true" locale="${locale}"/>
              </span></p>
            </c:if>
            <c:if test="${sessionScope.submittedTestResult.success == false}">
              <p class="text-center fs-5"><f:message key="verdict"/>: <span class="fw-bolder text-danger">
                <my:verdict success="false" locale="${locale}"/>
              </span></p>
            </c:if>
            <form method="post" action="${pageContext.request.contextPath}/controller"
                  id="startAgainForm">
              <input type="hidden" name="command" value="startTest">
              <input type="hidden" name="id" value="${sessionScope.startedTest.id}">
            </form>
          </div>
          <div class="card-footer">
            <button class="btn btn-primary" type="submit" form="startAgainForm"><f:message key="retry"/></button>
            <a class="btn btn-secondary" href="${pageContext.request.contextPath}/"><f:message key="close"/></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
