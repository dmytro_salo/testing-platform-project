<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<c:set var="locale" value="${sessionScope.loggedUser.localeString}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<%
  System.out.println("jsp: testResults.jsp");
  int k = (int) request.getAttribute("itemsPerPage") * ((int) request.getAttribute("currentPage") - 1);
%>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<html>
<head>
  <title><f:message key="test_results"/></title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
  <script async src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous"></script>
</head>
<body>
  <jsp:include page="/secure/header.jsp"/>
  <div class="container-md">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${root}/"><f:message key="home"/></a></li>
        <li class="breadcrumb-item active" aria-current="page"><f:message key="test_results"/> ${requestScope.user.email}</li>
      </ol>
    </nav>
    <table class="table table-bordered table-striped align-middle text-center">
      <tr class="table-header">
        <th scope="col">#</th>
        <th scope="col">
          <c:if test="${param.sort != 'title' || (param.sort == 'title' && param.reversed == true)}">
          <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=title<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${(param.sort == 'title' && param.reversed != true) || param.sort == null}">
            <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=title&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${(param.sort == 'title' && param.reversed != true) || param.sort == null}"><f:message key="title"/> &#9660;</c:if>
              <c:if test="${(param.sort == 'title' && param.reversed == true)}"><f:message key="title"/> &#9650;</c:if>
              <c:if test="${param.sort != 'title' && param.sort != null}"><f:message key="title"/></c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'subject' || (param.sort == 'subject' && param.reversed == true)}">
          <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=subject<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'subject' && param.reversed != true}">
            <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=subject&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'subject' && param.reversed != true}"><f:message key="subject"/> &#9660</c:if>
              <c:if test="${param.sort == 'subject' && param.reversed == true}"><f:message key="subject"/> &#9650</c:if>
              <c:if test="${param.sort != 'subject'}"><f:message key="subject"/></c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'difficulty' || (param.sort == 'difficulty' && param.reversed == true)}">
          <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=difficulty<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'difficulty' && param.reversed != true}">
            <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=difficulty&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'difficulty' && param.reversed != true}"><f:message key="difficulty"/> &#9660</c:if>
              <c:if test="${param.sort == 'difficulty' && param.reversed == true}"><f:message key="difficulty"/> &#9650</c:if>
              <c:if test="${param.sort != 'difficulty'}"><f:message key="difficulty"/></c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'language' || (param.sort == 'language' && param.reversed == true)}">
          <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=language<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'language' && param.reversed != true}">
            <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=language&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'language' && param.reversed != true}"><f:message key="language"/> &#9660</c:if>
              <c:if test="${param.sort == 'language' && param.reversed == true}"><f:message key="language"/> &#9650</c:if>
              <c:if test="${param.sort != 'language'}"><f:message key="language"/></c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'numOfQuestions' || (param.sort == 'numOfQuestions' && param.reversed == true)}">
          <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=numOfQuestions<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'numOfQuestions' && param.reversed != true}">
            <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=numOfQuestions&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'numOfQuestions' && param.reversed != true}"><f:message key="questions"/> &#9660</c:if>
              <c:if test="${param.sort == 'numOfQuestions' && param.reversed == true}"><f:message key="questions"/> &#9650</c:if>
              <c:if test="${param.sort != 'numOfQuestions'}"><f:message key="questions"/></c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'timeLimit' || (param.sort == 'timeLimit' && param.reversed == true)}">
          <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=timeLimit<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'timeLimit' && param.reversed != true}">
            <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=timeLimit&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'timeLimit' && param.reversed != true}"><f:message key="time_limit"/> &#9660</c:if>
              <c:if test="${param.sort == 'timeLimit' && param.reversed == true}"><f:message key="time_limit"/> &#9650</c:if>
              <c:if test="${param.sort != 'timeLimit'}"><f:message key="time_limit"/></c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'minPercentage' || (param.sort == 'minPercentage' && param.reversed == true)}">
          <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=minPercentage<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'minPercentage' && param.reversed != true}">
            <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=minPercentage&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'minPercentage' && param.reversed != true}"><f:message key="min_percentage"/> &#9660</c:if>
              <c:if test="${param.sort == 'minPercentage' && param.reversed == true}"><f:message key="min_percentage"/> &#9650</c:if>
              <c:if test="${param.sort != 'minPercentage'}"><f:message key="min_percentage"/></c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'resultPercentage' || (param.sort == 'resultPercentage' && param.reversed == true)}">
          <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=resultPercentage<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'resultPercentage' && param.reversed != true}">
            <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=resultPercentage&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'resultPercentage' && param.reversed != true}"><f:message key="result_percentage"/> &#9660</c:if>
              <c:if test="${param.sort == 'resultPercentage' && param.reversed == true}"><f:message key="result_percentage"/> &#9650</c:if>
              <c:if test="${param.sort != 'resultPercentage'}"><f:message key="result_percentage"/></c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'verdict' || (param.sort == 'verdict' && param.reversed == true)}">
          <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=verdict<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'verdict' && param.reversed != true}">
            <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=verdict&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'verdict' && param.reversed != true}"><f:message key="verdict"/> &#9660</c:if>
              <c:if test="${param.sort == 'verdict' && param.reversed == true}"><f:message key="verdict"/> &#9650</c:if>
              <c:if test="${param.sort != 'verdict'}"><f:message key="verdict"/></c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'submissionDate' || (param.sort == 'submissionDate' && param.reversed == true)}">
          <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=submissionDate<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'submissionDate' && param.reversed != true}">
            <a href="${root}/controller?command=showUserTestResultsPage&userId=${requestScope.user.id}&sort=submissionDate&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'submissionDate' && param.reversed != true}"><f:message key="submission_date"/> &#9660</c:if>
              <c:if test="${param.sort == 'submissionDate' && param.reversed == true}"><f:message key="submission_date"/> &#9650</c:if>
              <c:if test="${param.sort != 'submissionDate'}"><f:message key="submission_date"/></c:if>
            </a>
        </th>
      </tr>
      <c:forEach var="testResult" items="${requestScope.testResults}">
        <tr>
          <th><%= ++k %>
          </th>
          <td><c:out value="${testResult.test.title}"/></td>
          <td><c:out value="${locale == 'en' ? testResult.test.subject.name : testResult.test.subject.nameUk}"/></td>
          <td><c:out value="${locale == 'en' ? testResult.test.difficulty.name : testResult.test.difficulty.nameUk}"/></td>
          <c:if test="${locale == 'en'}">
            <td><c:out value="${testResult.test.language}"/></td>
          </c:if>
          <c:if test="${locale == 'uk'}">
            <td><my:ukrLangName language="${testResult.test.language}"/></td>
          </c:if>
          <td><c:out value="${testResult.test.numOfQuestions}"/></td>
          <td><c:out value="${testResult.test.timeLimit}"/></td>
          <td><c:out value="${testResult.test.minPercentage}"/></td>
          <td><c:out value="${testResult.percent}"/></td>
          <td>
            <span class="fw-semibold ${testResult.success ? 'text-success' : 'text-danger'}">
              <my:verdict success="${testResult.success}" locale="${locale}"/>
            </span>
          </td>
          <td><c:out value="${testResult.submissionDateFormatted}"/></td>
        </tr>
      </c:forEach>
    </table>
  </div>
  <jsp:include page="/secure/paginationFooter.jsp"/>
</body>
</html>
