<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% System.out.println("jsp: createTest.jsp"); %>
<html>
<head>
  <title>Create test</title>
  <meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous" async></script>
</head>
<body>
  <jsp:include page="header.jsp"/>
  <div class="container-md">
    <h2 class="text-center">Create new test</h2>
    <br/>
    <div class="row">
      <div class="col-lg-2"></div>
      <div class="col-lg-8">
        <div class="card align-middle">
          <div class="card-header">
            <br/>
            <h6 class="card-subtitle<c:if test="${param.command == null}"> text-danger</c:if>
            <c:if test="${param.command == 'showCreateTestPage'}"> text-success</c:if>">
            <c:choose>
              <c:when test="${param.invalidTitle == true}">
                Title must start from capital letter and be at least 7 characters long.
              </c:when>
              <c:when test="${param.duplicateTitle == true}">
                Test with this title already exists.
              </c:when>
              <c:when test="${param.invalidSubject == true}">
                Please specify subject
              </c:when>
              <c:when test="${param.invalidLanguage == true}">
                Please specify language
              </c:when>
              <c:when test="${param.invalidDifficulty == true}">
                Please specify difficulty
              </c:when>
              <c:otherwise>
                Test is successfully validated.
              </c:otherwise>
            </c:choose>
            </h6>
          </div>
          <div class="card-body">
            <form id="newTestForm" class="row g-3" method="post" action="${pageContext.request.contextPath}/controller">
              <input type="hidden" name="command" value="createTest"/>
              <input type="hidden" name="filePath" value="${sessionScope.filePath}"/>
              <div class="col-md-4">
                <label class="form-label" for="title">Title: </label>
                <input type="text" id="title" name="title"
                       value="${sessionScope.test.title}">
              </div>
              <div class="col-md-5">
                <label class="form-label" for="questions">Questions in test:</label>
                <input type="number" id="questions"
                       name="numOfQuestions" value="${sessionScope.test.numOfQuestions}" readonly="readonly">
              </div>
              <div class="col-md-3">
                <label class="form-label" for="timeLimit1">Time limit (minutes):</label>
                <input type="number" id="timeLimit1" min="1" max="500" name="timeLimit" value="20">
              </div>
              <div class="col-md-3">
                <label class="form-label" for="minPercentage1">Passing barrier (%):</label>
                <input type="number" class="form-number" id="minPercentage1" min="10" max="100" name="minPercentage" value="70">
              </div>
              <div class="col-md-3">
                <label class="form-label" for="selectSubject">Subject:</label>
                <select id="selectSubject" class="form-control" name="subjectId">
                  <c:forEach items="${applicationScope.subjects}" var="subject">
                    <option value="${subject.id}">${subject.name}</option>
                  </c:forEach>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label class="form-label" for="selectDifficulty">Difficulty:</label>
                <select id="selectDifficulty" class="form-control" name="difficultyId">
                  <c:forEach items="${applicationScope.difficulties}" var="difficulty">
                    <option value="${difficulty.id}">${difficulty.name}</option>
                  </c:forEach>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label class="form-label" for="selectLanguage">Language:</label>
                <select id="selectLanguage" class="form-control form-select" name="language">
                  <option value="English">English</option>
                  <option value="Ukrainian">Ukrainian</option>
                </select>
              </div>
            </form>
          </div>
          <div class="card-footer justify-content-end">
            <button type="submit" class="btn btn-primary" form="newTestForm">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
