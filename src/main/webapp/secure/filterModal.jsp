<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<c:set var="locale" value="${sessionScope.loggedUser.localeString}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="filterModalLabel"
     aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="filterModalLabel"><f:message key="filter_results_by"/></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="post" action="${pageContext.request.contextPath}/controller" id="filterForm"
              class="mx-auto">
          <input type="hidden" name="command" value="filterTests">
          <input type="hidden" name="returnCommand" value="${param.command}">
          <p><f:message key="subjects"/>:</p>
          <c:forEach var="subject" items="${applicationScope.subjects}">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="subjects" value="${subject.id}"
                     id="subject${subject.id}"
              <c:forEach var="selectedSubjectId" items="${sessionScope.filteredSubjects}">
                ${selectedSubjectId == subject.id.toString() ? ' checked' : ''}
              </c:forEach>
              >
              <label class="form-check-label" for="subject${subject.id}">
                  ${locale == 'uk' ? subject.nameUk : subject.name}
              </label>
            </div>
          </c:forEach>
          <hr/>
          <p><f:message key="difficulties"/>:</p>
          <c:forEach var="difficulty" items="${applicationScope.difficulties}">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="difficulties" value="${difficulty.id}"
                     id="difficulty${difficulty.id}"
              <c:forEach var="selectedDifficultyId" items="${sessionScope.filteredDifficulties}">
                ${selectedDifficultyId == difficulty.id.toString() ? ' checked' : ''}
              </c:forEach>
              >
              <label class="form-check-label" for="difficulty${difficulty.id}">
                  ${locale == 'uk' ? difficulty.nameUk : difficulty.name}
              </label>
            </div>
          </c:forEach>
          <hr/>
          <p><f:message key="languages"/>:</p>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="languages" value="English"
                   id="english"
            <c:forEach var="selectedLanguage" items="${sessionScope.filteredLanguages}">
              ${selectedLanguage == 'English' ? ' checked' : ''}
            </c:forEach>
            >
            <label class="form-check-label" for="english">
              <c:if test="${locale == 'en'}">English</c:if>
              <c:if test="${locale == 'uk'}"><my:ukrLangName language="English"/></c:if>
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="languages" value="Ukrainian"
                   id="ukrainian"
            <c:forEach var="selectedLanguage" items="${sessionScope.filteredLanguages}">
              ${selectedLanguage == 'Ukrainian' ? ' checked' : ''}
            </c:forEach>
            >
            <label class="form-check-label" for="ukrainian">
              <c:if test="${locale == 'en'}">Ukrainian</c:if>
              <c:if test="${locale == 'uk'}"><my:ukrLangName language="Ukrainian"/></c:if>
            </label>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <a class="btn btn-danger"
           href="${pageContext.request.contextPath}/controller?command=showFoundTestsPage&page=1&new=true&sort=${param.sort}">
          <f:message key="reset"/>
        </a>
        <button type="submit" form="filterForm" class="btn btn-primary"><f:message key="do_filter"/></button>
      </div>
    </div>
  </div>
</div>