<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="locale"
       value="${not empty param.loc ?
                param.loc : not empty sessionScope.loggedUser ?
                sessionScope.loggedUser.localeString : pageContext.request.locale}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<% System.out.println(pageContext.getAttribute("locale")); %>
<div class="container">
  <header
      class="d-flex flex-wrap align-items-center justify-content-center
             justify-content-lg-between py-3 mb-4 border-bottom">
    
    <a href="${root}/${not empty param.loc ? '?loc=' += param.loc : ''}"
       class="d-flex align-items-center col-md-5 mb-2 mb-md-0 text-dark text-decoration-none">
      <span class="fs-4">Testing Platform</span>
    </a>
    
    <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
      <li><a href="${root}/${not empty param.loc ? '?loc=' += param.loc : ''}"
             class="nav-link px-2 link-dark"><f:message key="home"/></a></li>
      <li>
        <a href="${root}/controller?command=showFoundTestsPage&new=true"
           class="nav-link px-2 link-dark
                  <c:if test="${sessionScope.loggedUser == null || sessionScope.loggedUser.status == 'blocked'}"> disabled</c:if>">
          <f:message key="testing"/>
        </a>
      </li>
    </ul>
    
    <div class="col-md-5 text-end">
      <div class="dropdown">
        <c:if test="${sessionScope.loggedUser == null}">
          <a class="btn btn-outline-secondary"
             href="${root += pageContext.request.servletPath}${locale == 'en' ? '?loc=uk' : '?loc=en'}">
            ${locale == 'en' ? 'Українською' : 'View in English'}
          </a>
          <c:if test="${paramValues.size() == 0 ||
                        pageContext.request.servletPath.contains('signUp.jsp') ||
                        (paramValues.size() == 1 && not empty param.loc)}">
            <button class="btn btn-primary dropdown-toggle" type="button"
                    id="dropdownMenuButton" data-bs-auto-close="outside"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <f:message key="login"/>
            </button>
          </c:if>
          <c:if test="${paramValues.size() > 0 &&
                        !pageContext.request.servletPath.contains('signUp.jsp') &&
                        empty param.loc}">
            <button class="btn btn-primary dropdown-toggle" type="button"
                    id="dropdownMenuButton" data-bs-auto-close="outside"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <f:message key="login"/> <span class="badge bg-danger">!</span>
            </button>
          </c:if>
          <div class="dropdown-menu">
            <div class="dropdown-item-text text-danger text-center">
              <c:if test="${param.wrongPassword == true}">
                <h6 class="dropdown-header text-danger"><f:message key="incorrect_password"/></h6>
                <div class="dropdown-divider"></div>
              </c:if>
              <c:if test="${param.capsInPassword == true}">
                <h6 class="dropdown-header text-danger"><f:message key="incorrect_password_caps"/></h6>
                <div class="dropdown-divider"></div>
              </c:if>
              <c:if test="${param.noUser != null}">
                <h6 class="dropdown-header text-danger"><f:message key="no_account"/> '${param.noUser}'</h6>
                <div class="dropdown-divider"></div>
              </c:if>
              <c:if test="${param.invalidEmail == true}">
                <h6 class="dropdown-header text-danger"><f:message key="enter_valid_email"/></h6>
                <div class="dropdown-divider"></div>
              </c:if>
            </div>
            <form class="px-4 py-3"
                  action="${root}/controller" method="post">
              <input type="hidden" name="command" value="signIn">
              <div class="form-group">
                <label for="exampleDropdownFormEmail1" class="lh-lg"><f:message key="email"/>:</label>
                <input type="text" class="form-control"
                       placeholder="<f:message key='email'/>"
                       id="exampleDropdownFormEmail1"
                       name="email" value="${sessionScope.writtenEmail}">
              </div>
              <br/>
              <div class="form-group">
                <label for="exampleDropdownFormPassword1" class="lh-lg"><f:message key="password"/>:</label>
                <input type="password" class="form-control"
                       id="exampleDropdownFormPassword1"
                       placeholder="<f:message key='password'/>" name="password">
              </div>
              <br/><br/>
              <div class="d-grid gap-1 col-12 mx-auto">
                <button type="submit" class="btn btn-primary btn"><f:message key="sign_in"/></button>
              </div>
            </form>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item"
               href="${root}/signUp.jsp${not empty param.loc ? '?loc=' += param.loc : ''}">
              <f:message key="new_around_here"/>
            </a>
              <%--<a class="dropdown-item" href="#">Forgot password?</a>--%>
          </div>
        </c:if>
        
        <c:if test="${sessionScope.loggedUser != null}">
          <c:if test="${param.command == 'showManageUsersPage'
                        || param.command == 'showManageTestsPage'
                        || param.command == 'showUserTestResultsPage'
                        || pageContext.request.servletPath == '/protected/foundTests.jsp'}">
            <c:if test="${pageContext.request.servletPath == '/protected/foundTests.jsp'}">
              <button type="button" data-bs-toggle="modal" data-bs-target="#filterModal"
                      <c:if test="${sessionScope.filtered == false}">
                        class="btn btn-light">
                      </c:if>
                      <c:if test="${sessionScope.filtered == true}">
                        class="btn btn-danger">
                      </c:if>
                <f:message key="filter"/>
              </button>
            </c:if>
            <button class="btn btn-light dropdown-toggle" data-bs-auto-toggle="outside"
                    type="button" id="showButton"
                    data-bs-toggle="dropdown" aria-expanded="false">
              <f:message key="view"/>
            </button>
            <ul class="dropdown-menu">
              <li><h6 class="dropdown-header text-muted text-center"><f:message key="rows_per_page"/></h6></li>
              <div class="dropdown-divider"></div>
              <li>
                <button class="dropdown-item" onclick="setItemQuantity(5, '/controller?command=${param.command}')">
                  5
                </button>
                <button class="dropdown-item" onclick="setItemQuantity(10, '/controller?command=${param.command}')">
                  10
                </button>
                <button class="dropdown-item" onclick="setItemQuantity(15, '/controller?command=${param.command}')">
                  15
                </button>
                <button class="dropdown-item" onclick="setItemQuantity(30, '/controller?command=${param.command}')">
                  30
                </button>
                <c:if test="${pageContext.request.servletPath == '/secure/manageTests.jsp'}">
                  <div class="dropdown-divider"></div>
                  <button type="button" class="dropdown-item" data-bs-toggle="modal" data-bs-target="#filterModal">
                    Filter
                  </button>
                </c:if>
              </li>
            </ul>
          </c:if>
          <c:if test="${sessionScope.loggedUser.id == 1}">
            <c:choose>
              <c:when test="${param.successfulUpdate == true}">
                <button class="btn btn-success dropdown-toggle" data-bs-auto-close="outside"
                        type="button" id="dropdownMenuButton2"
                        data-bs-toggle="dropdown" aria-expanded="false">
                  Tools
                </button>
              </c:when>
              <c:when test="${param.validationFailed == true ||
                              param.invalidNameError == true ||
                              param.invalidTestError == true}">
                <button class="btn btn-light dropdown-toggle"
                        type="button" id="dropdownMenuButton2"
                        data-bs-toggle="dropdown" aria-expanded="false">
                  Tools <span class="badge bg-danger">!</span>
                </button>
              </c:when>
              <c:otherwise>
                <button class="btn btn-light dropdown-toggle"
                        type="button" id="dropdownMenuButton2"
                        data-bs-toggle="dropdown" aria-expanded="false">
                  Tools
                </button>
              </c:otherwise>
            </c:choose>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
              <c:if test="${param.validationFailed == true || param.invalidTestError == true}">
                <li><h6 class="dropdown-header text-danger">Test validation failed</h6></li>
                <div class="dropdown-divider"></div>
              </c:if>
              <c:if test="${param.successfulUpdate == true}">
                <li><h6 class="dropdown-header text-success">Successfully updated</h6></li>
                <div class="dropdown-divider"></div>
              </c:if>
              <c:if test="${param.invalidNameError == true}">
                <li><h6 class="dropdown-header text-danger">File name don't match</h6></li>
                <div class="dropdown-divider"></div>
              </c:if>
              <c:if test="${param.repeat == true}">
                <li><h6 class="dropdown-header text-danger">Name must start with capital letter.
                  Password must contain be at least 8 characters long and contain at least one number.</h6></li>
                <div class="dropdown-divider"></div>
              </c:if>
              <li><h6 class="dropdown-header">Users</h6></li>
              <li><a class="dropdown-item"
                     href="${root}/controller?command=showManageUsersPage&sort=email&page=1">
                Manage users</a></li>
              <li><a class="dropdown-item"
                     href="${root}/signUp.jsp">
                Create new user account</a></li>
              <li><h6 class="dropdown-header">Tests</h6></li>
              <li><a class="dropdown-item"
                     href="${root}/controller?command=showManageTestsPage&page=1&sort=title&new=true">
                Manage tests</a></li>
              <li>
                <button type="button" class="dropdown-item" data-bs-toggle="modal" data-bs-target="#newTestUploadModal">
                  Add new test
                </button>
              </li>
              <li><h6 class="dropdown-header">Download</h6></li>
              <li><a class="dropdown-item"
                     href="${root}/controller?command=download&file=testExample">
                Test example</a></li>
              <li><a class="dropdown-item"
                     href="${root}/controller?command=download&file=testSchema">
                Test schema</a></li>
              <li><h6 class="dropdown-header">Other</h6></li>
              <li><a class="dropdown-item" href="${root}/secure/subjAndDiff.jsp">Subjects & difficulties</a></li>
            </ul>
          </c:if>
          
          <button class="btn
                    <c:choose>
                        <c:when test="${sessionScope.loggedUser.status == 'blocked'}">btn-danger</c:when>
                        <c:otherwise>btn-secondary</c:otherwise>
                    </c:choose>
                  dropdown-toggle" type="button" id="dropdownMenuButton1"
                  data-bs-toggle="dropdown" aria-expanded="false">
              ${sessionScope.loggedUser.firstName} ${sessionScope.loggedUser.lastName}
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <c:if test="${sessionScope.loggedUser.status == 'blocked'}">
              <li><h6 class="dropdown-header text-danger"><f:message key="blocked_message"/></h6></li>
              <li><a class="dropdown-item disabled" aria-disabled="true">
                <f:message key="my_profile"/>
              </a></li>
              <li><a class="dropdown-item disabled" aria-disabled="true">
                <f:message key="my_test_results"/>
              </a></li>
            </c:if>
            <c:if test="${sessionScope.loggedUser.status == 'active'}">
              <li><a class="dropdown-item" href="${root}/protected/myProfile.jsp">
                <f:message key="my_profile"/>
              </a></li>
              <li><a class="dropdown-item"
                     href="${root}/controller?command=showUserTestResultsPage&user=true&sort=submissionDate&reversed=true">
                <f:message key="my_test_results"/>
              </a></li>
            </c:if>
            <div class="dropdown-divider"></div>
            <li>
              <button class="dropdown-item" onclick="logOut()">
                <f:message key="log_out"/>
              </button>
            </li>
          </ul>
        </c:if>
      </div>
    </div>
  </header>
</div>
<c:if test="${sessionScope.loggedUser.id == 1}">
  <!-- Test upload modal -->
  <div class="modal fade" id="newTestUploadModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="newTestUploadModalLabel">Upload new test</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <p>XML file must be written according to
            <a href="${root}/controller?command=download&file=testSchema">schema</a>.
            Otherwise validation will fail.</p>
          <form method="post" action="${root}/upload/test"
                enctype="multipart/form-data" id="uploadTestForm"
                class="mx-auto">
              <%--        <label for="newTestFile">Test file:</label>--%>
            <div class="form-group align-content-center">
              <input type="file" id="newTestFile" accept=".xml" name="file" required>
            </div>
          </form>
        </div>
          <%--            <input type="submit" value="Upload" name="upload">--%>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" form="uploadTestForm" class="btn btn-primary">Upload</button>
        </div>
      </div>
    </div>
  </div>
</c:if>
<script async>
    async function logOut() {
        const xhr = new XMLHttpRequest();
        const url = "${root}/controller";
        const data = "command=logOut";
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                console.log(xhr.responseText);
            }
        };
        xhr.send(data);
        await new Promise(resolve => setTimeout(resolve, 500));
        window.location.replace("${root}");
    }

    async function setItemQuantity(quantity, path) {
        const xhr = new XMLHttpRequest();
        const url = "${root}/controller";
        const data = "command=setItemQuantity&quantity=" + quantity;
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                console.log(xhr.responseText);
            }
        };
        xhr.send(data);
        await new Promise(resolve => setTimeout(resolve, 800));
        window.location.replace("${root}" + path);
    }
</script>
