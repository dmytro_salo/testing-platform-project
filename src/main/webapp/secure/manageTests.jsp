<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
  System.out.println("jsp: manageTests.jsp");
  int k = (int) request.getAttribute("itemsPerPage") * ((int) request.getAttribute("currentPage") - 1);
%>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<html>
<head>
  <title>Manage Tests</title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous" async></script>
</head>
<body>
  <jsp:include page="header.jsp"/>
  <div class="container-xxl">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${root}/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Manage tests</li>
      </ol>
    </nav>
    <table class="table table-bordered table-striped align-middle text-center">
      <tr class="table-header">
        <th scope="col">#</th>
        <th scope="col">
          <c:if test="${param.sort != 'title' || (param.sort == 'title' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=title<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${(param.sort == 'title' && param.reversed != true) || param.sort == null}">
            <a href="${root}/controller?command=showManageTestsPage&sort=title&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${(param.sort == 'title' && param.reversed != true) || param.sort == null}">Title &#9660;</c:if>
              <c:if test="${(param.sort == 'title' && param.reversed == true)}">Title &#9650;</c:if>
              <c:if test="${param.sort != 'title' && param.sort != null}">Title</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'subject' || (param.sort == 'subject' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=subject<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
          </c:if>
          <c:if test="${param.sort == 'subject' && param.reversed != true}">
          <a href="${root}/controller?command=showManageTestsPage&sort=subject&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
          </c:if>
            <c:if test="${param.sort == 'subject' && param.reversed != true}">Subject &#9660</c:if>
            <c:if test="${param.sort == 'subject' && param.reversed == true}">Subject &#9650</c:if>
            <c:if test="${param.sort != 'subject'}">Subject</c:if>
          </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'difficulty' || (param.sort == 'difficulty' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=difficulty<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'difficulty' && param.reversed != true}">
            <a href="${root}/controller?command=showManageTestsPage&sort=difficulty&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'difficulty' && param.reversed != true}">Difficulty &#9660</c:if>
              <c:if test="${param.sort == 'difficulty' && param.reversed == true}">Difficulty &#9650</c:if>
              <c:if test="${param.sort != 'difficulty'}">Difficulty</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'language' || (param.sort == 'language' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=language<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'language' && param.reversed != true}">
            <a href="${root}/controller?command=showManageTestsPage&sort=language&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'language' && param.reversed != true}">Language &#9660</c:if>
              <c:if test="${param.sort == 'language' && param.reversed == true}">Language &#9650</c:if>
              <c:if test="${param.sort != 'language'}">Language</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'numOfQuestions' || (param.sort == 'numOfQuestions' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=numOfQuestions<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'numOfQuestions' && param.reversed != true}">
            <a href="${root}/controller?command=showManageTestsPage&sort=numOfQuestions&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'numOfQuestions' && param.reversed != true}">Questions &#9660</c:if>
              <c:if test="${param.sort == 'numOfQuestions' && param.reversed == true}">Questions &#9650</c:if>
              <c:if test="${param.sort != 'numOfQuestions'}">Questions</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'timeLimit' || (param.sort == 'timeLimit' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=timeLimit<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'timeLimit' && param.reversed != true}">
            <a href="${root}/controller?command=showManageTestsPage&sort=timeLimit&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'timeLimit' && param.reversed != true}">Time limit (minutes) &#9660</c:if>
              <c:if test="${param.sort == 'timeLimit' && param.reversed == true}">Time limit (minutes) &#9650</c:if>
              <c:if test="${param.sort != 'timeLimit'}">Time limit (minutes)</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'minPercentage' || (param.sort == 'minPercentage' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=minPercentage<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'minPercentage' && param.reversed != true}">
            <a href="${root}/controller?command=showManageTestsPage&sort=minPercentage&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'minPercentage' && param.reversed != true}">Minimum % &#9660</c:if>
              <c:if test="${param.sort == 'minPercentage' && param.reversed == true}">Minimum % &#9650</c:if>
              <c:if test="${param.sort != 'minPercentage'}">Minimum %</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'numOfSubmissions' || (param.sort == 'numOfSubmissions' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=numOfSubmissions<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'numOfSubmissions' && param.reversed != true}">
            <a href="${root}/controller?command=showManageTestsPage&sort=numOfSubmissions&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'numOfSubmissions' && param.reversed != true}">Number of submissions &#9660</c:if>
              <c:if test="${param.sort == 'numOfSubmissions' && param.reversed == true}">Number of submissions &#9650</c:if>
              <c:if test="${param.sort != 'numOfSubmissions'}">Number of submissions</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'numOfPasses' || (param.sort == 'numOfPasses' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=numOfPasses<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'numOfPasses' && param.reversed != true}">
            <a href="${root}/controller?command=showManageTestsPage&sort=numOfPasses&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'numOfPasses' && param.reversed != true}">Number of passes &#9660</c:if>
              <c:if test="${param.sort == 'numOfPasses' && param.reversed == true}">Number of passes &#9650</c:if>
              <c:if test="${param.sort != 'numOfPasses'}">Number of passes</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'passRate' || (param.sort == 'passRate' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=passRate<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'passRate' && param.reversed != true}">
            <a href="${root}/controller?command=showManageTestsPage&sort=passRate&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'passRate' && param.reversed != true}">Pass rate &#9660</c:if>
              <c:if test="${param.sort == 'passRate' && param.reversed == true}">Pass rate &#9650</c:if>
              <c:if test="${param.sort != 'passRate'}">Pass rate</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'creationDate' || (param.sort == 'creationDate' && param.reversed == true)}">
          <a href="${root}/controller?command=showManageTestsPage&sort=creationDate<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'creationDate' && param.reversed != true}">
            <a href="${root}/controller?command=showManageTestsPage&sort=creationDate&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'creationDate' && param.reversed != true}">Created &#9660</c:if>
              <c:if test="${param.sort == 'creationDate' && param.reversed == true}">Created &#9650</c:if>
              <c:if test="${param.sort != 'creationDate'}">Created</c:if>
            </a>
        </th>
        <th scope="col">Manage</th>
      </tr>
      <c:forEach var="test" items="${requestScope.tests}">
        <tr>
          <th><%= ++k %>
          </th>
          <td><c:out value="${test.title}"/></td>
          <td><c:out value="${test.subject.name}"/></td>
          <td><c:out value="${test.difficulty.name}"/></td>
          <td><c:out value="${test.language}"/></td>
          <td><c:out value="${test.numOfQuestions}"/></td>
          <td><c:out value="${test.timeLimit}"/></td>
          <td><c:out value="${test.minPercentage}"/></td>
          <td><c:out value="${test.numOfSubmissions}"/></td>
          <td><c:out value="${test.numOfPasses}"/></td>
          <td><c:out value="${test.passRate}"/></td>
          <td><c:out value="${test.creationDate}"/></td>
          <td>
            <div class="dropdown">
              <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button"
                      id="testOptions" data-bs-toggle="dropdown" aria-expanded="false">
                Options
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li>
                  <a class="dropdown-item"
                     href="${root}/controller?command=showConfigureTestPage&id=${test.id}">
                    Configure properties
                  </a>
                </li>
                <li>
                  <button type="button" class="dropdown-item" data-bs-toggle="modal"
                          data-bs-target="#contentUpdateModal${test.id}">
                    Update content
                  </button>
                </li>
                <li><a class="dropdown-item"
                       href="${root}/controller?command=download&file=test&id=${test.id}">
                  Download
                </a>
                </li>
                <li>
                  <button type="button" class="dropdown-item" data-bs-toggle="modal"
                          data-bs-target="#deleteModal${test.id}">
                    Delete
                  </button>
                </li>
                <hr class="dropdown-divider"/>
                <li>
                  <button type="button" class="dropdown-item" data-bs-toggle="modal"
                          data-bs-target="#editingProcedureModal">
                    Editing procedure
                  </button>
                </li>
              </ul>
            </div>
          </td>
        </tr>
        <!-- Delete modal -->
        <div class="modal fade" id="deleteModal${test.id}" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel${test.id}">Delete test?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <p>This operation will completely remove this test from server.</p>
                <p>Test title: <span class="fw-bolder">"${test.title}"</span></p>
                <p>Are you sure? This can't be undone.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" onclick="deleteTest(${test.id})">Delete</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Content update modal -->
        <div class="modal fade" id="contentUpdateModal${test.id}" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="contentUpdateModalLabel${test.id}">Update test content</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <p>Test title: <span class="fw-bolder">"${test.title}"</span></p>
                <form method="post" action="${root}/upload/update"
                      enctype="multipart/form-data" id="editTestForm${test.id}"
                      class="mx-auto">
                  <input type="hidden" name="id" value="${test.id}">
                  <div class="mb-3">
                    <label for="formFile${test.id}" class="form-label">
                      Test file name: <span class="fw-bolder">test_${test.id}.xml</span>
                    </label>
                    <input class="form-control" type="file" id="formFile${test.id}" accept=".xml" name="file" required>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" form="editTestForm${test.id}" class="btn btn-primary">Update</button>
              </div>
            </div>
          </div>
        </div>
      </c:forEach>
    </table>
  </div>
  <jsp:include page="paginationFooter.jsp"/>
  <!-- Editing procedure modal -->
  <div class="modal fade" id="editingProcedureModal" tabindex="-1" aria-labelledby="exampleModalLabel"
       aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="testEditingModalLabel">Test editing procedure</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <ol type="1">
            <li>Download the test. Don't change file name!</li>
            <li>Edit test file. Total amount of questions can be changed.</li>
            <li>Note, that test title can be changed only in "configure properties" menu.
              Be careful not to break document validity.
            </li>
            <li>Select "Edit questions" in the test options menu and upload the modified document.
              After validation, changes will be applied.
            </li>
          </ol>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Got it</button>
        </div>
      </div>
    </div>
  </div>
  <jsp:include page="filterModal.jsp"/>
  <script async>
      async function deleteTest(id) {
          const xhr = new XMLHttpRequest();
          const url = "${root}/controller";
          const data = "command=deleteTest&id=" + id;
          xhr.open("POST", url, true);
          xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhr.onreadystatechange = function () {
              if (xhr.readyState === 4 && xhr.status === 200) {
                  console.log(xhr.responseText);
              }
          };
          xhr.send(data);
          await new Promise(resolve => setTimeout(resolve, 1600));
          window.location.href = "${root}/controller?command=showManageTestsPage&page=1&sort=title&new=true";
      }
  </script>
</body>
</html>
