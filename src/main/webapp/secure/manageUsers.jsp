<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
  System.out.println("jsp: manageUsers.jsp");
  int k = (int) request.getAttribute("itemsPerPage") * ((int) request.getAttribute("currentPage") - 1);
%>
<html>
<head>
  <title>Manage Users</title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous" async></script>
</head>
<body>
  <jsp:include page="header.jsp"/>
  <div class="container-md">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Manage users</li>
      </ol>
    </nav>
    <table class="table table-bordered table-striped align-middle text-center">
      <tr class="table-header">
        <th scope="col">#</th>
        <th scope="col">
          <c:if test="${param.sort != 'email' || (param.sort == 'email' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=email<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
          </c:if>
          <c:if test="${(param.sort == 'email' && param.reversed != true) || param.sort == null}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=email&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
          </c:if>
              <c:if test="${(param.sort == 'email' && param.reversed != true) || param.sort == null}">Email &#9660;</c:if>
              <c:if test="${(param.sort == 'email' && param.reversed == true)}">Email &#9650;</c:if>
              <c:if test="${param.sort != 'email' && param.sort != null}">Email</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'firstName' || (param.sort == 'firstName' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=firstName<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
          </c:if>
          <c:if test="${param.sort == 'firstName' && param.reversed != true}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=firstName&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
          </c:if>
             <c:if test="${param.sort == 'firstName' && param.reversed != true}">First name &#9660</c:if>
             <c:if test="${param.sort == 'firstName' && param.reversed == true}">First name &#9650</c:if>
             <c:if test="${param.sort != 'firstName'}">First name</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'lastName' || (param.sort == 'lastName' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=lastName<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
          </c:if>
          <c:if test="${param.sort == 'lastName' && param.reversed != true}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=lastName&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
          </c:if>
              <c:if test="${param.sort == 'lastName' && param.reversed != true}">Last name &#9660</c:if>
              <c:if test="${param.sort == 'lastName' && param.reversed == true}">Last name &#9650</c:if>
              <c:if test="${param.sort != 'lastName'}">Last name</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'role' || (param.sort == 'role' && param.reversed == true)}">
          <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=role<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'role' && param.reversed != true}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=role&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'role' && param.reversed != true}">Role &#9660</c:if>
              <c:if test="${param.sort == 'role' && param.reversed == true}">Role &#9650</c:if>
              <c:if test="${param.sort != 'role'}">Role</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'status' || (param.sort == 'status' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=status<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
          </c:if>
          <c:if test="${param.sort == 'status' && param.reversed != true}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=status&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
          </c:if>
              <c:if test="${param.sort == 'status' && param.reversed != true}">Status &#9660</c:if>
              <c:if test="${param.sort == 'status' && param.reversed == true}">Status &#9650</c:if>
              <c:if test="${param.sort != 'status'}">Status</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'language' || (param.sort == 'language' && param.reversed == true)}">
          <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=language<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'language' && param.reversed != true}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=language&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'language' && param.reversed != true}">Preferred language &#9660</c:if>
              <c:if test="${param.sort == 'language' && param.reversed == true}">Preferred language &#9650</c:if>
              <c:if test="${param.sort != 'language'}">Preferred language</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'averagePercentage' || (param.sort == 'averagePercentage' && param.reversed == true)}">
          <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=averagePercentage<c:if test="${param.page != null}">&page=${param.page}</c:if>"
             class="text-decoration-none link-dark">
            </c:if>
            <c:if test="${param.sort == 'averagePercentage' && param.reversed != true}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=averagePercentage&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
              </c:if>
              <c:if test="${param.sort == 'averagePercentage' && param.reversed != true}">Success rate &#9660</c:if>
              <c:if test="${param.sort == 'averagePercentage' && param.reversed == true}">Success rate &#9650</c:if>
              <c:if test="${param.sort != 'averagePercentage'}">Success rate</c:if>
            </a>
        </th>
        <th scope="col">
          <c:if test="${param.sort != 'registrationDate' || (param.sort == 'registrationDate' && param.reversed == true)}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=registrationDate<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
          </c:if>
          <c:if test="${param.sort == 'registrationDate' && param.reversed != true}">
            <a href="${pageContext.request.contextPath}/controller?command=showManageUsersPage&sort=registrationDate&reversed=true<c:if test="${param.page != null}">&page=${param.page}</c:if>"
               class="text-decoration-none link-dark">
          </c:if>
              <c:if test="${param.sort == 'registrationDate' && param.reversed != true}">Registration date &#9660</c:if>
              <c:if test="${param.sort == 'registrationDate' && param.reversed == true}">Registration date &#9650</c:if>
              <c:if test="${param.sort != 'registrationDate'}">Registration date</c:if>
            </a>
        </th>
        <th scope="col">Manage</th>
      </tr>
      <c:forEach var="user" items="${requestScope.users}">
        <tr>
          <th><%= ++k %>
          </th>
          <td><c:out value="${user.email}"/></td>
          <td><c:out value="${user.firstName}"/></td>
          <td><c:out value="${user.lastName}"/></td>
          <td><c:out value="${user.role}"/></td>
          <td><c:out value="${user.status}"/></td>
          <td><c:out value="${user.preferredLanguage}"/></td>
          <td><c:out value="${user.averagePercentage}"/></td>
          <td><c:out value="${user.registrationDate}"/></td>
          <td>
            <div class="dropdown">
              <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button"
                      id="userOptions" data-bs-toggle="dropdown" aria-expanded="false">
                Options
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <li>
                  <button class="dropdown-item" data-bs-toggle="modal"
                          data-bs-target="#editUserModal${user.id}">
                    Edit
                  </button>
                </li>
                <c:if test="${user.id != 1}">
                  <li>
                    <button class="dropdown-item" data-bs-toggle="modal"
                            data-bs-target="#deleteUserModal${user.id}">
                      Delete
                    </button>
                  </li>
                  <c:if test="${user.status == 'active'}">
                    <li>
                      <button
                          class="dropdown-item"
                          data-bs-toggle="modal" data-bs-target="#blockUserModal${user.id}">
                        Block
                      </button>
                    </li>
                  </c:if>
                  <c:if test="${user.status == 'blocked'}">
                    <li>
                      <button
                          class="dropdown-item"
                          data-bs-toggle="modal" data-bs-target="#unblockUserModal${user.id}">
                        Unblock
                      </button>
                    </li>
                  </c:if>
                </c:if>
                <li>
                  <a
                      class="dropdown-item"
                      href="${pageContext.request.contextPath}/controller?command=showUserTestResultsPage&userId=${user.id}"
                  >
                    Show test results
                  </a>
                </li>
              </ul>
            </div>
          </td>
        </tr>
        <!-- Edit modal -->
        <div class="modal fade" id="editUserModal${user.id}" tabindex="-1"
             aria-labelledby="editUserModalLabel${user.id}" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="editUserModalLabel${user.id}">Edit user</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <form action="${pageContext.request.contextPath}/controller" method="post"
                      id="editUserForm${user.id}">
                  <input type="hidden" name="command" value="editUser">
                  <input type="hidden" name="id" value="${user.id}">
                  <input type="hidden" name="user" value="false">
                  <div class="form-group">
                    <div class="form-text">
                      <label for="editEmail">Email:</label>
                      <input type="text" name="email" id="editEmail" value="${user.email}" readonly disabled>
                    </div>
                  </div>
                  <br/>
                  <div class="form-group">
                    <div class="form-text">
                      <label for="editFirstName">First name</label>
                      <input type="text" name="firstName" id="editFirstName" value="${user.firstName}" required>
                    </div>
                    <div class="form-text">
                      <label for="editLastName">Last name</label>
                      <input type="text" name="lastName" id="editLastName" value="${user.lastName}" required>
                    </div>
                  </div>
                  <br/>
                  <p>Preferred language:</p>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="preferredLanguage" value="English"
                           id="english" ${user.preferredLanguage == 'English' ? ' checked' : ''}>
                    <label class="form-check-label" for="english">
                      English
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="preferredLanguage" value="Ukrainian"
                           id="ukrainian" ${user.preferredLanguage == 'Ukrainian' ? ' checked' : ''}>
                    <label class="form-check-label" for="ukrainian">
                      Ukrainian
                    </label>
                  </div>
                  <br/>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" form="editUserForm${user.id}" class="btn btn-primary">Edit</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Delete modal -->
        <div class="modal fade" id="deleteUserModal${user.id}" tabindex="-1"
             aria-labelledby="deleteUserModalLabel${user.id}" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="deleteUserModalLabel${user.id}">Delete user account?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <p>This account <span class="fw-bolder">"${user.email}"</span> will
                  be permanently deleted from the database.</p>
                <p>Are you sure? This can't be undone.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" onclick="deleteUser(${user.id})">Delete</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Block modal -->
        <div class="modal fade" id="blockUserModal${user.id}" tabindex="-1"
             aria-labelledby="blockUserModalLabel${user.id}" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="blockUserModalLabel${user.id}">Block user account?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <p>User account <span class="fw-bolder">"${user.email}"</span> will be blocked.</p>
                <p>Are you sure? You can unblock it any time later</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" onclick="blockUser(${user.id})">Block</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Unblock modal -->
        <div class="modal fade" id="unblockUserModal${user.id}" tabindex="-1"
             aria-labelledby="unblockUserModalLabel${user.id}" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="unblockUserModalLabel${user.id}">Unblock user account?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <p>User account <span class="fw-bolder">"${user.email}"</span> will be unblocked.</p>
                <p>Are you sure? You can block it any time later</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" onclick="unblockUser(${user.id})">Unblock</button>
              </div>
            </div>
          </div>
        </div>
      </c:forEach>
    </table>
  </div>
  <jsp:include page="paginationFooter.jsp"/>
  <script async>
      async function deleteUser(id) {
          const xhr = new XMLHttpRequest();
          const url = "${pageContext.request.contextPath}/controller";
          const data = "command=deleteUser&id=" + id;

          xhr.open("POST", url, true);
          xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhr.send(data);

          await new Promise(resolve => setTimeout(resolve, 500));

          window.location.href = "${pageContext.request.contextPath}/controller?command=showManageUsersPage";
      }

      async function blockUser(id) {
          const xhr = new XMLHttpRequest();
          const url = "${pageContext.request.contextPath}/controller";
          const data = "command=blockUser&id=" + id;

          xhr.open("POST", url, true);
          xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhr.send(data);

          await new Promise(resolve => setTimeout(resolve, 500));

          window.location.href = "${pageContext.request.contextPath}/controller?command=showManageUsersPage";
      }

      async function unblockUser(id) {
          const xhr = new XMLHttpRequest();
          const url = "${pageContext.request.contextPath}/controller";
          const data = "command=unblockUser&id=" + id;

          xhr.open("POST", url, true);
          xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhr.send(data);

          await new Promise(resolve => setTimeout(resolve, 500));

          window.location.href = "${pageContext.request.contextPath}/controller?command=showManageUsersPage";
      }
  </script>
</body>
</html>
