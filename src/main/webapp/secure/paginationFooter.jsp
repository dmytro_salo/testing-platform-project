<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<f:setLocale value="${sessionScope.loggedUser.localeString}"/>
<f:setBundle basename="text"/>
<br/><br/><br/>
<footer class="footer mt-auto py-3 bg-light fixed-bottom">
  <div class="container-md">
    <nav aria-label="Page navigation example">
      <ul class="pagination justify-content-center">
        <li class="page-item<c:if test="${requestScope.currentPage == 1}"> disabled</c:if>">
          <a class="page-link"
              <c:if test="${requestScope.sort == null}">
                href="${pageContext.request.contextPath}/controller?command=${param.command}&page=${requestScope.currentPage - 1}"
              </c:if>
              <c:if test="${requestScope.sort != null}">
                href="${pageContext.request.contextPath}/controller?command=${param.command}&page=${requestScope.currentPage - 1}&sort=${requestScope.sort}<c:if test="${param.reversed == true}">&reversed=true</c:if>"
              </c:if>
          >
            <f:message key="previous"/></a>
        </li>
        <c:forEach begin="1" end="${requestScope.pageQuantity}" var="pageNumber">
          <li class="page-item<c:if test="${requestScope.currentPage == pageNumber}"> active</c:if>">
            <a class="page-link"
                <c:if test="${requestScope.sort == null}">
                  href="${pageContext.request.contextPath}/controller?command=${param.command}&page=${pageNumber}"
                </c:if>
                <c:if test="${requestScope.sort != null}">
                  href="${pageContext.request.contextPath}/controller?command=${param.command}&page=${pageNumber}&sort=${requestScope.sort}<c:if test="${param.reversed == true}">&reversed=true</c:if>"
                </c:if>
            >
                ${pageNumber}
            </a>
          </li>
        </c:forEach>
        <li class="page-item<c:if test="${requestScope.currentPage == requestScope.pageQuantity}"> disabled</c:if>">
          <a class="page-link"
              <c:if test="${requestScope.sort == null}">
                href="${pageContext.request.contextPath}/controller?command=${param.command}&page=${requestScope.currentPage + 1}"
              </c:if>
              <c:if test="${requestScope.sort != null}">
                href="${pageContext.request.contextPath}/controller?command=${param.command}&page=${requestScope.currentPage + 1}&sort=${requestScope.sort}<c:if test="${param.reversed == true}">&reversed=true</c:if>"
              </c:if>
          >
            <f:message key="next"/></a>
        </li>
      </ul>
    </nav>
  </div>
</footer>
