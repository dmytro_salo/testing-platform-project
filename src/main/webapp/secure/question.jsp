<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="my" uri="/WEB-INF/myTagLib.tld" %>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags" %>
<c:set var="locale" value="${sessionScope.loggedUser.localeString}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<%
  System.out.println("jsp: question.jsp");
%>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="k" value="0"/>
<html>
<head>
  <title><f:message key="testing_in_progress"/></title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
  <link href="${root}/static/css/sidebars.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous" async></script>
  <script src="${root}/static/js/sidebars.js" async></script>
</head>
<body>
  <main>
    <div class="d-flex flex-column align-items-stretch flex-shrink-0 bg-white" style="width: 380px;">
      <div class="list-group list-group-flush border-bottom scrollarea">
        <c:forEach items="${sessionScope.startedTest.questions}" var="question">
          <c:set var="k" value="${k + 1}"/>
          <button onclick="goTo(${k})" type="submit" form="answerForm"
                  class="list-group-item list-group-item-action py-3
                     <c:if test="${requestScope.currentQuestionNumber == k}">active </c:if>lh-tight"
                  <c:if test="${requestScope.currentQuestionNumber == k}">aria-current="true"</c:if>
          >
            <div class="d-flex w-100 align-items-center justify-content-between">
              <small>Question ${k}</small>
              <c:if test="${sessionScope.answerMap.get(question.id) != null}">
                <small class="text-success"><f:message key="answered"/></small>
              </c:if>
              <c:if test="${sessionScope.answerMap.get(question.id) == null}">
                <small class="text-warning"><f:message key="not_answered"/></small>
              </c:if>
            </div>
            <div class="col-10 mb-1 small">${question.preview}</div>
          </button>
        </c:forEach>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-4">
            <c:if test="${sessionScope.allAnswered == true}">
              <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#submitModal">
                <f:message key="submit_test"/>
              </button>
            </c:if>
            <c:if test="${sessionScope.allAnswered == false}">
              <button class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#submitModal">
                <f:message key="submit_test"/>
              </button>
            </c:if>
            <span class="font-monospace">&nbsp;</span>
            <span class="font-monospace <my:timerColor deadline="${sessionScope.deadline}"/>"
                  id="submitTimer"></span>
          </div>
        </div>
      </div>
      <div class="card-body">
        <br/>
        <p>${requestScope.currentQuestion.questionText}</p>
        <br/>
        <form id="answerForm" action="${root}/controller" method="post">
          <input type="hidden" name="command" value="receiveAnswer" id="command"/>
          <input type="hidden" name="questionId" value="${requestScope.currentQuestion.id}" id="questionId"/>
          <div class="list-group mx-0">
            <c:if test="${requestScope.currentQuestion.multipleAnswers == false}">
              <c:forEach items="${requestScope.currentQuestion.answers}" var="answer">
                <label class="list-group-item d-flex gap-2">
                  <input class="form-check-input flex-shrink-0" type="radio"
                         name="selectedAnswers"
                         value="${answer.id}"
                      <c:if test="${sessionScope.answerMap.get(requestScope.currentQuestion.id) != null}">
                        <c:set var="answerArray"
                               value="${sessionScope.answerMap.get(requestScope.currentQuestion.id)}"/>
                        <c:if test="${answerArray[0] == answer.id}"> checked</c:if>
                      </c:if>/>
                  <span>${answer.text}</span>
                </label>
              </c:forEach>
            </c:if>
            <c:if test="${requestScope.currentQuestion.multipleAnswers == true}">
              <c:forEach items="${requestScope.currentQuestion.answers}" var="answer">
                <label class="list-group-item d-flex gap-2">
                  <input class="form-check-input flex-shrink-0" type="checkbox"
                         name="selectedAnswers"
                         value="${answer.id}"
                      <c:if test="${sessionScope.answerMap.get(requestScope.currentQuestion.id) != null}">
                        <c:set var="answerArray"
                               value="${sessionScope.answerMap.get(requestScope.currentQuestion.id)}"/>
                        <c:forEach var="checkedAnswer" items="${answerArray}">
                          <c:if test="${checkedAnswer == answer.id}"> checked</c:if>
                        </c:forEach>
                      </c:if>/>
                  <span>${answer.text}</span>
                </label>
              </c:forEach>
            </c:if>
          </div>
        </form>
      </div>
      <div class="card-footer justify-content-center">
        <div class="btn-group" role="group">
          <button class="btn btn-outline-primary
                        <c:if test="${requestScope.currentQuestionNumber == 1}"> disabled</c:if>"
                  onclick="goTo(${requestScope.currentQuestionNumber - 1})" type="submit" form="answerForm">
            <f:message key="previous_m"/>
          </button>
          <button class="btn-outline-primary btn" disabled>
            <span class="text-dark">${requestScope.currentQuestionNumber}</span>
          </button>
          <button class="btn btn-outline-primary
                        <c:if test="${requestScope.currentQuestionNumber == sessionScope.startedTest.questions.size()}"> disabled</c:if>"
                  onclick="goTo(${requestScope.currentQuestionNumber + 1})" type="submit" form="answerForm">
            <f:message key="next_m"/>
          </button>
        </div>
      </div>
    </div>
  </main>
  <!-- Submit modal -->
  <div class="modal fade" id="submitModal" tabindex="-1" aria-labelledby="exampleModalLabel"
       aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="submitModalLabel"><f:message key="submit_test"/></h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body text-center">
          <c:if test="${sessionScope.allAnswered == true}">
            <p><f:message key="submit_test_msg"/></p>
            <p><f:message key="continue?"/></p>
          </c:if>
          <c:if test="${sessionScope.allAnswered == false}">
            <p class="fw-semibold"><f:message key="not_all_questions_ans"/></p>
            <p><f:message key="still_continue?"/></p>
          </c:if>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><f:message key="cancel"/></button>
          <button <c:if test="${sessionScope.allAnswered == true}">class="btn btn-primary"</c:if>
                  <c:if test="${sessionScope.allAnswered == false}">class="btn btn-danger"</c:if>
                  type="submit" form="answerForm" onclick="submitTest()">
            <f:message key="submit_test"/>
          </button>
        </div>
      </div>
    </div>
  </div>
  <script>
      const countDownDate = new Date(${sessionScope.deadline});
      const x = setInterval(function () {
          const now = new Date().getTime();
          const distance = countDownDate - now;

          let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          let seconds = Math.floor((distance % (1000 * 60)) / 1000);
          
          if (hours < 10) {
              hours = "0" + hours;
          }
          if (minutes < 10) {
              minutes = "0" + minutes;
          }
          if (seconds < 10) {
              seconds = "0" + seconds;
          }
          if (hours !== "00") {
              document.getElementById("submitTimer").innerHTML = hours + ":"
                  + minutes + ":" + seconds;
          } else {
              document.getElementById("submitTimer").innerHTML = minutes + ":" + seconds;
          }
          if (distance < 0) {
              submitTest();
          }
      }, 1000);
  </script>
  <script async>
      async function goTo(questionToGo) {
          await new Promise(resolve => setTimeout(resolve, 150));
          window.location.href = "${root}/controller?command=serviceTest&question=" + questionToGo;
      }

      async function submitTest() {
          await new Promise(resolve => setTimeout(resolve, 150));
          post("${root}/controller", {command: "submitTest"});
      }
      
      function post(path, params) {
          const form = document.createElement('form');
          form.method = "POST";
          form.action = path;

          for (const key in params) {
              if (params.hasOwnProperty(key)) {
                  const hiddenField = document.createElement('input');
                  hiddenField.type = 'hidden';
                  hiddenField.name = key;
                  hiddenField.value = params[key];

                  form.appendChild(hiddenField);
              }
          }

          document.body.appendChild(form);
          form.submit();
      }
  </script>
</body>
</html>
