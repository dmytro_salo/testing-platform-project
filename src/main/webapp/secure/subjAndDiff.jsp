<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% System.out.println("jsp: subjAndDiff.jsp"); %>
<html>
<head>
  <title>Subjects and Difficulties</title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"/>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous" async></script>
</head>
<body>
  <jsp:include page="header.jsp"/>
  <div class="container-md">
    <div class="row">
      <div class="col-6">
        <p class="fs-4 text-center">Manage subjects:</p>
        <hr/>
        <c:if test="${param.deleteSubjError == 'testExists'}">
          <p class="fs-6 text-danger text-center">
            Cannot delete, at least one test with this subject exists in database
          </p>
        </c:if>
        <c:if test="${param.emptySubjName == true}">
          <p class="text-center fs-6 text-danger">Name must not be empty</p>
        </c:if>
        <c:if test="${param.subjEnglishNameExists == true}">
          <p class="text-center fs-6 text-danger">Subject with this english name already exists</p>
        </c:if>
        <c:if test="${param.subjUkrainianNameExists == true}">
          <p class="text-center fs-6 text-danger">Subject with this ukrainian name already exists</p>
        </c:if>
      </div>
      <div class="col-6">
        <p class="fs-4 text-center">Manage difficulties:</p>
        <hr/>
        <c:if test="${param.deleteError == 'testExists'}">
          <p class="fs-6 text-danger text-center">
            Cannot delete, at least one test with this difficulty exists in database
          </p>
        </c:if>
        <c:if test="${param.emptyDiffName == true}">
          <p class="text-center fs-6 text-danger">Name must not be empty</p>
        </c:if>
        <c:if test="${param.levelExists == true}">
          <p class="text-center fs-6 text-danger">Difficulty with this level already exists</p>
        </c:if>
        <c:if test="${param.englishNameExists == true}">
          <p class="text-center fs-6 text-danger">Difficulty with this english name already exists</p>
        </c:if>
        <c:if test="${param.ukrainianNameExists == true}">
          <p class="text-center fs-6 text-danger">Difficulty with this ukrainian name already exists</p>
        </c:if>
      </div>
    </div>
    <div class="row">
      <c:set var="firstSubject" value="${applicationScope.subjects.get(0)}"/>
      <div class="col-2">
        <div class="list-group" id="list-tab-subj" role="tablist">
          <a class="list-group-item list-group-item-action active" id="list-subj-list-${firstSubject.id}"
             data-bs-toggle="list" href="#list-subj-${firstSubject.id}" role="tab"
             aria-controls="list-subj-${firstSubject.id}">
            ${firstSubject.name}
          </a>
          <c:forEach var="subject" items="${applicationScope.subjects}" begin="1">
            <a class="list-group-item list-group-item-action" id="list-subj-list-${subject.id}"
               data-bs-toggle="list" href="#list-subj-${subject.id}" role="tab"
               aria-controls="list-subj-${subject.id}">
                ${subject.name}
            </a>
          </c:forEach>
        </div>
        <br/>
        <button class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#newSubjModal">
          Create new subject
        </button>
      </div>
      <div class="col-4">
        <div class="tab-content" id="nav-tabContent-subj">
          <div class="tab-pane fade show active" id="list-subj-${firstSubject.id}"
               role="tabpanel" aria-labelledby="list-subj-list-${firstSubject.id}">
            <form action="${pageContext.request.contextPath}/controller" method="post"
                  id="formSubj1-${firstSubject.id}">
              <input type="hidden" name="command" value="editSubject">
              <input type="hidden" name="id" value="${firstSubject.id}">
              <div class="form-group">
                <div class="form-text">
                  <label for="nameS1" class="form-label">Name (English):</label>
                  <input type="text" id="nameS1" name="name" value="${firstSubject.name}"/>
                </div>
                <div class="form-text">
                  <label for="nameSUk1" class="form-label">Name (Ukrainian):</label>
                  <input type="text" id="nameSUk1" name="nameUk" value="${firstSubject.nameUk}"/>
                </div>
              </div>
            </form>
            <hr/>
            <button class="btn btn-outline-danger" data-bs-toggle="modal"
                    data-bs-target="#deleteSubjectModal${firstSubject.id}">Delete</button>
            <button class="btn btn-primary" type="submit" form="formSubj1-${firstSubject.id}">Edit</button>
          </div>
          <c:forEach var="subject1" items="${applicationScope.subjects}" begin="1">
            <div class="tab-pane fade" id="list-subj-${subject1.id}"
                 role="tabpanel" aria-labelledby="list-subj-list-${subject1.id}">
              <form action="${pageContext.request.contextPath}/controller" method="post" id="formSubj-${subject1.id}">
                <input type="hidden" name="command" value="editSubject">
                <input type="hidden" name="id" value="${subject1.id}">
                <div class="form-group">
                  <div class="form-text">
                    <label for="nameS" class="form-label">Name (English):</label>
                    <input type="text" id="nameS" name="name" value="${subject1.name}"/>
                  </div>
                  <div class="form-text">
                    <label for="nameSUk" class="form-label">Name (Ukrainian):</label>
                    <input type="text" id="nameSUk" name="nameUk" value="${subject1.nameUk}"/>
                  </div>
                </div>
              </form>
              <hr/>
              <button class="btn btn-outline-danger" data-bs-toggle="modal"
                      data-bs-target="#deleteSubjectModal${subject1.id}">Delete</button>
              <button class="btn btn-primary" type="submit" form="formSubj-${subject1.id}">Edit</button>
            </div>
            <!-- Delete subject modal -->
            <div class="modal fade" id="deleteSubjectModal${subject1.id}" tabindex="-1"
                 aria-labelledby="deleteSubjectModalLabel${subject1.id}" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="deleteSubjectModalLabel${subject1.id}">Delete subject?</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <p>Subject <span class="fw-bolder">"${subject1.name}"</span> will
                      be permanently deleted from the database.</p>
                    <p>Are you sure? This can't be undone.</p>
                    <form method="post" action="${pageContext.request.contextPath}/controller"
                          id="deleteSubj${subject1.id}">
                      <input type="hidden" name="command" value="deleteSubject">
                      <input type="hidden" name="id" value="${subject1.id}">
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" form="deleteSubj${subject1.id}">Delete</button>
                  </div>
                </div>
              </div>
            </div>
          </c:forEach>
        </div>
      </div>
      
      <!-- ~~~~~~~~~~~~~~~~~~~~~~~ -->
      
      <c:set var="firstDifficulty" value="${applicationScope.difficulties.get(0)}"/>
      <div class="col-2">
        <div class="list-group" id="list-tab-diff" role="tablist">
          <a class="list-group-item list-group-item-action active" id="list-diff-list-${firstDifficulty.id}"
             data-bs-toggle="list" href="#list-diff-${firstDifficulty.id}" role="tab"
             aria-controls="list-diff-${firstDifficulty.id}">
            ${firstDifficulty.name}
          </a>
          <c:forEach var="difficulty" items="${applicationScope.difficulties}" begin="1">
            <a class="list-group-item list-group-item-action" id="list-diff-list-${difficulty.id}"
               data-bs-toggle="list" href="#list-diff-${difficulty.id}" role="tab"
               aria-controls="list-diff-${difficulty.id}">
                ${difficulty.name}
            </a>
          </c:forEach>
        </div>
        <br/>
        <button class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#newDiffModal">
          Create new difficulty
        </button>
      </div>
      <div class="col-4">
        <div class="tab-content" id="nav-tabContent-diff">
          <div class="tab-pane fade show active" id="list-diff-${firstDifficulty.id}"
               role="tabpanel" aria-labelledby="list-diff-list-${firstDifficulty.id}">
            <form action="${pageContext.request.contextPath}/controller" method="post"
                  id="formDiff1-${firstDifficulty.id}">
              <input type="hidden" name="command" value="editDifficulty">
              <input type="hidden" name="id" value="${firstDifficulty.id}">
              <div class="form-group">
                <div class="form-text">
                  <label for="name1" class="form-label">Name (English):</label>
                  <input type="text" id="name1" name="name" value="${firstDifficulty.name}"/>
                </div>
                <div class="form-text">
                  <label for="nameUk1" class="form-label">Name (Ukrainian):</label>
                  <input type="text" id="nameUk1" name="nameUk" value="${firstDifficulty.nameUk}"/>
                </div>
                <div class="form-text">
                  <label for="level1" class="form-label">Level:</label>
                  <input type="number" id="level1" name="level" max="100" min="1"
                         value="${firstDifficulty.level}"/>
                </div>
              </div>
            </form>
            <hr/>
            <button class="btn btn-outline-danger" data-bs-toggle="modal"
                    data-bs-target="#deleteDifficultyModal${firstDifficulty.id}">Delete</button>
            <button class="btn btn-primary" type="submit" form="formDiff1-${firstDifficulty.id}">Edit</button>
          </div>
          <c:forEach var="difficulty1" items="${applicationScope.difficulties}" begin="1">
            <div class="tab-pane fade" id="list-diff-${difficulty1.id}"
                 role="tabpanel" aria-labelledby="list-diff-list-${difficulty1.id}">
              <form action="${pageContext.request.contextPath}/controller" method="post"
                    id="formDiff-${difficulty1.id}">
                <input type="hidden" name="command" value="editDifficulty">
                <input type="hidden" name="id" value="${difficulty1.id}">
                <div class="form-groum">
                  <div class="form-text">
                    <label for="name1" class="form-label">Name (English):</label>
                    <input type="text" id="name" name="name" value="${difficulty1.name}"/>
                  </div>
                  <div class="form-text">
                    <label for="nameUk1" class="form-label">Name (Ukrainian):</label>
                    <input type="text" id="nameUk" name="nameUk" value="${difficulty1.nameUk}"/>
                  </div>
                  <div class="form-text">
                    <label for="level1" class="form-label">Level:</label>
                    <input type="number" id="level" name="level" max="100" min="1"
                           value="${difficulty1.level}"/>
                  </div>
                </div>
              </form>
              <hr/>
              <button class="btn btn-outline-danger" data-bs-toggle="modal"
                      data-bs-target="#deleteDifficultyModal${difficulty1.id}">Delete</button>
              <button class="btn btn-primary" type="submit" form="formDiff-${difficulty1.id}">Edit</button>
            </div>
            <!-- Delete difficulty modal -->
            <div class="modal fade" id="deleteDifficultyModal${difficulty1.id}" tabindex="-1"
                 aria-labelledby="deleteDifficultyModalLabel${difficulty1.id}" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="deleteDifficultyModalLabel${difficulty1.id}">Delete difficulty?</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <p>Difficulty <span class="fw-bolder">"${difficulty1.name}"</span> will
                      be permanently deleted from the database.</p>
                    <p>Are you sure? This can't be undone.</p>
                    <form method="post" action="${pageContext.request.contextPath}/controller"
                          id="deleteDiff${difficulty1.id}">
                      <input type="hidden" name="command" value="deleteDifficulty">
                      <input type="hidden" name="id" value="${difficulty1.id}">
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" form="deleteDiff${difficulty1.id}">Delete</button>
                  </div>
                </div>
              </div>
            </div>
          </c:forEach>
        </div>
      </div>
    </div>
  </div>
  <!-- Create subject modal -->
  <div class="modal fade" id="newSubjModal" tabindex="-1"
       aria-labelledby="newSubjModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="newSubjModalLabel">Create new subject</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form action="${pageContext.request.contextPath}/controller" method="post" id="newSubjForm">
            <input type="hidden" name="command" value="createSubject">
            <div class="form-group">
              <div class="form-text">
                <label for="new-s-name" class="form-label">Name (English):</label>
                <input type="text" id="new-s-name" name="name"
                       value="${not empty sessionScope.writtenSubject ? sessionScope.writtenSubject.name : ''}"
                       required/>
              </div>
              <div class="form-text">
                <label for="new-s-nameUk" class="form-label">Name (Ukrainian):</label>
                <input type="text" id="new-s-nameUk" name="nameUk"
                       value="${not empty sessionScope.writtenSubject ? sessionScope.writtenSubject.nameUk : ''}"
                       required/>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" form="newSubjForm">Create</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Create difficulty modal -->
  <div class="modal fade" id="newDiffModal" tabindex="-1"
       aria-labelledby="newDiffModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="newDiffModalLabel">Create new difficulty</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form action="${pageContext.request.contextPath}/controller" method="post" id="newDiffForm">
            <input type="hidden" name="command" value="createDifficulty">
            <div class="form-group">
              <div class="form-text">
                <label for="new-name" class="form-label">Name (English):</label>
                <input type="text" id="new-name" name="name"
                       value="${not empty sessionScope.writtenDifficulty ? sessionScope.writtenDifficulty.name : ''}"
                       required/>
              </div>
              <div class="form-text">
                <label for="new-nameUk" class="form-label">Name (Ukrainian):</label>
                <input type="text" id="new-nameUk" name="nameUk"
                       value="${not empty sessionScope.writtenDifficulty ? sessionScope.writtenDifficulty.nameUk : ''}"
                       required/>
              </div>
              <div class="form-text">
                <label for="new-level" class="form-label">Level:</label>
                <input type="number" id="new-level" name="level" max="100" min="1"
                       value="${not empty sessionScope.writtenDifficulty ? sessionScope.writtenDifficulty.level : ''}"
                       required/>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" form="newDiffForm">Create</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Delete first subject modal -->
  <div class="modal fade" id="deleteSubjectModal${firstSubject.id}" tabindex="-1"
       aria-labelledby="deleteSubjectModalLabel${firstSubject.id}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteSubjectModalLabel${firstSubject.id}">Delete subject?</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <p>Subject <span class="fw-bolder">"${firstSubject.name}"</span> will
            be permanently deleted from the database.</p>
          <p>Are you sure? This can't be undone.</p>
          <form method="post" action="${pageContext.request.contextPath}/controller"
                id="deleteSubj${firstSubject.id}">
            <input type="hidden" name="command" value="deleteSubject">
            <input type="hidden" name="id" value="${firstSubject.id}">
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger" form="deleteSubj${firstSubject.id}">Delete</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Delete first difficulty modal -->
  <div class="modal fade" id="deleteDifficultyModal${firstDifficulty.id}" tabindex="-1"
       aria-labelledby="deleteDifficultyModalLabel${firstDifficulty.id}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteDifficultyModalLabel${firstDifficulty.id}">Delete difficulty?</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <p>Difficulty <span class="fw-bolder">"${firstDifficulty.name}"</span> will
            be permanently deleted from the database.</p>
          <p>Are you sure? This can't be undone.</p>
          <form method="post" action="${pageContext.request.contextPath}/controller"
                id="deleteDiff${firstDifficulty.id}">
            <input type="hidden" name="command" value="deleteDifficulty">
            <input type="hidden" name="id" value="${firstDifficulty.id}">
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger" form="deleteDiff${firstDifficulty.id}">Delete</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
