<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<c:set var="locale"
       value="${not empty param.loc ?
       param.loc : not empty sessionScope.loggedUser ?
       sessionScope.loggedUser.localeString : pageContext.request.locale}"/>
<f:setLocale value="${locale}"/>
<f:setBundle basename="text"/>
<% System.out.println("signUpLocale = " + pageContext.getAttribute("locale")); %>
<html>
<head>
  <title><f:message key="sign_up"/></title>
  <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/static/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/static/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/static/favicon/favicon-16x16.png">
  <link rel="manifest" href="${pageContext.request.contextPath}/static/favicon/site.webmanifest">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous" async></script>
</head>
<body>
  <jsp:include page="secure/header.jsp"/>
  <div class="container-md">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/"><f:message key="home"/></a></li>
        <li class="breadcrumb-item active" aria-current="page">
          <c:choose>
            <c:when test="${sessionScope.loggedUser.role != 'admin'}"><f:message key='sign_up'/>:</c:when>
            <c:otherwise><f:message key='new_account'/>:</c:otherwise>
          </c:choose>
        </li>
      </ol>
    </nav>
    <br/>
    <div class="row">
      <div class="col-lg-2"></div>
      <div class="col-lg-8">
        <div class="card align-middle">
          <div class="card-header">
            <br/>
            <h6 class="card-subtitle
                <c:choose>
                  <c:when test="${paramValues.size() == 0}">text-muted</c:when>
                  <c:otherwise>text-danger</c:otherwise>
                </c:choose>">
              <c:if test="${paramValues.size() == 0}">
                <f:message key="enter_details"/>
              </c:if>
              <c:if test="${param.invalidEmail == true}">
                <f:message key="unreal_email"/>
              </c:if>
              <c:if test="${param.assertPassError == true}">
                <f:message key="repeat_password_message"/>
              </c:if>
              <c:if test="${param.invalidPassword == true}">
                <f:message key="invalid_password"/>
              </c:if>
              <c:if test="${param.invalidName == true}">
                <f:message key="invalid_name"/>
              </c:if>
              <c:if test="${param.alreadySignedUp == true}">
                <f:message key="email"/> '${sessionScope.writtenUser.email}' <f:message key="already_registered"/>
              </c:if>
            </h6>
          </div>
          <div class="card-body">
            <form action="${pageContext.request.contextPath}/controller" accept-charset="UTF-8" method="post">
              <input type="hidden" name="command" value="signUp"/>
              <input type="hidden" name="localeString"
                     value="${locale}">
              <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">
                  <f:message key="email"/>:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control"
                         placeholder="<f:message key='email'/>"
                         name="email" value="${sessionScope.writtenUser.email}">
                </div>
              </div>
              <br/>
              <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">
                  <f:message key="name"/>:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control"
                         placeholder="<f:message key='first_name'/>"
                         name="firstName" value="${sessionScope.writtenUser.firstName}">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"></label>
                <div class="col-sm-10">
                  <input type="text" class="form-control"
                         placeholder="<f:message key='last_name'/>"
                         name="lastName" value="${sessionScope.writtenUser.lastName}">
                </div>
              </div>
              <br/>
              <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"><f:message key="password"/>:</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control"
                         placeholder="<f:message key='password'/>"
                         name="password">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"></label>
                <div class="col-sm-10">
                  <input type="password" class="form-control"
                         placeholder="<f:message key='repeat_password'/>"
                         name="repeatPassword"/>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-10 ml-sm-auto">
                  <br/>
                  <button type="submit" class="btn btn-primary"><f:message key="submit"/></button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>