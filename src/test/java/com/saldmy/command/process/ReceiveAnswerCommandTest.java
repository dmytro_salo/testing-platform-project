package com.saldmy.command.process;

import com.saldmy.exception.CommandException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import static org.mockito.Mockito.*;

public class ReceiveAnswerCommandTest {

    @Test
    void testCommand() throws CommandException {
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);

        Map<Integer, String[]> answerMap = new HashMap<>();
        IntStream.range(1, 6).forEach(integer -> answerMap.put(integer, null));

        when(req.getSession()).thenReturn(session);
        doNothing().when(session).setAttribute(anyString(), any());
        when(req.getParameter("questionId")).thenReturn("1");
        doReturn(new String[] {"2", "3"}).when(req).getParameterValues("selectedAnswers");
        doReturn(answerMap).when(session).getAttribute("answerMap");

        ReceiveAnswerCommand receiveAnswerCommand = new ReceiveAnswerCommand();
        Assertions.assertNull(receiveAnswerCommand.execute(req, resp));
        Assertions.assertArrayEquals(answerMap.get(1), new String[] {"2", "3"});
    }

}
