package com.saldmy.command.process;

import com.saldmy.exception.CommandException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.mockito.Mockito.*;

public class ServiceTestCommandTest {

    @ParameterizedTest
    @ValueSource(strings = {"3", "4", "5"})
    void testCommand(String questionNumber) throws CommandException {
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse resp = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        com.saldmy.entity.Test test = mock(com.saldmy.entity.Test.class);

        when(req.getSession()).thenReturn(session);
        when(test.getNumOfQuestions()).thenReturn(20);

        List<com.saldmy.entity.Test.Question> questions = new ArrayList<>();
        IntStream.range(0, 20).forEach(integer -> {
            com.saldmy.entity.Test.Question q = new com.saldmy.entity.Test.Question();
            q.setId(integer);
            questions.add(integer, q);
        });
        when(test.getQuestions()).thenReturn(questions);
        when(session.getAttribute("startedTest")).thenReturn(test);

        when(req.getParameter("question")).thenReturn(questionNumber);

        Assertions.assertEquals(new ServiceTestCommand().execute(req, resp), "/secure/question.jsp");
    }
}