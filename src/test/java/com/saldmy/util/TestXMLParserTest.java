package com.saldmy.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class TestXMLParserTest {

    @Test
    void testParseTest() throws IOException, SAXException {
        assertNotNull(TestXMLParser.parseTest("src/test/resources/krok-1.xml"));
        assertDoesNotThrow(() -> TestXMLParser.parseTest("src/test/resources/krok-1-mul.xml"));
    }

    @Test
    void testInvalidParseTest(){
        assertThrows(IllegalArgumentException.class, () -> TestXMLParser.parseTest(null));
    }

}
