package com.saldmy.util;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.xml.sax.SAXException;

import static org.junit.jupiter.api.Assertions.*;

public class ValidatorsTest {

    @Nested
    class ValidateEmailTest {

        @Test
        void testNull() {
            assertFalse(Validators.validateEmail(null));
        }

        @ParameterizedTest
        @ValueSource(strings = {"dmytrosalo@protonmail.com", "d.sa@mskk.sd", "234@s.s", "As1_.@Skd.aD", "S@D.D"})
        void testValid(String input) {
            assertTrue(Validators.validateEmail(input));
        }

        @ParameterizedTest
        @ValueSource(strings = {"dmytrosaloprotonmail.com", "@mskk.sd", "234@s.s.d", "As1_:.@Skd.aD",
                                "S@D", "sk@skd,com", "sлв@laj.com", "slkd@kdlf"})
        void testInvalid(String input) {
            assertFalse(Validators.validateEmail(input));
        }

    }

    @Nested
    class ValidatePasswordTest {

        @Test
        void testNull() {
            assertFalse(Validators.validatePassword(null));
        }

        @ParameterizedTest
        @ValueSource(strings = {"testpass3", "adslfasjfh44532", "s2345245", "testpas3", "DKJFDLJF8"})
        void testValid(String input) {
            assertTrue(Validators.validatePassword(input));
        }

        @ParameterizedTest
        @ValueSource(strings = {"testpa3", "adslfasjfh", "23454322345245", "testpas3 "})
        void testInvalid(String input) {
            assertFalse(Validators.validatePassword(input));
        }

    }

    @Nested
    class ValidateIntParameterTest {

        @Test
        void testNull() {
            assertFalse(Validators.validateIntParameter(null));
        }

        @ParameterizedTest
        @ValueSource(strings = {"3", "52", "2435", "0"})
        void testValid(String input) {
            assertTrue(Validators.validateIntParameter(input));
        }

        @ParameterizedTest
        @ValueSource(strings = {"-3", "-0", "2345432 ", "af", "4.3", "432j234", "0x42435"})
        void testInvalid(String input) {
            assertFalse(Validators.validateIntParameter(input));
        }

    }

    @Nested
    class ValidateNameTest {

        @Test
        void testNull() {
            assertFalse(Validators.validateName(null));
        }

        @ParameterizedTest
        @ValueSource(strings = {"Ілона", "Мар'яна", "Єлизавета", "Карпенко-Карий", "John", "Alice", "Ян"})
        void testValid(String input) {
            assertTrue(Validators.validateName(input));
        }

        @ParameterizedTest
        @ValueSource(strings = {"ілона", "Ф", "Афог ", "af", "4.3", "432j234", "0x42435", "АоарАЛ"})
        void testInvalid(String input) {
            assertFalse(Validators.validateName(input));
        }

    }

    @Nested
    class ValidateTestTitleTest {

        @Test
        void testNull() {
            assertFalse(Validators.validateTestTitle(null));
        }

        @ParameterizedTest
        @ValueSource(strings = {"Variable", "Hello. Those are beautiful.",
                                "Єлизавета; Hkdjf, fjdl/jf,/", "Follow Java."})
        void testValid(String input) {
            assertTrue(Validators.validateTestTitle(input));
        }

        @ParameterizedTest
        @ValueSource(strings = {"variable", "4ello. Those are beautiful.",
                "Єлиза", "/ollow Java."})
        void testInvalid(String input) {
            assertFalse(Validators.validateTestTitle(input));
        }

    }

    @Nested
    class ValidateCapsInPasswordTest {

        @Test
        void testNull() {
            assertFalse(Validators.validateCapsInPassword(null));
        }

        @ParameterizedTest
        @ValueSource(strings = {"tESTPASS4", "TESTPASS4"})
        void testValid(String input) {
            assertTrue(Validators.validateCapsInPassword(input));
        }

        @ParameterizedTest
        @ValueSource(strings = {"teaSTskjdf5", "Testpass4", "TEsgslanah6", "YR", "U"})
        void testInvalid(String input) {
            assertFalse(Validators.validateCapsInPassword(input));
        }

    }

    @Nested
    class ValidateBooleanParameterTest {

        @Test
        void testNull() {
            assertFalse(Validators.validateBooleanParameter(null));
        }

        @ParameterizedTest
        @ValueSource(strings = {"true", "false"})
        void testValid(String input) {
            assertTrue(Validators.validateBooleanParameter(input));
        }

        @ParameterizedTest
        @ValueSource(strings = {"TRUE", "FALSE", "True", "False", "U", "1", "lskd"})
        void testInvalid(String input) {
            assertFalse(Validators.validateBooleanParameter(input));
        }

    }

    @Nested
    class ValidateTestTest {

        @Test
        void testValid() {
            assertDoesNotThrow(() -> Validators.validateTest("src/test/resources/krok-1.xml"));
            assertDoesNotThrow(() -> Validators.validateTest("src/test/resources/krok-1-mul.xml"));
        }

        @Test
        void testInvalid() {
            assertThrows(SAXException.class, () -> Validators.validateTest(
                    "src/test/resources/krok-1-incorrect.xml"
            ));
            assertThrows(SAXException.class, () -> Validators.validateTest(
                    "src/test/resources/krok-1-incorrect1.xml"
            ));
        }
    }

}
